# PL1

## dureza e escala Mohs
**dureza**
- a dureza de um mineral é a resistência que a sua superfície oferece ao ser riscada
- o grau de dureza é determinado pela comparação da dificuldade ou facilidade com que um mineral é riscado
- principio: um material mais duro irá riscar/deixa um risco um material mais mole
- este principio de medição relativa já é usado desde a antiguidade. A escala usada em mineralogia é a de Mohs. Outras modificações á escala, como a de Ridgway, têm mais aplicação na industria

**escala Mohs de dureza de minerais**
- é uma escala qualitativa, relativa e ordinal (de 1 a 10)
- baixa precisão mas prática no campo
- criada em 1822 por Friedrich Mohs
- composta por minerais que são fácies de encontrar, á excepção do diamante
- ao talco atribui o valor 1 e ao diamante o 10
- não é linear

![nach moh harte skala](img/miner/mohs.png)<br>
![moh scale wrong minerals](img/miner/mohs-wrong.png)<br>
![moh scale ok](img/miner/mohs-ok.jpg)<br>

Os minerais da escala:
1. Talco
2. Gesso

a unha tem uma dureza entre 2 e 3. É fácil arranhar o talco. O gesso já é mais complicado e a calcite impossivel de arranhar.

3. Calcite
4. Fluorite
5. Apatite

o canivete/tesoura de aço (liga de Fe e C) e o vidro estão aqui algures

6. Feldspato (ortoclásio/potássico)

a placa de porcelana

7. Quartzo
8. Topázio
9. Corindo
10. Diamante

Não há diamante no lab... Normalmente temos lá o íman.

![mohs minerals](img/miner/mohs-minerals.png)


procedimento:<br>
"é riscar mesmo, não é fazer festinhas"<br>
"se não querem usar a mão, deixem crescer e cortem a unha do pé"<br>
temos 2 minerais, A e B<br>
Se o A risca o B, o A é mais duro<br>
Se o A não rsica o B, o B é mais duro<br>
Se A e B têm a mesma dureza, serão ineficientes na produção de riscos um no outro (no entanto, podem surgir pequenos riscos)


Extra:
- pode-se falar de dureza de uma forma mais geral, como sendo uma medida da resistência a deformação plástica localizada
- riscar (fricção com outro objecto), como na escala de Mohs, não é a única forma de induzir essa deformação plástica.<br>
Na [dureza á indentação](https://en.wikipedia.org/wiki/Indentation_hardness), a deformação é devida a uma carga compressiva constante. Há varias escalas, conforme a natureza do material que se quer avaliar (há para metais, cerâmicos, polímeros), geralmente absolutas.<br>
eg. [teste dureza Brinnell](https://www.youtube.com/watch?v=RJXJpeH78iU), [Vickers](https://www.youtube.com/watch?v=7Z90OZ7C2jI), [Rockwell](https://www.youtube.com/watch?v=G2JGNlIvNC4)<br>
Também aparece na net uma [dureza dinâmica/de resalto](https://material-properties.org/what-is-rebound-hardness-definition/) em que se mede a altura do ressalto de um objecto deixado cair de uma altura fixa... Nunca ouvi falar disto, não me parece que seja muito usado.
- supostamente o cobre tem dureza entre 3 e 4, podendo então arranhar a calcite. Por isso, uma referencia parece ser uma moeda de cobre. Mas não sei até que ponto isto é aplicável ás moedas de euro. Elas têm cobre mas... em concreto,  1, 2 e 5 são aço banhado a cobre e as de 10, 20 e 50 são ligas metálicas de cobre. Podem testar e ver.
- em ingles,<br>
duro = hard, em oposição a mole = soft<br>
dureza ao risco = scratch hardness<br>
à indentação = indentation hardness<br>
talco = talc/talcum
gesso/gipsita = gypsum

https://en.wikipedia.org/wiki/Mohs_scale_of_mineral_hardness



## formas simples

TODO!

# PL2

identificação de minerais em amostra de mão

O objectivo será decorar a seguinte 
## lista de minerais

**ELEMENTOS NATIVOS**
- [Grafite](http://rruff.info/doclib/hom/graphite.pdf)
- [Enxofre nativo](http://rruff.info/doclib/hom/sulphur.pdf)

**ÓXIDOS**
- [Corindo](http://rruff.info/doclib/hom/corundum.pdf)
- [Magnetite](http://rruff.info/doclib/hom/magnetite.pdf)
- [Hematite](http://rruff.info/doclib/hom/hematite.pdf)

**SILICATOS**
- Grupo das olivinas

    Exemplo: [forsterite](http://rruff.info/doclib/hom/forsterite.pdf)

- Grupo das granadas

    Exemplo: [andradite](http://rruff.info/doclib/hom/andradite.pdf)

- [Andaluzite](http://rruff.info/doclib/hom/andalusite.pdf)
- [Topázio](http://rruff.info/doclib/hom/topaz.pdf)
- [Estaurolite](http://rruff.info/doclib/hom/staurolite.pdf)

- Grupo das turmalinas
    
    Exemplo: [schorlite](http://rruff.info/doclib/hom/schorl.pdf)

- [Berilo](http://rruff.info/doclib/hom/beryl.pdf)
- Grupo das piroxenas
    
    [Espodumena](http://rruff.info/doclib/hom/spodumene.pdf)

- [Talco](http://rruff.info/doclib/hom/talc.pdf)
- Grupo das micas

    Exemplos:
    - [Moscovite](http://rruff.info/doclib/hom/muscovite.pdf)
    - [Biotites](http://rruff.info/doclib/hom/biotite.pdf)
    - [Lepidolite](http://rruff.info/doclib/hom/lepidolite.pdf)

- Grupo das clorites
    
    Exemplo: [clinocloro](http://rruff.info/doclib/hom/clinochlore.pdf)

- [Quartzo](http://rruff.info/doclib/hom/quartz.pdf)
- Grupo dos feldspatos
    
    Exemplo: [microclina](http://rruff.info/doclib/hom/microcline.pdf)

**FOSFATOS**
- Grupo das apatites
    
    Exemplo: [fluoroapatite](http://rruff.info/doclib/hom/apatitecaf.pdf)

**CARBONATOS**
- [Calcite](http://rruff.info/doclib/hom/calcite.pdf)
- [Aragonite](http://rruff.info/doclib/hom/aragonite.pdf)
- [Malaquite](http://rruff.info/doclib/hom/malachite.pdf)

**SULFATOS**
- [Barite](http://rruff.info/doclib/hom/baryte.pdf)
- [Gesso](http://rruff.info/doclib/hom/gypsum.pdf)

**TUNGSTATOS**
- Série das volframites
    
    Exemplo: [ferberite](http://rruff.info/doclib/hom/ferberite.pdf)

**SULFURETOS**
- [Galena](http://rruff.info/doclib/hom/galena.pdf)
- [Esfalerite (blenda)](http://rruff.info/doclib/hom/sphalerite.pdf)
- [Pirite](http://rruff.info/doclib/hom/pyrite.pdf)
- [Calcopirite](http://rruff.info/doclib/hom/chalcopyrite.pdf)
- [Arsenopirite](http://rruff.info/doclib/hom/arsenopyrite.pdf)
- [Pirrotite](http://rruff.info/doclib/hom/pyrrhotite.pdf)
- [Estibina](http://rruff.info/doclib/hom/stibnite.pdf)
- [Molibdenite](http://rruff.info/doclib/hom/molybdenite.pdf)

**HALETOS** (HALOGENETOS/HALOIDES)
- [Halite](http://rruff.info/doclib/hom/halite.pdf)
- [Fluorite](http://rruff.info/doclib/hom/fluorite.pdf)

## minerais da primeira aula

não faço ideia qual foi o tema mas regra geral a risca era escura

- volframite

    - dos Tungstatos só vamos falar da série das volframites, que são tungstatos de Fe. Também têm manganes. $`(Fe,Mn)WO_4`$. Nesta série, podemos ir da ferberite (rica em Fe) até á hubnerite (rica em Mn), mas nós não vamos fazer essa distinção. Ou seja, chamamos a todos eles volframite.
    - a risca é castanho muito escuro/"cholocate preto"
    - tabular e com clivagem lateral 

    ![volframite](img/miner/volframite.png)
    
- grafite
    - elemento nativo, C
    - dureza 1.5, fácil de riscar com a unha
    - tacto untuoso, "desfaz-se todo"
    - clivagem pinacoidal

    ![grafite](img/miner/grafite.png)

- magnetite
    - um óxido (de Fe)
    - tem magnetismo

    ![magnetite](img/miner/magnetite.png)

VS

- hematite
    - um óxido (de Fe)
    - também pode apresentar magnetismo MAS...
    - risca castanha-ferroginosa ("sangue seco")
    - dureza 5.5-6 (*desnecessário*)

    ![hematite](img/miner/hematite.png)


- galena
    - um sulfureto (de chumbo, Pb)
    - dureza 2.5
    - risca cinzenta muito escura, com um brilho intenso
    - forma cubos, e tem clivagem cúbica

    ![galena](img/miner/galena.png)<br>
    ![galena2](img/miner/galena2.png)<br>
    ![galena3](img/miner/galena3.png)<br>

VS

- esfalerite
    - um sulfureto (de Zn), mas também é o principal minério do Índio (In). O Fe diminui o valor dela
    - dureza 3.5-4
    - é "enganosa/aldrabona" porque pode passar por galena (dureza 2.5).
    - é incolor com risca branca mas á medida que o teor em Fe aumenta, fica mais escura. A cor não se parece muito com a da hematite, mas também se pode reparar que a hematite é mais dura

    ![esfalerite](img/miner/esfalerite.png)


- pirite
    - um sulfureto (de Fe)
    - tonalidade dourada (ouro-dos-tolos)
    - risca bem o vidro (dureza 6-6.8 mas *desnecessário*)

    ![pirite](img/miner/pirite.png)<br>
    ![pirite2](img/miner/pirite2.png)<br>



VS

- calcopirite
    - um sulfureto (de Cu e Fe)
    - tonalidade dourada como a pirite mas não risca o vidro

    ![calcopirite](img/miner/calcopirite.png)<br>
    ![calcopirite2](img/miner/calcopirite2.png)<br>


VS

- arsenopirite
    - um sulfureto (de As e Fe), principal minério de arsénio
    - cor é um cinzento pálido, não é tão dourada como a pirite

    ![arsenopirite](img/miner/arsenopirite.png)<br>
    ![arsenopirite2](img/miner/arsenopirite2.png)<br>


VS

- pirrotite
    - sulfureto (de Fe)
    - tonalidade dourada ("cinzento bronze")
    - é magnética

    ![pirrotite](img/miner/pirrotite.png)

    ![pirrotite2](img/miner/pirrotite2.png)

    ![pirrotite3](img/miner/pirrotite3.png)

    um quartzo com intrusão de pirrotite


- estibina
    - sulfureto (de antimónio, 51 Sb)
    - dureza semelhante á da grafite
    - cristais alongados numa única direção, clivagem lateral

    ![estibina](img/miner/estibina.png)<br>
    ![estibina2](img/miner/estibina2.png)<br>





# PL3

## **Minerais de risca clara que não riscam o vidro**

(dureza < 5)

- enxofre nativo
    - cor amarela
    - risca também amarela

    ![enxofre](img/miner/enxofre-nativo-2.png)

- No grupo dos silicatos, vimos os filossilicatos. Todos estes ficam dispostos em folhas.

    - micas
        - moscovite
            - silicato de Al e K, com Mg e Fe
            - é a "mica branca". A cor é branca/cinzenta/amarelado
            - clivagem em planos muito finos

            ![moscovite](img/miner/moscovite.jpg)
        
        - biotite
            - é a "mica preta". A cor é castanha/negro
            - "Cuidado com a biotites!". Na biotite "fresca" a risca é branca mas em contacto com a água (e o vapor de água na atmosfera), a risca pode ficar mais escura

            ![biotite](img/miner/biotite.jpg)
        
        - lepidolite
            - em vez do Al, pode ter Si, Rb, Li; principal minério de lítio
            - é a "mica lilás"
            - geralmente não desenvolve cristais grandes. Textura de agregado "sacaróide" (parece açucar)

            ![lepidolite](img/miner/lepidolite.jpg)

            ![lepidolite2](img/miner/lepidolite2.jpg)

            ![lepidolite3](img/miner/lepidolite3.png)

    - clorite
        - substitui Mg e Fe por Ni e Mn
        - tonalidade verde (tem tom verde mas a que vi era cinza)
        - separa-se em folhas
        
        ![clorite](img/miner/clorite.jpg)
    
    - talco
        - aparece na escala Mohs
        - menos duro que o gesso (*não confiar*, mas fácil de riscar com a unha)
        - untoso ao tacto

        ![talco](img/miner/talco.jpg)


- No grupo dos fosfatos, só estudamos as apatites
    - apatite
        - fosfato de Ca
        - aparece na escala de Mohs mas "aparece com diferentes cores (é incolor) e parece um bocado baça"

        ![apatite](img/miner/apatite.jpg)

- No grupo dos carbonatos,
    - calcite
        - calcite e aragonite são ambas carbonato de cálcio
        - aparece na escala Mohs

        ![calcite](img/miner/calcite.jpg)

    - aragonite
        - é mais dura que a calcite
        - forma maclas
        - cristais pseudohexagonais

        ![aragonite](img/miner/aragonite.jpg)

- Todos os sulfatos
    - gesso
        - sulfato de Ca
        - aparece na escala Mohs
        - riscado pela unha
        - clivagem pinacoidal

        ![gesso](img/miner/gesso.jpg)<br>
        ![gesso2](img/miner/gesso2.png)<br>
        ![gesso3](img/miner/gesso3.png)<br>
        ![gesso4](img/miner/gesso4.png)<br>

    
    - barite
        - sulfato de bário (Ba)
        - dureza 3-3.5
        - elevada densidade

        ![barite](img/miner/barite-2.png)<br>
        ![barite2](img/miner/barite2.png)


- Todos os haletos
    - halite
        - cloreto de sódio (o mineral do sal de cozinha)
        - fica gorduroso com a humidade

        ![halite](img/miner/halite-2.png)
    
    - fluorite
        - fluoreto de Ca
        - aparece na escala Mohs
        - pode ter "cores bonitas" mas não necessariamente
        - dureza 4
        
        ![fluorite](img/miner/fluorite-2.png)<br>
        ![fluorite2](img/miner/fluorite2.png)<br>
        ![fluorite3](img/miner/fluorite3.png)<br>

        fluorite com a forma do modelo cubo com terminação nos vértices de octaedro<br> 
        ![fluorite-modelo](img/miner/fluorite-modelo.png)
    
# PL4

## tarefa M1

## **Minerais de risca clara que riscam o vidro**

No grupo dos Óxidos,
- corindo
    - está na escala de Mohs, é o 9
    - não apresenta cristais muito desenvolvidos

    ![corindo](img/miner/corindo.png)<br>
    ![corindo2](img/miner/corindo2.png)<br>

VS

No grupo dos Silicatos,
- topázio
    - está na escala de Mohs, é o 8

    ![topázio](img/miner/topázio.png)

    ![topázio castanho](img/miner/topázio-castanho.png)
    

- quartzo
    - os nossos são cinzentos, cor de rosa
    - terminações pseudopiramidais, mas nem sempre observáveis

    ![quartzo rosa](img/miner/quartzo-rosa.png)

    ![quartzo](img/miner/quartzo.png)

    ![quartzo rosa again](img/miner/quartzo-rosa2.png)

    ![quartzo rosa nunca cansa](img/miner/quartzo-rosa3.png)

    ![quartzo vermelho](img/miner/quartzo-vermelho.png)
    

- berilo
    - os nossos têm cores claras
    - clivagem pinacoidal

    ![berilo](img/miner/berilo.png)<br>
    ![berilo2](img/miner/berilo2.png)<br>
    ![berilos weird](img/miner/berilo-weird.png)<br>

    aqui há quartzo + berilo + turmalina. O vermelho é impurezas (óxidos de Fe)<br>
    ![berilo3](img/miner/berilo3.png)


- estaurolite
    - pode fazer macla em cruz ("cruz de pedra/latina") mas também há prismático

    ![estaurolite](img/miner/estaurolite.png)

    ![estaurolite2](img/miner/estaurolite2.png)

- andaluzite
    - cor lilás/cor de rosa escuro/"lembra carne crua em bom estado..."

    ![andaluzite2](img/miner/andaluzite2.png)<br>
    ![andaluzite3](img/miner/andaluzite3.png)<br>

    aqui vê-se andaluzite + quartzo + turmalina + moscovite<br>
    ![andaluzite](img/miner/andaluzite.png)

- grupo das turmalinas
    - cor muito escura ou preta
    - hábito prismático

    ![turmalina](img/miner/turmalina.png)

    ![turmalina e feldspato](img/miner/turmalina2.png)

- grupo das granadas
    - forma de grãos

    ![granada ugly](img/miner/granada1.png)<br>
    ![granada meh](img/miner/granada2.png)<br>
    ![granada3](img/miner/granada3.png)<br>
    ![granada4](img/miner/granada4.png)<br>

    granada com a forma do modelo icositetraedro deltóide<br>
    ![granada modelo](img/miner/granada-modelo.png)


- grupo dos feldspatos
    - clivagem pinacoidal muito marcada

    ![feldspato](img/miner/feldspato.png)

    ![quartzo e feldspato](img/miner/quartzo-feldspato.png)
    
    ![mais quartzo e feldspato](img/miner/quartzo-feldspato2.png)


- grupo das piroxenas, só estudamos a espodumena
    - espodumena
        - as nossas têm uma parte biológica (conchas)

        ![espodumena](img/miner/espodumena.png)

        ![espodumena2](img/miner/espodumena2.png)



**"exercicios"**
<br>aka idk<br>
![berilo or is it?](img/miner/berilo-dubious.png)<br>
![estaurolite not sure](img/miner/estaurolite-dubious.png)<br>
![talco not sure](img/miner/talco-dubious.png)<br>


# PL5

## tarefa M2

## Minerais que faltavam!

No grupo dos sulfuretos,
- molibdenite
    - faz lembrar papel de alumínio

    ![molibdenite](img/miner/molibdenite.png)

No grupo dos carbonatos,
- malaquite
    - cor verde
    - parece plasticina/que foi pintada

    ![malaquite](img/miner/malaquite.png)


**cruzes nas maclas**
- cruz latina

    ![cruz-latina](img/miner/cruz-latina.png)

    ![cruz-latina2](img/miner/cruz-latina2.png)

- cruz de santo andré

    ![cruz-santo-andre](img/miner/cruz-santo-andre.png)


**NÃO VIMOS/NÃO ME RECORDO**

No grupo dos silicatos, as olivinas
- olivina
    - cor verde (+ comum), amarelada, castanha ou branca
    - risca branca
    - transparente a translúcido, com um brilho vítreo

    ![olivina](img/miner/olivina1.png)

    ![olivina2](img/miner/olivina2.png)


**coleção**<br>
![coleçao1](img/miner/coleçao1.png)<br>
![coleçao2](img/miner/coleçao2.png)<br>
![coleçao3](img/miner/coleçao3.png)<br>
![coleçao4](img/miner/coleçao4.png)<br>
![coleçao5](img/miner/coleçao5.png)<br>

**exercicios - 10 minerais, 10 minutos?? boa sorte colegas...**<br>
![exer1](img/miner/exer1.png)

![exer2](img/miner/exer2.png)

![exer3](img/miner/exer3.png)

![exer4](img/miner/exer4.png)

![exer5](img/miner/exer5.png)

![exer6](img/miner/exer6.png)

![exer7](img/miner/exer7.png)

![exer8](img/miner/exer8.png)

![exer9](img/miner/exer9.png)

![exer10](img/miner/exer10.png)


*é tudo por agora, aceitam-se sugestões/denuncia de erros