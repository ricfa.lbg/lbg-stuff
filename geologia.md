# Geologia

## "aula 0"
(nao é uma aula. Estava a pensar fazer o resumo do secundário mas...na aconteceu)

- Geologia<br>
    - ciencia que estuda o sistema Terra
    - a Terra pode ser considerado um sistema fechado<br>
    pois efetua sobretudo trocas de energia com o exterior, nao sendo significativas as trocas de matéria<br>
    alguns autores chamam a isso semi-aberto ou semi-fechado
    - divisão em subsistemas: a hidrosfera, a atmosfera, a biosfera e a geosfera<br>
    estes subsistemas são abertos (trocam entre si matéria e energia) 


- hidrosfera
    - totalidade da água no planeta, no estado liquido e sólido (criosfera)
    - cobre ~70% da superficie da Terra<br>
    - a maior parte da massa de água (~97%) compõe os oceanos (na forma de água "salgada"/salina, ~2% está em gelo nos glaciares e nas calotes polares. O restante (<1%) está no estado liquido nas zonas continentais (á superficie e em profundidade).<br>
    Apenas 0,02% está nos lagos e rios e 0,001% como vapor na atmosfera.<br>
    Apenas 0.03% é movimentada anualmente no ciclo da água


- ciclo da água
    - movimento contínuo da massa de água devido devido ao Sol (que aquece a água dos oceanos) 
    - fenómenos de evaporaçao, fusão, transpiraçao, precipitação, infiltraçao e escorrência


- atmosfera
    - mistura de gases que envolvem a Terra
    - constituida por azoto (78%), oxigénio (21%), argon (<1%), dióxido de C (<0.1%), vapor de água (% variável), e outros gases
    - camadas: troposfera (0-12Km), estratosfera(12-50Km), mesosfera (50-80Km), termosfera (80-700Km), exosfera (700-10 000Km)
    - progressiva diminuição da pressão, T e densidade mas não é uma variação uniforme
    - a troposfera contém ~80% da massa da atmosfera e praticamente todo o vapor de água (pelo que é aí que se dão os fenómenos do tempo metereológico; separada da estratosfera pela tropopausa).<br>
    a estratosfera contém a camada do ozono, uma região com concentrações relativamente altas desse gás, O3, que é importante na filtração de radiação ultravioleta da luz solar<br>


- geosfera
    - parte sólida da Terra
    - estrutura em camadas com diferentes caracteristicas
    - mecanicamente dividida na litosfera (0-~100Km), astenosfera (100-350Km), mesosfera (350-2900Km), nucleo externo (2900-5100Km) e nucleo interno (5100-6370Km)

extra:<br>
    - quais são os limites/fronteiras da Terra e de cada um dos subsistema?<br>
    - quais são os continentes e os oceanos da Terra?<br>
    - porque água dos oceanos é salgada? Que quantidade de sais contem? O que é água doce?<br>
    - quais os requesitos para água ser potável?<br>
    - porque é que nas regiões polares da Terra (altas latitudes), a água está sobretudo sob a forma de gelo?<br>
    - como tem variado a "velocidade" do ciclo da água ao longo dos tempos?<br>
    - como se determinam os fluxos subterrâneos de água?<br>
    - definiçao de corpos de água (rio, lagos, lagoa, laguna)?<br>
    - Quais os efeitos do ozono na troposfera e na estratosfera? Porque se acumula na camada de ozono? Como filtra a radiação solar?<br>

links:<br>
https://en.wikipedia.org/wiki/Hydrosphere<br>
https://en.wikipedia.org/wiki/Water<br>
https://en.wikipedia.org/wiki/Saline_water<br>
https://en.wikipedia.org/wiki/Fresh_water<br>
https://en.wikipedia.org/wiki/Drinking_water<br>
https://www.britannica.com/video/179581/explanation-seawater<br>
https://en.wikipedia.org/wiki/Glacier<br>
https://en.wikipedia.org/wiki/Polar_regions_of_Earth<br>
https://en.wikipedia.org/wiki/Cryosphere<br>
https://en.wikipedia.org/wiki/Atmosphere_of_Earth<br>
https://en.wikipedia.org/wiki/Weather<br>
https://en.wikipedia.org/wiki/Ozone<br>
https://en.wikipedia.org/wiki/Ozone_layer<br>
https://en.wikipedia.org/wiki/Structure_of_Earth<br>
https://www.e-education.psu.edu/marcellus/node/870<br>


## aula 1

TODO

## aula 2

TODO

## aula 3

TODO

## aula 4

TODO

## aula 5

TODO

## aula 6

TODO

## aula 7

TODO

## aula 8

TODO

## aula 9

TODO

## aula 10

TODO

## aula 11

TODO

------------------------

**TESTE 1**

------------------------

## aula 12 - rochas metamórficas
**definiçao de metamorfismo**

O metamorfismo ocorre quando a rocha é exposta a um ambiente com características físicas e químicas substancialmente diferentes daquelas sob as quais se formou, fazendo com que a  rocha sofra alteraçoes na sua textura e composiçao mineralógica, sem que ocorra fusao.<br>
A rocha original que foi submetida ao metamorfismo é designada de protólito.<br>
O protólito pode ser qualquer tipo de rocha, sendo geralmente possível identificar essa natureza. 

- definiçao 1<br>
mudanças ocorridas nas rochas relativamente à mineralogia, textura e/ou composição, que ocorrem predominantemente no estado sólido, entre as condições de diagénese e de fusão.

- definiçao 2<br>
mudanças de textura, composição mineralógica e/ou química, sofridas por uma rocha, por efeito do aumento da pressão, temperatura e/ou circulação de fluídos para valores diferentes daqueles sob os quais a rocha se formou originalmente.

- definição IUGS-SCMR<br>
Metamorfismo é um processo subsolidus que induz modificações na mineralogia e/ou textura (p.ex. tamanho de grão) e frequentemente na
composição química de uma rocha. Estas mudanças são devidas a condições físicas e/ou químicas que diferem das que normalmente ocorrem à superfície de planetas e em zonas de cimentação e diagénese abaixo desta superfície. Elas podem coexistir com fusão parcial.


The Subcommission on the Systematics of Metamorphic Rocks (SCMR) is a branch of the IUGS Commission on the Systematics in Petrology (CSP).

**limites do metamorfismo**

A diagénese e a meteorização são também processos que alteram a textura e/ou mineralogia das rochas. Contudo, restringimos esses processos aos que ocorrem a temperaturas inferiores a 200ºC e pressões inferiores a cerca de 300 MPa, o que equivale a cerca de 3.000 atmosferas de pressão.<br>
Considera-se que o metamorfismo ocorrerá a temperaturas e/ou pressões superiores a 200ºC e 300 MPa respectivamente, variando o seu início de acordo com a natureza do protólito.


O domínio do metamorfismo é entre o:
- limite inferior<br>
100º a 150º para os protólitos mais instáveis com formação de minerais (como laumontite, analcite, paragonite prenite, pumpeleíte, etc.)
- limite superior<br>
pode começar aos 600ºC em granitos ricos em minerais hidratados<br>
O limite superior de metamorfismo ocorre à pressão e temperatura de fusão parcial "húmida" da rocha em questão. Uma vez iniciada a fusão, o processo muda para um processo ígneo e não para um processo metamórfico.

![dominio-metamorfismo](img/geo/dominio-metamorfismo.png)

**agentes de metamorfismo**

Durante o metamorfismo, o protólito sofre alterações na textura e/ou na composição mineral, que ocorrem principalmente no estado sólido e são causadas por mudanças nas condições físicas ou químicas.<br>
Estas mudanças de condiçoes podem ser provocadas pelo enterramento, stress tectónico, aquecimento proveniente de intrusões de magma ou interacções com fluidos.<br>
Cada uma dessas situaçoes envolve um ou mais agentes de metamorfismo:
- o calor

    a exposiçao das rochas a diversas fontes de calor, como as resultantes do aumento de profundidade ou do contacto com intrusoes magmáticas, conduzem a um aumento progressivo da sua temperatura.<br>
    Este aumento de T leva a que os elementos da rede cristalina de alguns minerais que constituem as rochas preexistentes passem a dispor-se segundo novos arranjos, sem que ocorra a fusao da rocha.<br>
    Este processo de recristalizaçao permite assim a formaçao de novos minerais mais estáveis nas novas condiçoes e consequentemente, a formaçao de novos tipos de rochas.
- a pressão
- a deformação
- os fluidos
    
    a circulaçao de fluidos nas rochas pode desencadear ou acelerar processos metamorficos.<br>
    Os fluidos hidrotermais transportam em soluçao nao só o dióxido de carbono mas também outras substancias como sódio, potássio, cobre, zinco ou sílica.<br>
    Os fluidos, ao movimentar-se através da rocha, vao reagindo com os minerais podendo originar novos minerais por remoçao ou introduçao de determinados elementos quimicos na sua rede cristalina.<br>
    Conduzem portanto a alteraçoes ao nivel da composiçao quimica/mineralogica da rocha inicial sem que ocorra alteraçao da sua textura.

A composição e características fisicas dos protólitos, e o tempo, influenciam a rocha resultante mas nao são considerados fatores de metamorfismo/agentes externos.

A pressao e a deformaçao podem ser agrupados num único factor "tensao". As tensoes podem ser de vários tipos:
- litostática<br>
    é nao-dirigida.<br>
    As rochas que estao subterradas ficam sujeitas á carga da massa rochosa suprajacente, que gera tensoes que se fazem sentir em todas as direçoes.<br>
    Esta tensao provoca sobretudo diminuiçao do volume das rochas e consequente aumento da sua densidade

- nao litostática<br>
    sao dirigidas (atuam sobre uma orientaçao bem definida)<br>
    tende a fazer com que os minerais se orientem segundo planos paralelos, prependiculares á direçao de esforço máximo (textura apelidada de foliaçao).<br>
    Pode ser um esforço/forças/natureza:
    - compressivo
    - distensivo
    - de cisalhamento


**tipos metamorfismo de acordo com o principal agente metamorfico**
- térmico<br>
    principal é o calor/T

- dinâmico<br>
    principal sao as tensoes

- dinâmico-termal<br>
    calor e tensoes sao relevantes

- metassomatismo<br>
    alteraçao quimica de uma rocha por fluidos hidrotermais ou outros fluidos.<br>
    Ou seja, o principal sao os fluidos circulantes<br>
    O metamorfismo hidrotermal é causado por fluidos quentes ricos em água

**metamorfismo regional vs de contacto**

O que aprendemos no secundário foi a distinçao de dois tipos principais de metamorfismo: regional e de contacto.<br>
Esta classificaçao é baseada na intensidade relativa dos fatores de metamorfismo:
- de contacto<br>
    este tipo de metamorfismo ocorre essencialmente nas proximidades da instalaçao de corpos magmáticos intrusivos, sendo portanto, de carácter localizado.<br>
    Os fatores determinantes sao o calor e a circulaçao de fluidos provenientes da intrusao magmatica. Como as tensoes nao estao envolvidas, as rochas formadas caracterizam-se por ausencia de foliaçao, exibindo uma textura nao foliada.<br>
    As rochas encaixantes do corpo magmático sao metamorfizadas ao longo de uma área designada por auréola de metamorfismo<br>
    As rochas genericamente conhecidas por corneanas resultam da alteraçao de rochas que se encontram perto de uma intrusao magmática. Num sentido mais restrito, o termo corneana poderá adquirir diferentes significados consoante a rocha que sofre metamorfismo:
    - uma corneana pelítica, resulta do metamorfismo de contacto de um argilito
    - quartzitos e mármores, resultam da recristalizaçao de arenitos ricos em quartzo e de calcários, respetivamente

- regional<br>
    atua em extensas áreas. É caracteristico de limites tectónicos convergentes e está associado a fenómenos orogénicos (edificaçao de cadeias montanhosas) e á formaçao de arcos vulcanicos<br>
    resulta da açao combinada do calor, dos fluidos e das tensoes dirigidas. Consequentemente, as rochas apresentam uma textura foliada.<br>
    Exemplos de rochas: a ardósia, o micaxisto e o gnaisse<br> 



**tipos de metamorfismo segundo IUGS-SCMR**
- de contacto
- regional
- hidrotermal
- de zona de falha
- de impacto

**metamorfismo de contacto**

![metamorfismo-contacto-rochas](img/geo/metamorfismo-contacto-rochas.png)

----- mal explicado. shale, limestone, sandstone?
https://pt.wikipedia.org/wiki/Rocha_cl%C3%A1stica

rochas resultantes:
- em rochas carbonatadas => skarn/escarnito

![metamorfismo-contacto-carbonatadas](img/geo/metamorfismo-contacto-carbonatadas.png)

- em rochas pelíticas => corneana (hornfels)

![metamorfismo-contacto-peliticas](img/geo/metamorfismo-contacto-peliticas.png)


**metamorfismo regional**

O metamorfismo regional é frequentemente uma combinação de metamorfismo orogénico e de contacto

![metamorfismo-regional](img/geo/metamorfismo-regional.png)

- orogénico<br>
origina rochas com anisotropias, foliadas

![metamorfismo-orogenico](img/geo/metamorfismo-orogenico.png)

- de afundamento (baixo grau)<br>
ocorre em bacias sedimentares devido ao afundamento dos sedimentos<br>
Coombs, 1961

![metamorfismo-afundamento](img/geo/metamorfismo-afundamento.png)

- de fundo-oceânico/crista-oceânica<br>
metamorfismo que afecta a crusta oceânica nas cristas médias oceânicas<br>
metassomatismo (reacções de troca entre basalto e água quente), com perda de Ca e Si e ganho de Mg e Na na maioria dos casos<br>
Miyashiro et al, 1971<br>
Este metamorfismo pode resultar em fontes hidrotermais/fumarolas negras

![metamorfismo-oceanico](img/geo/metamorfismo-oceanico.png)

![metamorfismo-oceanico2](img/geo/metamorfismo-oceanico2.png)

A black smoker or deep sea vent is a type of hydrothermal vent found on the seabed, typically in the bathyal zone (with largest frequency in depths from 2500 m to 3000 m), but also in lesser depths as well as deeper in the abyssal zone. They appear as black, chimney-like structures that emit a cloud of black material. Black smokers typically emit particles with high levels of sulfur-bearing minerals, or sulfides. Black smokers are formed in fields hundreds of meters wide when superheated water from below Earth's crust comes through the ocean floor (water may attain temperatures above 400 °C). This water is rich in dissolved minerals from the crust, most notably sulfides. When it comes in contact with cold ocean water, many minerals precipitate, forming a black, chimney-like structure around each vent. The deposited metal sulfides can become massive sulfide ore deposits in time. Some black smokers on the Azores portion of the Mid Atlantic Ridge are extremely rich in metal content, such as Rainbow with 24,000 μM concentrations of iron.<br>
https://en.wikipedia.org/wiki/Hydrothermal_vent

![formation-black-smokers](img/geo/formation-black-smokers.png)

**metamorfismo em zonas de falha**

- falhas pouco profundas<br>
originam uma brecha de falha

- falhas mais profundas (erosão)<br>
ocorre deformação dúctil com formação de milonitos

![cross-section-fault-zone](img/geo/cross-section-fault-zone.png)

--- erosao???

**metamorfismo de impacto**

devido ao choque de um meteorito<br>
Há elevada deformação; metamorfismo dinâmico

![metamorfismo-impacto](img/geo/metamorfismo-impacto.png)

**fácies de metamorfismo**

![faceis-metamorfismo-1](img/geo/faceis-metamorfismo-1.png)

![faceis-metamorfismo-2](img/geo/faceis-metamorfismo-2.png)

**classificação de rochas metamórficas**

critérios:
- mineralógicos<br>
    quartzito: rocha original constituída essencialmente por quartzo<br>
    mármore: rocha ... calcite<br>
    anfibolito: rocha ... anfíbola

- de génese<br>
    corneana: rocha formada por metamorfismo de contacto

- mineralógico-texturais<br>

    Em quase todos os casos, uma rocha metamórfica tem uma textura claramente diferente da da rocha-mãe original. Por exemplo, quando o calcário é metamorfisado em mármore, os finos grãos de calcite coalescem e recristalizam em cristais de calcite maiores. Os cristais de calcite são entrelaçados num padrão de mosaico que dá ao mármore uma textura notoriamente diferente da do calcário de base. Se o calcário é composto inteiramente de calcite, então o metamorfismo em mármore não envolve a formação de novos minerais, apenas uma mudança na textura.<br>

    filito e micaxisto: rochas formadas essencialmente por micas e vulgarmente designados por xistos, apresentando micas observáveis a olho nu e xistosidade no caso dos micaxisto ou sem se observarem as micas e com clivagem de fluxo, caso dos filitos.<br>
    gnaisse : rocha formada por quartzo, feldspato (>20) e mica, com estrutura bandada.

    O metamorfismo extremo, onde a rocha funde parcialmente, pode resultar na formação de migmatitos.

presença de foliação (textural)
- foliadas
    - ardósia<br>
    composiçao: argilas, micas (biotite, moscovite), clorite
    rocha-mae: argilitos, cinza vulcanica
    caracteristicas: grao fino, foliaçao evidente (clivagem xistenta)
    tipo de metamorfismo: regional

    - xisto/micaxisto<br>
    composiçao: micas, clorite, quartzo, talco, granada, estaurolite, grafite
    rocha-mae: argilitos, rocha carbonatadas, rochas igneas máficas (eg. basalto)
    caracteristicas: grao médio a grosseiro, foliaçao evidente (xistosidade)
    tipo de metamorfismo: regional

    - gnaisse<br>
    composiçao: quartzo, feldspato, hornblenda, micas
    rocha-mae: argilitos, arenitos, rocha igneas félsicas (eg. granito)
    caracteristicas: grao médio a grosseiro, foliaçao evidente, com alternancia de faixas claras e escuras (bandado gnaissico)
    tipo de metamorfismo: regional

- não foliadas
    - corneana pelítica<br>
    composiçao: micas, granadas, andaluzite, cordierite, quartzo
    rocha-mae: argilitos, rochas carbonatadas
    caracteristicas: grao fino, elevada dureza
    tipo de metamorfismo: contacto

    - quartzito<br>
    composiçao: quartzo
    rocha-mae: arenitos quartzosos
    caracteristicas: grao medio, elevada dureza
    tipo de metamorfismo: regional ou de contacto

    - mármore<br>
    composiçao: calcite e dolomite
    rocha-mae: calcários e dolomitos
    caracteristicas: grao medio, reage com ácidos
    tipo de metamorfismo: regional ou de contacto

![classificaçao-metamorficas](img/geo/classificaçao-metamorficas.png)

![exemplos-metamorficas](img/geo/exemplos-metamorficas.png)


**metamorfismo e tectónica de placas**

A teoria da tectónica das placas explica as características observadas nas rochas metamórficas e relaciona o seu desenvolvimento com outras actividades na Terra. Em particular, a tectónica de placas explica:
- o enterramento profundo das rochas originalmente formadas à superfície da Terra ou na sua proximidade;
- a intensa pressão necessária para o stress diferencial, implícito nas rochas foliadas;
- a presença de água nas profundezas da litosfera;
- a grande variedade de pressões e temperaturas que se acredita estarem presentes durante o
metamorfismo.

**graus de metamorfismo e minarais-indice**

alguns minerais, designados por minerais-índice, formam-se em condiçoes termodinamicas com limites bem definidos, permitindo por isso caracterizar as condiçoes de pressao e temperatura em que a rocha que os apresenta se formou<br>
feldspato e quartzo nao sao minerais indice (sao estáveis num extenso intervalo de p e T)<br>
Os minerais-indice permitem establecer os diferentes graus de metamorfismo:
- baixo grau (200ºC)<br>
    podem estar presentes argila, clorite, micas (juntamente com os feldspatos e quartzo)<br>
    originando ardósias, com cristais de grao fino
- médio grau<br>
    podem estar presentes granada, estaurolite, cianite<br>
    formam micaxistos
- alto grau (800ºC)
    silimanite<br>
    formam gnaisse



## aula 13 - estruturas geológicas
............. yup na vou ter tempo para estas andanças

# Referencias
- slides das aulas e resumos prof (principal)
- mta cena tirada da wiki
- mta cena do livro "Reis - Preparaçao para o exame nacional Biologia e Geologia 2020"