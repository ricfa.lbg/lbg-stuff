# Botanica

_essencialmente copy-paste dos slides_

# 0 - Motivaçao
- porquê estudar plantas? ~~fazer o curso...~~
    - para satisfaçao intelectual/estética<br>
    apresentam caracteristicas muito diversas, sobrevivem em diferentes habitats<br>
    sao organismos espetaculares:<br>
        mais longo > 100m<br>
        mais velho > 5000anos<br>
    - necessárias para a sobrevivencia Humana
        - produzem a maior parte do oxigénio que respiramos
        - produzem a maior parte dos glúcidos que comemos<br>
        na fotossintese, fixam o C02, ou seja, convertem-no em glucidos<br>
        são alimentos (frutas, legumes, pao,...), bebidas (chá, chocolate, café, sumos,...), especiarias
        - produzem outros compostos úteis<br>
        vitamina A, vitamina C, vanilina, cafeina<br> que entram em medicamentos (morfina, aspirina, digitoxina, quinina,...), perfumes, plásticos (a partir de materiais vegetais biodegradaveis), óleos, borrachas, ceras, vestuário (algodão, linho), madeira (construçao, mobiliario, aquecimento), papel/papiro (fibras vegetais), biocombustivel (açucares, amido e celulose podem ser fermentados em etanol; turfa usada para aquecimento), decoraçao e afins
        - têm impacto ambiental<br>
        evitam a erosao do solo, abrandam o vento, constituem o habitat de muitas espécies, são reservatorios de CO2<br>
        podem ser contaminantes e infestantes (eg. ervas daninhas)
    - podem ser patogenicas
        - venenos potentes
        - alergias
    - para preservar/melhorar a resistencia de plantas em ambientes ameaçados<br>
    e assim ser capaz de fornecer alimento, medicamentos e energia a uma população crescente...


# 1 - Taxonomia vegetal

- Sistemas de classificação
    - Jonh Ray<br>
    foi quem começou a evidenciar as relaçoes entre plantas
    - Linnaeus (Lineu)<br>
    o seu livro Systema Naturae usava um sistema pouco natural para agrupar as plantas (apenas caracteristicas reprodutivas)<br> 
    origem do epíteto específico<br>
    eg. Mentha piperita em vez de Menta floribus capitatus, follis lanceolatis serratis subpetiolatis
    - filogenéticas<br>
        inicio com Darwin e Wallace<br>
        são complicadas pela existencia de características homólogas e análogas, e ao facto de algumas plantas de um mesmo grupo taxonómico apresentarem morfologias distintas<br>
        Numa árvore filogenética, cada taxon deve ser monofilético. Os taxa polifiléticos e parafiléticos são artificiais (não-naturais) => nao apresentam valor taxonómico.<br>
        eg. Protista, briófitas
        - Whittaker - 5 reinos<br>
            - Morena (procariotas)
            - Protista (eucariotas unicelulares)<br>
                - protozoários (sem parede celular)
                - algas (com parede)
            - Fungi (eucariotas sem clorofila)
            - Plantae - plantas
            - Animalia - animais
        - Woese - 3 domínios
            - Archaea (procariotas)
            - Bacteria (procariotas)
            - Eukarya (eucariotas)
        - mais esquemas usando sistemática molecular<br>
            eg. em relaçao ao citocromo c ou á subunidade menor da RuBisCo
    - taxonomia atual<br>
    International Copde of Botanical Nomenclature, 1993<br>
    contempla os seguintes taxa (plural de taxon, os nomes das categorias), escritos em itálico
        - Reino
        - Divisao/Filo (terminaçao -phyta)
        - Classe
        - Ordem (terminaçao -ales)
        - Familia (terminaçao -aceae)
        - Genero
        - Especie<br>
        
        eg. Zea mays (milho)
        - Reino: Plantae
        - Filo: Anthophyta
        - Classe: Monocotyledones
        - Ordem: Commeniales
        - Familia: Poaceae
        - Genero: Zea
        - Especie: Zea mays

- Plantas - reino Plantae<br>
    maioria sao multicelulares, com grandes vacúolos e parede celular com celulose<br>
    maioria tem clorofila a e b => capacidade fotossintetica, mas algumas passaram a heterotróficas (eg. plantas carnívoras)
    - descendem das algas verdes ou clorofitas - filo Chlorophyta<br>
    as algas não sao plantas mas são estudadas na botanica<br>
    têm clorofilas a e b
    - plantas terrestres (embriófitas)<br>
        embrião multicelular<br>
        alternação de geraçoes (são haplodiplontes, e produzem anterideos e arquegónios)<br>
        esporos revestidos de esporopolenina<br>
        (nao necessario saber mas) têm plasmodesmata e cutículo<br>
        - briófitos<br>
            os briófitos/as sao só as hepáticas, musgos e antoceros. As plantas vasculares estao como filha para transmitir a idea que descendem de um ancestral comum com origem nas briófitas.<br>
            as hepáticas e antoceros têm talo (o gametófito geralmente é achatado e ramificado dicotómicamente)<br>
            os musgos e as hepáticas têm filídeos (o gametófito parece apresentar "caules" - cauloides, e "folhas" - filideos, mas não apresentam vasos condutores)<br>
            apenas nos musgos, os caulóides (seta/seda) alongam gradualmente e a cápsula expande após terminarem de alongar. A caliptra é persistente na cápsula<br>
            (nao necessario saber mas) os musgos e antoceros têm estomas<br>
            - hepáticas ("liverworts") - filo Hepatophyta
                - Marchantia polymorpha (1 espécie especifica)
            - musgos ("mosses") - Bryophyta<br>
                nao confundir briófitos (designação comum, sem valor taxonómico) com a divisao Briofita (sem s)
                - Funaria (género, a familia seria Funariaceae)
                - Polytrichum
                - Sphagnum
            - antoceros ("hornworts") - Anthocerophyta
                - Anthoceros
            - plantas vasculares (traqueófitas)<br>
                têm tecido vascular (xilema e floema verdadeiros) => têm caules, raizes e folhas<br>
                lignina verdadeira<br>
                esporófito dominante e independente, ramificado e com multiplos esporangios<br>
                (nao é preciso daber mas) só os fetos têm folhas verdadeiras
                - licófitos ("club mosses") - Licopodiophyta
                    - Selaginella (selaginelas)
                    - Lycopodium (licopódios)
                - fetos ("ferns") - Pteridophyta<br>
                    o Psilotum e o Equisetum não são considerados fetos mas estão no mesmo filo (são "aliados dos fetos")<br>
                    - Polypodium (polipódio)
                    - Psilotum (psilotales)
                    - Equisetum (equisetales)
                - plantas com semente<br>
                    produzem sementes<br>
                    - gimnospermas/gimnospermicas
                        - Cycadophyta (cicádias)
                        - Ginkgophyta (ginko)
                        - Conipherophyta (coníferas)
                        - Gnetophyta (gnetófitas)
                    - Antophyta (angiospermicas == plantas com flor)<br>
                    as angiospérmicas têm flores e fruto

- Protistas - Reino Protista
    - protistas heterotróficos (anteriormente considerados fungos)
        - Myxomycota ("plasmodial slime molds")
        - Dictyostellomycota ("cellular slime molds")
    - Algas - Algae
        - as que são necessário saber:
            - Clorophyta (algas verdes, clorófitas)
                - Chlamydomonas
                - Volvox
                - Hydrodictyon
                - Oedogonium
                - Cladophora
                - Ulva
                - Spirogyra
                - Chara
            - Phaeophyta (castanhas, feófitas)
                - Ectocarpus
                - Laminaria
                - Fucus
            - Rhodophyta (vermelhas, rodófitas)
                - Polysiphonia 
        - mas há 9 divisões
            - Dinophyta (dinoflagelados)
            - Euglenophyta (euglenofitos)
            - Cryptophyta (criptomonados)
            - Haptophyta (haptófitos)
            - Bacillariophyta (diatomos)
            - Chrysophyta (crisófitos)
            - Oomycota ("water molds") - era considerado um fungo; a prof nao considera uma alga, dai serem 9
        - as divisões sao distinguidas por
            - pigmentos fotossintéticos
            - polímeros de reserva
            - componentes da parede celular
            - número e tipo de flagelos

- Fungos - Reino Fungi
    - Chytridiomycota (chitridiomicetes)
    - Zygomycota (zigomicetes)
    - Ascomycota (ascomicetes)
    - Basidiomycota (basidomicetes, teliomicetes, ustomicetes)


# 2 - Associaçoes com plantas e ciclos de vida

- ciclos de vida<br>
    num ciclo de vida, há alternancia de fases nucleares (fase haploide n e fase diploide 2n) devido á ocorrência de meiose e conjugaçao<br>
    esta alternancia permite que o número de cromossomas se mantenha constante<br>
    uma geração é um organismo pluricelular (de vida livre ou nao), que constitui uma parte de um ciclo de vida<br>
    diferentes tipos de ciclos de vida podem ser distinguidos com base na altura em que a meiose ocorre
    - haplonte
        - meiose (pós-)zigótica
        - predominancia da fase nucelar haploide (n)
        - ocorre em: fungos, algumas plantas
    - diplonte
        - meiose (pré-)gamética
        - predominancia da fase nucelar diploide (2n)
        - ocorre em: animais, alguns protozoários e algas
    - haplodiplonte
        - meiose (pré-)espórica
        - alternancia de gerações (individuos multicelulares com fase nuclear haploide e diploide)
        - ocorre em: plantas e muitas algas
        - geraçao esporofita (2n)<br>
            inicia-se no zigoto/ovo e termina nos esporos
            - o zigoto resultou da fertilizaçao/conjugaçao do gametas (2x n => 2n)
            - o zigoto, por mitose forma o esporofito
            - o esporofito (2n) gera...
        - geraçao gametófita (n)<br>
            inicia-se nos esporos e termina nos gametas
            - ...esporos (n) por meiose (meiósporos, 2n => 4x n)
            - os esporos formam gametófitos (masculino e feminino) por mitose
            - o gametófito (n) gera gâmetas (n) por mitose
            - eles eventualmente encontram-se e o nucleo funde na conjugaçao

    - ciclos de vida das plantas com flor
        - ciclo de crescimento anual (anuais)
        - bianuais
        - perenes/vivazes
    
    - estruturas do haplodiplonte
        - geração gametófita<br>
            esta geração forma gametas, nos gametângios<br>
            - gametângios
                - femininos<br>
                originam oosferas<br>
                apelidados oogónios (unicelulares) ou arquegónios (pluri)
                - masculinos=anterídeos<br>
                originam anterozóides (flagelados) ou espermácios (nao-flagelados)
            - forma dos gâmetas
                - isogamia<br>
                gametas indistintos, tipos parentais + e -
                - anisogamia<br>
                ambos flagelados mas um deles (masculino) menor que o outro
                - oogamia<br>
                gameta feminino nao flagelado (é maior e imóvel)

        - geração esporofita<br>
            esta geração forma esporos, nos esporângios<br>
            - os esporos podem ser
                - móveis (zoósporos) vs nao-moveis (aplanósporos)
                - resultar de reproduçao assexuada (zoosporos) vs sexuada (meiosporos, resultantes da meiose)

- relações simbióticas<br>
    simbiose = vivência em conjunto
    - parasitismo<br>
    um organismo beneficia da relação, o outro é prejudicado
    - comensalismo<br>
    um beneficia, o outro é indiferente
    - mutualismo<br>
        ambos benificiam
        - liquenes<br>
            - associaçoes entre um micobionte (um fungo), que constitui a maior parte do liquen, e um fotobionte (capaz de fazer fotossintesse).<br>
            o fungo obtém nutrientes do fotobinte, o fotobionte obtém água e minerais<br>
            - pode ser alga verde (protista fotobionte) + fungo, ou cianobacterias (bacteria fotobionte) + fungo
            - nome cientifico baseado no fungo  (98% ascomicetes, 2% basidiomicetes)
            - diferentes formas
                - encrustantes/crustáceos
                - foliáceos/folhosos
                - fruticulosos/fruticosos
            - reproduçao<br>
                podem apresentar a reproduçao tipica dos fungos que os constituem; processo mais comum de assexuada é reproduçao vegetativa através do desenvolvimento de pequenos granulos (sorédios), compreendendo ambos os simbiontes<br>
        
        <img src='img/botanica/liquenes0.png' width='500'><br>
        <img src='img/botanica/liquenes1.png' width='500'><br>
        <img src='img/botanica/liquenes2.png'><br>

        - micorriza (planta-fungo)<br>
            a planta obtem água e nutrientes e proteçao contra stress ambiental e biótico; o fungo obtém fotoassimilados da planta<br>
            - endomicorriza (penetram nas células das raizes)<br>
            a transferencia dos nutrientes ocorre nos arbusculos<br>
            o fungo é um zigomiceto<br>
            - ectomicorriza<br>
            as hifas nao penetram nas células, formam apenas uma rede (rede de Hartig) que as rodeia<br>
            o fungo é geralmente um basideomiceto ou um ascomiceto<br>
        
        <img src='img/botanica/micorriza0.png' width='500'><br>

# 3 - A diversidade das algas

- Chlorophyta (clorófitas)
    - apresenta muitas das caracteristicas encontradas nas plantas => estarao na origem deste grupo
    - distinçao
        - pigmentos fotossinteticos: clorofila a e b
        - carbohidrato de reserva: amido
        - componentes da parede celular: celulose
        - flagelos: 0 ou 2+
    - alga unicelular **Chlamydomonas**
        - reproduçao sexuada: ciclo haplonte
            - isogamia
            - tubo de conjugaçao
        - caracteristicas
            - 2 flagelos
            - 1 único cloroplasto, que contém um pirenóide (acumulação da enzima RuBisCo e amido)
            - 1 estigma, com fotorreceptores<br>
    
    <img src='img/botanica/chlamydomonas0.png' width='500'><br>
    <img src='img/botanica/chlamydomonas1.png' width='500'><br>
    <img src='img/botanica/chlamydomonas2.png' width='500'><br>

    - alga colonial **Volvox**
        - milhares de células numa forma esférica, unidas por filamentos citoplasmáticos
        - reproduçao sexuada: ciclo haplonte
            - oogamia
            - colónias podem ser unisexuais ou bisexuais
        - reproduçao assexuada<br>
        forma colónias-filhas no interior, que são libertadas após desintegraçao da colónia parental<br>
    
    <img src='img/botanica/volvox0.png' width='250'><br>

    - alga cenocítica colonial **Hydrodictyon**
        - reproduçao sexuada: ciclo haplonte
            - isogamia
        - reproduçao asexuada<br>
        a célula é clivada em vários fragmentos uninucleados, que adquirem flagelos (zoósporos)<br>
    
    <img src='img/botanica/hydrodictyon0.png' width='500'><br>
    <img src='img/botanica/hydrodictyon1.png' width='250'><br>

    - alga filamentosa **Oedogonium**
        - reproduçao sexuada: ciclo haplonte
            - gametângio feminino são oógonios
            - oogamia
        - reproduçao assexuada<br>
        através de zoósporos<br>
    
    <img src='img/botanica/oedogonium0.png' width='500'><br>
    <img src='img/botanica/oedogonium1.png' width='500'><br>

    - alga cenocitica, filamentosa, ramificada **Cladophora**
        - reproduçao sexuada: ciclo haplodiplonte
            - isogamia
            - geraçoes isomórficas
            - gametas com 2 flagelos, esporos com 4 flagelos<br>
    
    <img src='img/botanica/cladophora0.png' width='500'><br>

    - alga multicelular **Ulva**
        - reproduçao sexuada: ciclo haplodiplonte
            - tudo = á cladophora mas multicelular (isogamia; geraçoes isomórficas; gametas com 2 flagelos, esporos com 4 flagelos)
        - caracteristicas
            - individuos formados por 2 camadas de células<br>
    
    <img src='img/botanica/ulva0.png' width='500'><br>

    - alga filamentosa unicelular colonial **Spirogyra**
        - reproduçao sexuada: ciclo haplonte
            - tubo de conjugaçao<br>
            dois filamentos entram em contacto<br>
            emissao de um tubo de conjugação entre células adjacentes de filamentos diferentes<br>
            passagem do protoplasma de uma das células (gameta feminino/-) através do tudo de conjugação, fundindo-se com a célula do outro filamento (gameta masculino/+) e formando o zigoto<br>
            meiose do zigoto e formaçao de novos filamentos
            - das 4 células da meiose, 3 degeneram
        - reproduçao assexuada<br>
        formaçao de novos filamentos por fragmentaçao
        - características
            - de água doce
            - células vegetativas com cloroplastos em forma de hélice, associados a pirenóides (síntese de amido)
            - nao tem flagelos<br>
    
    <img src='img/botanica/spirogyra0.png' width='250'><br>
    <img src='img/botanica/spirogyra1.png' width='500'><br>
    <img src='img/botanica/spirogyra.png' width='500'><br>

    - alga multicelular filamentosa ramificada **Chara**
        - reproduçao sexuada: ciclo haplonte
            - é unica nao algas - anterideos e arquegónios multicelulares
        - caracteristicas
            - frequentemente confundida com uma planta aquatica devido á sua aparencia
            - são as algas mais próximas das plantas (únicas capazes de produzir flavenóides)<br>

    <img src='img/botanica/chara0.png' width='500'><br>

- Phaeophyta (feófitas)
    - distinçao
        - pigmentos fotossintéticos: clorofilas a e c;<br>
        carotenóides, principalmente a fucoxantina, que lhe dá a cor castanha<br>
        - polimero de reserva: laminina, manitol
        - componentes da parede: celulose embutida numa matriz de algina muciloginosa
        - flagelos: células flageladas (zoosporos e gametas) tipicos, com 2 flagelos laterais, um deles franjado
    - caracteristicas
        - maioria das espécies sao marinhas
        - maioria apresenta alternancia de geraçoes, com vida independente
        - algumas são monóicas (1 só individuo/gametófito produz oógonios e anterídeos, ou seja, tem ambos os orgaos sexuais, pelo que o gametófito é hermafrodita), outras são dióicas (espécie com individuos de 2 sexos)
    - alga multicelular filamentosa ramificada **Ectocarpus**
        - reproduçao sexuada: ciclo haplodiplonte
            - gerações isomórficas
            - os gametângios e esporângios são a mesma estrutura (tanto pode formar esporos de dispersão como gâmetas). São pluriloculares (subdividida em várias células, ou seja, pluricelulares)
        - reproduçao assexuada<br>
        zoosporos<br>
    
    <img src='img/botanica/ectocarpus0.png' width='500'><br>
    <img src='img/botanica/ectocarpus1.png' width='500'><br>

    - alga multicelular **Laminaria**
        - reproduçao sexuada: ciclo haplodiplonte
            - gerações heteromórficas
            - esporângios uniloculares
        - caracteristicas
            - estrutura interna lembra vasos condutores<br>
    
    <img src='img/botanica/laminaria0.png' width='250'><br>
    <img src='img/botanica/laminaria1.png' width='500'><br>

    - alga multicelular **Fucus**
        - reproduçao sexuada: ciclo diplonte<br>
        geração diploide constituida por um talo, com ramificaçao dicotómica, que possui um disco de fixaçao ao substrato<br>
        os gametângios localizam-se em estruturas apicais (receptáculos). Na perifieria dos receptáculos encontram-se conceptáculos, estruturas globosas que possuem uma abertura para o exterior, o ostíolo.<br>
        Pode ser hermafrodita, caso em que conseguimos ver conceptáculos contendo gametângios (oogonios e anterideos) e filamentos estéreis (parífises, perto do ostíolo, e paráfises, na base do conceptáculo)
        - caracteristicas
            - todas as espécies são marinhas
            - flutuadores (aerocistos), cheios de ar<br>
    
    <img src='img/botanica/fucus0.png' width='250'><br>
    <img src='img/botanica/fucus1.png' width='500'><br>
    <img src='img/botanica/fucus2.png' width='500'><br>
    <img src='img/botanica/fucus3.png' width='500'><br>
    <img src='img/botanica/fucus4.png' width='500'><br>
    <img src='img/botanica/fucus5.png' width='500'><br>
    <img src='img/botanica/fucus6.png' width='500'><br>

- Rhodophyta (rodófitas)
    - distinçao
        - pigmentos fotossinteticos: clorofila a, ficobilinas e carotenóides => cor vermelha deve-se á abundância de uma ficobilina vermelha (ficoeritrina), que lhes permite absorver luz azul e viver em maior profundidade
        - polimero de reserva: amido florídeo, que se acumula no citoplasma (e não no cloroplasto)
        - componentes da parede: celulose embuida numa matriz de galactanos (usualmente)
        - flagelos: nao (nem os gametas) => oogamia
    - caracteristicas
        - muitas apresentam depósitos de carbonato de Ca
        - muitas apresentam 1 ciclo de vida com 3 fases (gametófico n, carposporófito 2n, tetrasporófito 2n) => nao existe correspondencia de geraçao cam fases nucleares
    - alga multicelular **Polysiphonia**
        - reproduçao sexuada: ciclo haplodiplonte
            - ciclo com 3 fases
            - gametófito cenocítico. O masculino apresenta espermatângios. O feminino apresenta um carpogónio, que contem a oosfera e emite o tricogénio, que permite a aderencia dos espermácios. cada carpósporo origina um individuo multicelular -  os tetrasporangios (geraçao tetrasporófitos)
            - após fertilizaçao, forma-se o carposporangio, que produz carpósporos (geração carposporófita)
            - nos tetrasporangios irá ocorrer a meiose, originando tetrasporos
            - as gerações gametófito e tetrasporófito são isomórficos<br>
    
    <img src='img/botanica/polysiphonia0.png' width='500'><br>
    <img src='img/botanica/polysiphonia1.png' width='500'><br>
    <img src='img/botanica/polysiphonia2.png' width='500'><br>
    

# 4 - A conquista do meio terrestre

- evoluçao para plantas
    - implicou a evoluçao de individuos em que todas ou a maioria das células poderiam reproduzir todo o individuo, para organismos em que a maioria das células sao somáticas e apenas uma parte são reprodutoras
    - meio terrestre tem diferentes condiçoes
        - a água era o suporte (para)=> nao existe meio de suporte
        - água disponivel a todas as células => poderá ocorrer desidratação dos tecidos em contacto com o ar
        - O2, CO2 e nutrientes estao disponiveis a todas as células, dissolvidos na agua => O2 e CO2 estao facilmente disponiveis acima do solo, mas nao nos tecidos; a água e nutrientes apenas estao disponiveis no solo
        - luz é limitante e a maioria das células fotossintetiza => a fotossintese ocorre apenas nas partes aereas nao havendo grandes limitaçoes de luz
    - adaptações/estruturas que
        - evitassem a desidratação, mas permitissem trocas gasosas - aparecimento de cutículas e estomas
        - permitissem uma maior rigidez das suas células - pressao de turgescencia promovida pelo vacuolo de grandes dimensoes
        - permitissem a atividade metabólica num ambiente mais seco - vacúolos de grandes dimensoes
        - permitissem o transporte de substancias de/para as partes aereas e subterraneas - feixes condutores (xilema e fluema)
        - permitissem uma maior rigidez da planta - células com espessamentos que fornecem rigidez ás estruturas que as contêm (xilema e esclerênquima)
    - a alga primitiva que terá dado origem á vida terrestre deverá ter pertencido á Chlorophyta e deverá ter sido semelhante á Fritschiella tuberosa.<br>
    Ela possui regioes eretas do talo e apresenta cutícula nessas regioes. Possui diferentes tipos de células (ramos e rizóides)<br>

- briófitos
    - hepáticas, antoceros e musgos 
    - caracteristicas
        - crescem em habitats muito húmidos, são importantes colonizadores inicais e são sensíveis a poluentes aéreos (tal como os líquenes)
        - há grupos com talo (hepáticas e antoceros) e grupos com filideos (hepáticas e musgos)
        - identicas ás das algas verdes
            - pigmentos foto: clorofila a e b<br>
            cloroplastos identicos ás plantas, com grana bem definidos
            - polimero de reserva: amido
            - parede celular: com celulose
            - flagelos: anterozoides flagelados
        - tipicas das plantas ("novas")
            - gametângios e esporangios pluricelulares
            - possuem cutícula a revestir parte do seu corpo
            - possuem estomas (os mugos e os antoceros) ou poros (hepáticas)
        - nao possuem
            - vasos condutores<br>
            apresentam rizoides, que os agarram ao substrato, mas não possuem estruturas próprias para a absorçao de água e nutrientes (é por capilaridade)
            - tecidos lenhificados<br>
            podem apresentar tecidos condutores incipientes mas sem lenhina
    - reproduçao sexual: ciclo haplodiplonte
        - gerações heteromórficas<br>
        a geraçao gametófita dá origem aos gametas (para aumentar variabilidade). A fecundaçao ocorre num ambiente húmido para que os gametas se encontrem<br>
        a geração esporofita dá origem aos esporos (para aumentar a disseminaçao). A dispersao dos esporos é facilitada por via aérea.
        - oogamia
        - o gametófito é de maiores dimensoes e constitui a geraçao predominante => são as unicas plantas com gametófito predominante
            - é perene e fotossintético
            - reproduz-se assexuadamente por fragmentação
            - agarra-se ao solo por rizoides
            - o protonema é a forma jovem do gametófito<br>
            depois da germinação do esporo, desenvolve-se um protonema, de que se desenvolverá depois o gametófito maduro, enquanto o protonema morre e desaparece.<br>
            O protonema é mais evidente nos musgos
            - os gametangios sao pluricelulares, com células estereis. Apresentam distinçao entre gametangio feminino e masculino, muitas vezes gametófitos separados
        - o esporofito é de menores dimensoes e está preso ao gametofito
            - é temporario, nao ramificado
            - absorve nutrientes do gametófito
            - tem poucas células fotossinteticas, pelo que é dependente do gametófito
            - após fecundação, é desenvolvido o esporofito, que é multicelular e produz um unico esporangio<br>
            o esporangio tem tecido esporogenico, que por meiose, produz uma grande quantidade de esporos<br>
            os esporos nao sao flagelados<br>
    
    <img src='img/botanica/briofitos0.png' width='500'><br>
    <img src='img/botanica/briofitos1.png' width='250'><br>
    <img src='img/botanica/briofitos2.png' width='500'><br>
    <img src='img/botanica/briofitos3.png' width='500'><br>

- Hepatophyta (hepáticas)
    - caracteristicas
        - do gametófito
            - com estrutura de corpo em talo ou folhosa
            - rizóides unicelulares
            - podem apresentar poros e gemas
        - do esporófito
            - ligado ao gametófito (talo), que pode ter anterídeos e arquegónios (bisexual) ou apenas um dos tipos (uni)
            - esporofito simples (pouco mais que o esporangio)
            - libertaçao dos esporos ocorre por morte do gametófito
            - sem estomas<br>
    
    <img src='img/botanica/hepaticas0.png' width='500'><br>
    <img src='img/botanica/hepaticas1.png' width='500'><br>

    - Marchantia polymorpha
        - os gametangios encontram-se em estruturas especializadas - os gametangióforos (arquegonióforo e anteridióforo)<br>
        os talos sao unisexuais
        - após fertilização, forma-se o esporofito.<br>
        Ele nao apresenta poros nem estomas.<br>
        Algumas células do esporangio nao sofrem meiose e diferenciam-se em elatérios (que sao higroscópicos e facilitam a dispersao dos esporos)<br>
        - reproduçao assexuada<br>
        por fragmentaçao ou produçao de gemas (ou propágulos) em cupúlas (ou taças)<br>
    
    <img src='img/botanica/marchantia0.png'><br>
    <img src='img/botanica/marchantia1.png' width='500'><br>
    <img src='img/botanica/marchantia2.png'><br>
    <img src='img/botanica/marchantia3.png' width='500'><br>
    <img src='img/botanica/marchantia4.png' width='500'><br>
    <img src='img/botanica/marchantia5.png' width='500'><br>
    <img src='img/botanica/marchantia6.png' width='500'><br>
    <img src='img/botanica/marchantia7.png' width='500'><br>

- Anthocerophyta (antoceros)
    - género Anthoceros
    - caracteristicas
        - gametófico e esporofito podem possuir estomas 
        - do gametófito
            - estrutura do corpo em talo, como as hepáticas, originados por protonemas muito reduzidos
            - têm cavidades internas, que sao colonizadas pela cianobactéria Nostoc (fixa azoto)
            - gametangios encravados na superficie do talo
            - rizóides unicelulares
        - do esporófito
            - praticamente só o esporangio
            - vários podem surgir do mesmo talo
            - esporofito consiste no pé e cápsula/esporangio alongado
            - tem tecido meristémico basal<br>
            os esporos sao formados no centro do esporangio. A maturaçao e deiscencia inicia-se da base para o topo
            - apresenta elatérios (como as hepáticas) e columela (como os musgos)<br>
    
    <img src='img/botanica/antocero0.png' width='500'><br>
    <img src='img/botanica/antocero1.png' width='500'><br>
    <img src='img/botanica/antocero2.png' width='500'><br>
    <img src='img/botanica/antocero3.png' width='500'><br>
    <img src='img/botanica/antocero4.png' width='500'><br>

- Bryophyta (musgos)
    - caracteristicas 
        - do gametófito
            - estrutura do corpo em ramos eretos - gametóforos<br>
            gametóforos diferenciados em caulóides e filídeos<br>
            filideos com 1 célula de espessura, á exceptçao da nervura média<br>
            caulóides podem apresentar um tecido condutor incipiente, apresentando hidróides
            - protonema geralmente filamentoso
            - rizóides multicelulares
            - podem produzir gemas
        - do esporófito
            - esporofito constituido por esporangio/cápsula e seta/seda<br>
            o esporangio tem uma cumela no seu interior, e é tapado pelo opérculo, que abre quando o esporangio está maduro<br>
            - o esporofito apresenta estomas (ao contrário do gametófito)
            - o peristoma é responsável pela libertação dos esporos<br>
            os seus dentes são sensiveis á humidade, libertando os esporos em condições de secura<br>
    - Funaria
        - geraçao gametófita onde vemos numa primeira fase o protonema, com gemas; a partir dai forma-se o gametóforo, com rizóides, filídeos e caulóide
        - geração esporófita dependente do gametófito; constituida por pé, seda e cápsula<br>
        a cápsula compreende o opérculo (exterior ao peristoma), a columela (eixo de suporte) e o saco esporal (onde tem lugar a meiose e ficam acumulados os esporos)<br>
    
    <img src='img/botanica/funaria0.png' width='500'><br>
    <img src='img/botanica/funaria1.png' width='500'><br>
    <img src='img/botanica/funaria2.png' width='500'><br>
    <img src='img/botanica/funaria3.png' width='500'><br>
    <img src='img/botanica/funaria4.png' width='500'><br>
    <img src='img/botanica/funaria5.png' width='500'><br>

    - Polytrichum
        - em tudo igual funcionalmente, aparencia diferente<br>

    <img src='img/botanica/polytrichum0.png' width='500'><br>
    <img src='img/botanica/polytrichum1.png'><br>

    - Sphagnum
        - a geração gametófita
            - apresenta filídeos se nervura média, com dois tipos de células - as fotossintéticas e as hialinas (absorção de água), que formam um reticulado caracteristico<br>
            - possui uma enorme capacidade de absorver água e altera o pH do meio, tornando-o mais ácido<br>
            devido à acidez a decomposição é reduzida (eg. homem de Tollund), dando origem à turfa<br>
    
    <img src='img/botanica/sphagnum0.png' width='500'><br>
    <img src='img/botanica/sphagnum1.png'><br>

# 5 - A evoluçao das plantas vasculares

- a evoluçao das plantas vasculares
    - evoluçao do sistema vascular
        - elas adquirem feixes condutores para permitir a melhor distribuiçao de água e nutrientes
        - sintetizam lenhina (polímero de 3 monómeros fenólicos) para permitir um maior suporte e capacidade de conduçao da água
        - a teoria estelar (Fierghem e Douliot 1886) explica a estrutura do eixo das plantas<br>
        a estela corresponde á regiao do caule ou raiz primários que compreende o periciclo e os tecidos vasculares e nao vasculares no seu interior
            - a protostela<br>
            é o tipo de estela mais simples e mais primitiva, consistindo num cilindro sólido de xilema, rodeado por floema<br>
            surge em especies mais ancestrais (algumas extintas), em caules jovens de algumas especies e em muitas raizes (nao-monocotiledóneas)<br>
            - a sifonestela<br>
            no seio do xilema existe uma medula central (o floema poderá estar localizado em ambos os lados)<br>
            surge na maior parte dos caules dos fetos<br>
            - a eustela<br>
            é o tipo de estela mais evoluído<br>
            consiste em feixes vasculares discretos envolvidos por medula<br>
            surge nos caules da maior parte das plantas com semente<br>

    - evoluçao das raizes e folhas
        - maior especializaçao do corpo do esporófito, com raizes (para ancorar ao solo e absorver água) e caules e folhas (suporte e fotossintese)
        - crescimento secundário
        - 2 teorias<br>
            a da enaçao aplica-se melhor a micrófilos (um tipo de folha de planta definido pela existência de um única nervura que não se ramifica) e a do teloma, explica melhor os macrófilos (folhas grandes)<br>
            um aspeto que distingue os dois tipos de folha é que nos macrofilos existe um intervalo de separaçao das folhas no caule (nos microfilos, nao) 
            - a teoria da enação<br>
            segundo a qual os micrófilos terão evoluído a partir de protuberancias do caule<br>
            é uma explicaçao credível para folhas com apenas um feixe vascular, associados a protostelas<br>
            - a teoria do teloma<br>
            diz que as folhas surgiram da reduçao progressiva de algumas das ramificaçoes<br>
            é improvável para os micrófilos mas é explicação credivel para os macrófilos (folhas de maior dimensao com sistemas defeixes vasculares complexos, geralmente associadas a sifonostelas e eustelas)<br>
    - evoluçao do sistema de reproduçao
        - possuem meristemas apicais no esporófito para permitir uma maior ramificaçao e produçao de multiplos esporangios (nos briofitos, o esporofito apresenta meristemas sub-apicais)
        - geração gametófita cada vez mais reduzida em dimensao, até que se tornou dependente do esporófito
        - esporos com cutina, capazes de dispersarem pelo vento
        - tendência para a heterosporia<br>
            - na homosporia,
                - a meiose dá origem a apenas um único tipo de esporo
                - os gametófitos sao independentes dos esporofitos
                - e desenvolvem-se exteriormente á parede do esporo (desenvolvimento exospórico)
            - na heterosporia,
                - a meiose dá origem a dois tipos de esporo (macróscopo e micrósporo)
                - a distinção entre os esporos é principalmente funcional
                - ocorre uma reduçao significativa dadimensao e complexidade dos gametófitos, que vao começar a depender dos esporófitos
                - os gametófitos desenvolvem-se no interior da parede do esporo (desenvolvimento endospórico), estando mais protegidos<br>
    
    <img src='img/botanica/estela.png' width='500'><br>
    <img src='img/botanica/protostela.png' width='250'><br>
    <img src='img/botanica/sifonostela.png' width='250'><br>
    <img src='img/botanica/eustela.png' width='250'><br>
    <img src='img/botanica/folhas.png' width='250'><br>
    <img src='img/botanica/homosporia.png' width='500'><br>
    <img src='img/botanica/heterosporia.png' width='500'><br>


- plantas vasculares
    - licófitos ("club mosses") - Licopodiophyta<br>
        - caracteristicas
            - primeiros membros surgiram no Devónico, eram dominantes no Carbonífero<br>
            todas as espécies lenhosas extinguiram-se antes do final do Paleozóico<br>
            atualmente só existem os membros herbácios
            - apresentam "folhas" muito incipientes (afiadas) - micrófilos/enaçoes
            - apresentam raízes verdadeiras, mas podem possuir rizoma
            - os exemplares extintos apresentavam um câmbio vascular e crescimento secundário mas como nao ocorriam divisoes anticlinais no cambio, o crescimento era limitado (comparado com as plantas com semente lenhosas atuais; <10cm)
            - a água é fundamental para que ocorra fertilizaçao
        - Lycopodium
            - geraçao esporófita
                - consiste num rizoma ramificado, de onde emergem ramos aéreos
                - o caule e as raizes apresentam protoestelas
                - os esporangios encontram-se na superficie de micrófilos férteis (esporófilos)<br>
                os esporófilos podem estar organizados em estróbilos/cones terminais<br>
                ou podem ser muito identicos a micrófilos estéreis (sem estróbilos)
                - são homospóricos<br>
                => possui esporófilos e esporangios; nao possui lígua
            - geração gametófita
                - os esporos originam gametófitos bisexuais, independentes dos esporófitos (fotossintéticos ou em endomicorrizas)
                - cada arquegónio produz apenas uma oosfera<br>

        <img src='img/botanica/licopodio0.png' width='500'><br>
        <img src='img/botanica/licopodio1.png' width='250'><br>
        <img src='img/botanica/licopodio2.png'><br>
        <img src='img/botanica/licopodio3.png' width='500'><br>

        - Selaginella
            - geração esporófita
                - pode apresentar 2 tipos de micrófilos
                - possui protostelas nos caules e raizes
                - possui uma lígua na base de cada micrófilo e esporófilo
                - esporófilos geralmente organizados em estróbilos
                - heterospórica<br>
                => possui macroesporófilos onde sao desenvolvidos os macroesporangios, e microesporófilos, onde são desenvolvidos os microesporangios; possui lígua
            - geração gametófita
                - os gametófitos masculinos (microgametófitos) desenvolvem-se no interior do micrósporo<br>
                para ocorrer libertação dos gametas, é necessário ocorrer a rutura da parede do esporo
                - os gametófitos femininos (macrogametófitos) rompem a parede do macrosporo<br>
        
        <img src='img/botanica/selaginella0.png' width='500'><br>
        <img src='img/botanica/selaginella1.png' width='500'><br>
        <img src='img/botanica/selaginella2.png' width='500'><br>
        <img src='img/botanica/selaginella3.png' width='500'><br>
        <img src='img/botanica/selaginella4.png'><br>
        <img src='img/botanica/selaginella5.png' width='500'><br>
        <img src='img/botanica/selaginella6.png' width='500'><br>
        <img src='img/botanica/selaginella7.png' width='500'><br>
        
    - fetos ("ferns") - Pteridophyta<br>
        - caracteristicas
            - muito abundantes em regiões tropicais
            - muitos membros sao epifiticos
            - distinguem-se pela existencia de 2 tipos de esporangios
                - eusporangios<br>
                as células iniciais do esporangio estao na superficie do tecido que o irá desenvolver
                - leptosporangios<br>
                desenvolve-se a partir de uma única célula superficial
            - quando o esporangio seca, ocorre a contraçao do anel, promovendo a deiscencia do esporangio e dispersao dos esporos
            - inicialmente o esporofito depende do gametofito
        
        - distinçao
            - pelo tipo de esporangios
            - distinguem-se também pela homosporia ou heterosporia (mais rara)<br>
        
        <img src='img/botanica/pteridophyta0.png' width='500'><br>
        
        - Polypodium (polipódio) - os fetos comuns
            - com leptosporangios e homosporia
            - assim como os outros membros da ordem Ficales, como o Adiantum
            - caracteristicas
                - do esporófito 
                    - rizoma que produz folhas todos os anos (frondes), e com sistema vascular organizado em sifonostela
                    - as folhas/frondes são macrófilos<br>
                    são formadas por pecíolo, a nervura central (ráquis) e lâmina (que poderá ser composta em pinas e pinulas)<br>
                    no inicio do seu desenvolvimento, estao enroladas em forma de báculo (pré-folheaçao circinada)
                    - as folhas e os rizomas podem estar revestidos por tricomas ou escamas
                    - os esporangios desenvolvem-se nas margens abaxiais das folhas em aglomerados chamados soros<br>
                    muitas especies apresentam os soros envoltos num indúsio (protuberancia da folha)<br>
                    ou seja, cada bolinha no esporofito é um soro, que é um conjunto de capsulas, que sao os esporangios (formam os esporos e rebentam libertando-os).
                - do gametófito
                    - quando o esporo germina, dá origem ao gametófito (de vida livre e bisexuado) - o protalo<br>
        
        <img src='img/botanica/polipodio0.png' width='250'><br>
        <img src='img/botanica/polipodio1.png' width='500'><br>
        <img src='img/botanica/polipodio2.png' width='200'><br>
        <img src='img/botanica/polipodio3.png' width='500'><br>
        <img src='img/botanica/polipodio4.png' width='500'><br>
        <img src='img/botanica/polipodio5.png'><br>

        - Psilotum (ordem psilotales) - membros mais primitivos
            - geração esporófita
                - corpo com enaçoes
                - rizomas com rizóides
                - sistema vascular com protoestela
                - homospóricos<br>
                esporangios estao agrupados em grupos de 3 nas extremidades de ramificaçoes laterais
                <br>
                ou seja, os esporangios estao "juntos" nas pontas de cada ramo
            - geração gametófita
                - após germinaçao, desenvolve-se o gametófito bisexual que establece simbiose com um fungo...<br>
        
        <img src='img/botanica/psilotales0.png' width='500'><br>
        <img src='img/botanica/psilotales1.png' width='500'><br>
        <img src='img/botanica/psilotales2.png'><br>
        
        - Equisetum (ordem equisetales) - as plantas fósseis
            - caracteristicas
                - no periodo do Devónio, poderiam atingir alturas de 18m
                - atualmente só se conhece um género - o Equisetum, com 15 espécies - considerado o sobrevivente mais antigo das plantas
            - geraçao esporófita
                - corpo das cavalinhas é facilmente reconhecível<br>
                quando presentes, estao dispostos no caule em verticilos transversais<br>
                cada nó do caule e dos ramos apresenta verticilos de folhas, que em regra são pequenas e escamiformes<br>
                quando existem ramos, os nós que os apresentam alternam com nós apresentando verticilos transversais de folhas
                - as raizes surgem nos nós do rizoma<br>
                o canal central e canais carinais ficam cheios de água<br>
                os canais valeculares contém sempre ar
                - homosporicos
                - cada esporangióforo (disposto num estróbilo) possui 10 esporangios
                - nalgumas espécies existem caules férteis distintos de caules nao-férteis
                - libertaçao dos esporos facilitada pelos elatérios
                - ou seja, os esporos sao produzidos nos estróbilos, uma estrutura em forma de cone. A aparencia depende da espécie
            - geração gametófita
                - independente e de vida livre
                - bisexuais ou possuem apenas anterídeos<br>
        
        <img src='img/botanica/equisetales0.png' width='500'><br>
        <img src='img/botanica/equisetales1.png' width='500'><br>
        <img src='img/botanica/equisetales2.png' width='500'><br>
        <img src='img/botanica/equisetales3.png' width='500'><br>
        <img src='img/botanica/equisetales4.png' width='500'><br>
        <img src='img/botanica/equisetales5.png' width='500'><br>
        <img src='img/botanica/equisetales6.png'><br>

        - outros
            - fetos aquaticos
                - com leptosporangios e únicos a apresentar heterosporia
            - membros com eusporangios (atualmente estao pouco representados)
    - generos extintos
        - Rhyniophyta - os representantes mais primitivos das plantas vasculares
        - Zosterophyllophyta
        - Trimerophyta
        - Progymnospermophyta
        - Pteridospermophyta
    - plantas com semente
