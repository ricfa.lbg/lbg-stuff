# Petrologia

- petrologia
    - e nao pretologia, que é racismo. Tal como em Geo, a cadeira tem imagens giras :ok_hand: e provavelmente qb para memorizar
    - cadeira "Petrologia I", foco em petrologia ignea


- profs
    - Pedro Pimenta (T, regente)
    - ?? (PL)


- programa
    - A Petrologia como ciência
    - Composição, textura e classificação das rochas ígneas
    - Estruturas ígneas e relações de campo
    - Instalação de magmas
    - Propriedadades do magma
    - Equilíbrio sólido-líquido em sistemas magmáticos
    - Diferenciação magmática<br>
        Petrogénese dos magmas. Séries magmáticas e sua caracterização.<br>
    - Associações petrotectónicas<br>
        seu enquadramento geotectónico e modelos petrogenéticos<br>


- biblio
    - "essencial"
        - [Winter - An Introduction to Igneous and Metamorphic Petrology, 2001 Pearson](https://elearning.uminho.pt/bbcswebdav/pid-1204804-dt-content-rid-5795056_1/xid-5795056_1) ("livro 1")
        - [Myron – Igneous and Metamorphic Petrology, 2009 Pearson](https://elearning.uminho.pt/bbcswebdav/pid-1204805-dt-content-rid-5795057_1/xid-5795057_1) ("livro 2")
    - complementar
        - [Frost - Essentials of Igneous and Metamorphic Petrology, 2019 Cambridge](https://elearning.uminho.pt/bbcswebdav/pid-1204806-dt-content-rid-5795058_1/xid-5795058_1) ("livro 3")
        - [Maitre - Igneous Rocks: A Classification and Glossary of Terms, 2002 Cambridge](https://elearning.uminho.pt/bbcswebdav/pid-1204807-dt-content-rid-5795059_1/xid-5795059_1) ("livro 4")
        - [Rodrigues - Interpretaçao de diagramas de fases de interesse geológico, 1983 UNL](https://elearning.uminho.pt/bbcswebdav/pid-1204808-dt-content-rid-5795060_1/xid-5795060_1) ("livro diagramas de fase")
    - textos
        - [slides](https://elearning.uminho.pt/webapps/blackboard/content/listContent.jsp?course_id=_51941_1&content_id=_1204791_1)
        - [Elsa Gomes - Sebenta de Mineralogia, 2004](https://elearning.uminho.pt/bbcswebdav/pid-1204811-dt-content-rid-5795063_1/xid-5795063_1)
        - [Stephen Nelson - magmatic differentiation lecture notes, 2012](http://www.tulane.edu/~sanelson/eens212/index.html#Lecture%20Notes)
        - [tese Ludov - Associações Petrotectónicas, 2013](https://elearning.uminho.pt/bbcswebdav/pid-1204810-dt-content-rid-5795062_1/xid-5795062_1)
    - PL
        - livros
            - [MacKenzie - Rocks and Minerals in Thin Section, 2017 CRC](https://elearning.uminho.pt/bbcswebdav/pid-1205267-dt-content-rid-5796126_1/xid-5796126_1) ("atlas de minerais")
            - [MacKenzie - Atlas of Igneous Rocks and Their Textures, 1982 Pearson](https://elearning.uminho.pt/bbcswebdav/pid-1205269-dt-content-rid-5796127_1/xid-5796127_1) ("atlas de rochas ígneas")
            - [Raith - Guia para microscopia de minerais em lâminas delgadas, 2014](https://elearning.uminho.pt/bbcswebdav/pid-1205271-dt-content-rid-5796138_1/xid-5796138_1) ("guia de microscopia")
            - [Jerram - The Field Description of Igneous Rocks, 2011 Wiley](https://elearning.uminho.pt/bbcswebdav/pid-1205274-dt-content-rid-5796142_1/xid-5796142_1)
        - textos
            - [slides - aulas online](https://elearning.uminho.pt/webapps/blackboard/content/listContent.jsp?course_id=_51941_1&content_id=_1205266_1)
            - [slides - material de apoio](https://elearning.uminho.pt/bbcswebdav/pid-1205272-dt-content-rid-5796139_1/xid-5796139_1)
            - [slides - microscopia básica](https://elearning.uminho.pt/bbcswebdav/pid-1205270-dt-content-rid-5796128_1/xid-5796128_1)
            - [Descrição propriedades ópticas dos principais minerais](https://elearning.uminho.pt/bbcswebdav/pid-1205273-dt-content-rid-5796141_1/xid-5796141_1)
            - [Tabela de identificação das principais características ópticas de minerais mais frequentes](https://elearning.uminho.pt/bbcswebdav/pid-1205275-dt-content-rid-5796143_1/xid-5796143_1) ("mineralogia-tabela")
            - [Estudo Petrográfico de Rochas Ígneas](https://elearning.uminho.pt/bbcswebdav/pid-1205276-dt-content-rid-5796144_1/xid-5796144_1)
        - links
            - [Alex Strekeisen - petrografia](http://www.alexstrekeisen.it/english/index.php)
            - [Minerais de rochas ígneas ao microscópio](http://minerva.union.edu/hollochk/c_petrology/ig_minerals.html)
            - [Microscopia 1](http://www.dct.uminho.pt/rpmic/interactividade/index.html)
            - [Microscopia 2](http://www.dct.uminho.pt/mictic/capa.html)
            - [Microscopia 3](http://www.earth.ox.ac.uk/~oesis/micro/index.html)
            - [Microscopia 7](http://www.brocku.ca/earthsciences/people/gfinn/optical/222lect.htm)
            - [Como fazer a figura de interferência](https://www.youtube.com/watch?v=-26WGB6igKs)

- metapetrologia
    - avaliaçao<br>
        teste teórico 70% + testes praticos 30%<br>
        T: 19 Abril, 31 Maio<br>
        P: nessa semana<br>
        nota minima: T 9.5, P 6<br>


# PLs

## PL 24 fev

"introdução ao lab", olhamos para umas rochas e tentamos desenhar e descrever

## PL 3 março

- descrição de rochas
    - mineralogia<br>
        que minerais ocorrem/sao visiveis e em que percentagens?<br>
        para isso uso um aparelho especifico [?????]() ou estimo usando uma lupa para definir um campo e calcular as %<br>
        vou projetar esta composição num diagram QAPF, um losango com essas letras nos cantos<br>
        Q - quartzo<br>
        A - feldspato alcalino<br>
        P - feldspato plagioclássico<br>
        F - feldspatoides<br>
    
    - modo de ocorrência<br>
        = modo de jaziga nas rochas igneas<br>
        [????]()

    - estrutura da rocha<br>
        como é os constituintes se organizam?<br>
        - homogénea ou maciça<br>
        - bandada<br>
            com bandas
        - nodular<br> 
            com nódulos [????]()
        - vesicular<br>
            com vesiculas [????]()
        - com encraves<br>
            com encraves [????]()<br>
            diz-se uma relação hospedeiro/encrave
        - com segregaçoes<br>
            aparecem veios de minerais
        - com schlieren<br>
            aka estrias; os minerais alinham-se e formam listras<br>
            eg. schlieren de biotite
    
    - fábrica ("fabric")<br>
        como é que os elementos se distribuem espacialmente?<br>
        - isotropica<br>
            nenhuma orientação perferencial
        - foliação (magmática)
        - lineação (magmática)<br>
            nao é tao facil de ver<br>
            eg. uma rocha que parece isotropica mas podemos medir a anisotropia magnética (causada pelos minerais que a constituem) e pode revelar que há na realidade uma orientação perferencial

        - foliação + lineação
    
    - textura no sentido lato (amplo)
        - cor<br>
            - hololeucrata<br>
                nada escuro [????]()
            - leucrata<br>
                clara
            - mesocrata<br>
                intermédia
            - melanocrata<br>
                escura
        
        - (grau de) cristalinidade<br>
            é a razão entre a % de minerais e a % de vidro<br>
            - holocristalina<br>
                .>90% de cristais
            - hemicristalina
            - holohialinas<br>
                .>90% vidro
            
            eg. rochas plutónicas são 100% cristais => são faneriticas, holocristalinas

        - granularidade<br>
            - % do grau visivel?
                - fanerítica/granular<br>
                    .>90% dos grãos sao visiveis em amostra de mao/a olho nu
                    
                    - os graus têm todos +/- a mesma dimensão?<br>
                    - equigranular<br>
                        sim<br>
                        neste caso, só temos de referir a dimensão do grau
                    - inequigranular<br>
                        nao<br>
                        neste caso, é possivel identificar uma matriz e megacristais<br>
                        os megacristais descrevem-se dizendo ao mineral, dimensao e forma<br>
                        eg. de x com 5mm<br>
                        a matriz descreve-se quanto á granularidade (novamente, faneritica/micro/afanitica) + dimensão do grao<br>
                        eg. matriz feldspática com dimensão de grau médio

                - microfanerítica<br>
                    visivel ao microscópio<br>
                    eg. dolerito, rochas hipabissais (rochas subvulcanicas - que se instalam entre as vulcanicas e as plutónicas)<br>
                    pode ser
                    - microcristalina<br>
                        verifica-se que é cristalino ao microscópio óptico
                        
                    - criptocristalina<br>
                        nao se ve ao micro óptico mas mas sob luz polarizada vê-se que é cristalina<br>
                        eg. silex; obsidiana - pode ser que parte esteja cristalizada

                - afanitica<br>
                    .>90% não visiveis a olho nu<br>
                    eg. basalto olivinio - observamos as olivinas mas é afanitico porque tem a matriz onde nao conseguimos identificar o grao<br>
                    pode ser
                    - microcristalinas<br>
                    - criptocristalina<br>
                    - vítrea
            
            - categorias para dimensão do grau
                - grao vasco e encosta [????]()
                - grao muito grosseiro (> 30mm)
                    - se for inequigranular, é pegmatítica
                - grao grosseiro (5-30)
                - grao médio (1-5)
                - grao fino (<1)
                    - se for faneritica, é aplítica/sacaróide

                como é dificil ter noçao destas grandezas, o prof sugeriu fazer no autocad/photoshop uma escala granulometrica para comparar com a rocha
            
        - forma dos minerais<br>
            - euédrico<br>
                todo limitado por faces próprias (planos de clivagem, de macla)
            - subédrico
            - anédrico<br>
                limitado por planos que não pertencem am mineral<br>
                eg. se for arredondado é anédrico
        
        - textura (no sentido restrito)
            - idiomórfica/automorfica<br>
                .>90% euédrico
            - hipomórfica/hipeutomórfica<br>
            - alotriomórfica<br>
                .>90% anédrico
        
        - texturas especiais/padrões texturais
            - porfiróide
            - porfiritica
            - intergranular
            - intersertal
            - cumulada
            - ofítica
            - sub-ofítica
            - vítrea
            - perlítica
            - spinifax
            - vesicular
            - syneurix

    

- siglas [????]() IUGS, IMPA


http://www.dct.uminho.pt/pnpg/trilhos/pitoes/paragem5/textura.html


# 1. Rochas Igneas - Introduçao

- minerais
    - abundância na crusta<br>
    <img src='img/petro/minerais-crusta.png' width='250'><br>

- Petrologia
    - objetivos
        - identificar e caracterizar rochas
        - identificar as fontes/protólitos
        - identificar os processos formadores de rochas
        - caracterizar ambientes
        - datar rochas e processos e descrever a sua história
    - petrologia ignea = petrologia das rochas igneas<br>
        o estudo dos processos magmáticos, desde a fusão dos materiais até à obtenção dos produtos (rochas ígneas), passando pela caracterização das fontes dos magmas, processos de diferenciação e das respectivas rochas

- divisões das rochas
    - ígneas/magmáticas
        - extrusivas
        - intrusivas

        <img src='img/petro/rochas-igneas.png' width='350'><br>
        - critérios para identificaçao
            - mineralógicos e texturais (incluindo estudo microscópico)<br>
                analise mais detalhada da textura e mineralogia<br>
                - (micro)texturas
                - (micro)estruturas

                determinaçao da composição:
                - rocha<br>
                    composição química + isotópica
                - minerais<br>
                    - análise modal (microscópio)<br>
                    - composição química (microssonda)<br>
                    - difracção de raios-X<br>

                    <img src='img/petro/analise-modal.png' width='350'><br>

            - observaçoes de campo<br>
                sao estruturas pouco deformadas em comparação com as estruturas metamórficas<br>
                identificação das estruturas e texturas<br>
                - cartografia dos corpos geológicos<br>
                    eg. magmas, depósitos piroclásticos<br>
                - identificar a forma dos corpos e relações de contacto entre rochas
                    - existência de encraves, fácies de bordadura
                    - tipo de contacto entre corpos (brusco, gradacional, intersecção)
                    - cronologia relativa entre corpos


    - metamórficas

        <img src='img/petro/rochas-metamorficas.png' width='350'><br>

    - sedimentares

        <img src='img/petro/rochas-sedimentares.png' width='350'><br>

- ciclo das rochas<br>
    em ambientes diferentes, ocorrem processos especificos que levam á origem de materiais/rochas diferentes<br>

    <img src='img/petro/ciclo-rochas.png' width='350'><br>

    - fontes de energia/calor<br>
    <img src='img/petro/fontes-energia-ciclo.png' width='350'><br>

- subsistemas da Terra<br>
    <img src='img/petro/subsistema-terra.png' width='350'><br>




- formaçao do sistema solar

    TODO

- formaçao da Terra

    TODO<br>
    - estrutura inicial: nucleo e manto primitivos
    - estrutural atual

    <img src='img/petro/terra-estrutura.png' width='350'><br>

    1 - crusta continental<br>
    2 - crusta oceanica<br>
    A - [descontinuidade Moho](https://en.wikipedia.org/wiki/Mohorovicic_discontinuity) (Mohorovicic) - entre crusta e manto<br>
    3 - manto superior<br>
    4 - manto inferior<br>
    B - [discontinuidade Gutenberg](https://en.wikipedia.org/wiki/Core%E2%80%93mantle_boundary)<br>
    5 - nucleo exterior/externo<br>
    C - [descontinuidade de Lehmann](https://en.wikipedia.org/wiki/Lehmann_discontinuity)<br>
    6 - nucleo interior<br>

    - diferenciaçao

        <img src='img/petro/diferenciaçao-terra.png' width='350'><br>

- formaçao da Lua
    
    TODO

- descontinuidades<br>
    definidas por alterações na velocidade das ondas sismicas

    - [ondas sismicas](https://en.wikipedia.org/wiki/Seismic_wave)<br>
        definidas pelo modo como a particulas vibram em relação á direçao de propagaçao da onda

        - [primárias, P](https://en.wikipedia.org/wiki/P_wave) - são ondas longitudinais<br>
            aka particulas vibram paralelamente á direçao de propagaçao da onda

            <img src='img/petro/onda-P.png' width='350'><br>

        - [secundárias, S](https://en.wikipedia.org/wiki/S_wave) - transversais<br>
            aka vibraçao perpendicular á direçao de propagaçao<br>
            nao se propagam em meios líquidos! (velocidade ~0)

            <img src='img/petro/onda-S.png' width='350'><br>

        - de superfície
            - [Rayleigh](https://en.wikipedia.org/wiki/Rayleigh_wave) - orbita elíptica

                <img src='img/petro/Rayleigh2.png' width='350'><br>

            - [Love](https://en.wikipedia.org/wiki/Love_wave) - transversais, horizontalmente polarizadas

                <img src='img/petro/Love2.png' width='350'><br>

            - [Stoneley](https://en.wikipedia.org/wiki/Stoneley_wave) - geradas durante sondagens, guiada ao longo da interface do furo
        
    <img src='img/petro/descontinuidades.png' width='350'><br>

    - [Conrad](https://en.wikipedia.org/wiki/Conrad_discontinuity)<br>
        - entre a crusta continental superior e inferior; nao presente na oceanica<br>

    - Moho<br>
        - 35-40km, separa a crusta do manto
        - velocidade das ondas P aumenta
        - aumento de ~1 km/s
        - acima da descontinuidade, a velocidade das P é consistente com as que atravessam [basalto](https://en.wikipedia.org/wiki/Basalt) (6.7-7.2 km/s), e abaixo, sao similares ás que atravessam [peridotito](https://en.wikipedia.org/wiki/Peridotite) ou [dunito](https://en.wikipedia.org/wiki/Dunite) (7.6-8.6 km/s)<br>

        <img src='img/petro/Moho.png' width='350'><br>
        <img src='img/petro/Moho2.png' width='350'><br>

    - [zona de baixa velocidade](https://en.wikipedia.org/wiki/Low-velocity_zone)
        - zona entre os 100-220km onde a velocidade das S decresce abruptamente
        - é uma zona, e não uma superficie de descontinuidade bem definida, e nao se observa em todos os locais!
        - uma pequena parte do material do manto (peridotitos, ricos em olivina e piroxena) está fundido
        - define a [astenosfera](https://en.wikipedia.org/wiki/Asthenosphere), uma camada menos rigida do manto sobre a qual se movem as placas litosféricas. Acima da astenosfera flutuam entao os pedaços de litosfera (que inclui entao a crusta e parte do manto superior), e abaixo da astenosfera está a mesosfera

    - descontinuidade de Gutenberg<br>
        - profundidade 2900km, as ondas S deixam de se propagar e a velocidade das P diminui bastante
        - inferido por Beno Gutenberg (sismólogo alemao, 1889-1960) ao estudar as zona de sombra sismica das ondas P e S<br>
            as P nao se registam entre os 103º e os 143º<br>
            as S nao se registam a partir dos 103º<br>

        - corresponde á camada D'', em que as rochas estão parcialmente fundidas. As ondas S deixam de se propagar porque ondas transversais nao se propagam em meios de rigidez nula. Marca a fronteira entre o manto e o núcleo externo, que estará no estado liquido. É de onda surgem as convecções que originam as [plumas mantélicas](https://en.wikipedia.org/wiki/Mantle_plume)
    
    - descontinuidade de Lehmann
        - profundidade 5150km, marca a fronteira entre um nucleo externo liquido e um núcleo interno sólido
        - inferido por Inge Lehmann (geofisica dinamarquesa, 1888-1992), ao verificar que algumas ondas P eram registadas na zona de sombra
        - devem-se a ondas P refletidas e refratadas nesta descontinuidade, que podem aparacer na zona de sombra

- modelos da [estrutura interna da Terra](https://en.wikipedia.org/wiki/Structure_of_Earth)
    - fisico<br>
        baseado nas propriedades fisicas dos materiais, conforme inferido pelas ondas sismicas, sobretudo rigidez
        - litosfera<br>
            0-220km, sólida e rígida<br>
            espessura ~220km (nos oceanos, cerca de 100km; nos continentes até 220km)

        - astenosfera<br>
            220-410km, sólida mas dúctil/com comportamento plástico (~1% fundida)<br>
            profundidade da "base" depende do modelo, alguns autores consideram que vai até aos 350km, outros até 650km. Aqui considera-se um valor intermédio<br>
            espessura ~190km

        - mesosfera/manto mesosférico<br>
            410-2890km, sólida<br>
            espessura ~2480km

        - endosfera<br>
            2890-6370km<br>
            espessura ~4380km

            - nucleo interno<br>
                2890-5150km, liquido

            - nucleo externo<br>
                5150-6370km, sólido

    - quimico<br>
        baseado na composiçao quimica
        
        - crusta
            - continental<br>
                0-30/40km (até 70km)<br>
                rochas magmáticas (granitos e dioritos), rochas metamórficas e sedimentares<br>

            - oceanica
                0-5/10km<br>
                rochas magmáticas (superficialmente - basalto; em profundidade - gabro)<br>
        
        - manto
            - superior<br>
                base da crusta-660km<br>
                peridotito (rocha magmática rica em olivina e piroxena)<br>

            - inferior
                660-2890km<br>
                minerais mais densos. eg. [perovskite](https://en.wikipedia.org/wiki/Perovskite)<br>
            
            <img src='img/petro/manto-quimica.png' width='500'><br>
        
        - nucleo
            - externo<br>
                2890-5150km<br>
                Fe + 12% Ni, silica, S, K<br>

            - interno<br>
                5150-6370km<br>
                Fe + 10-20% Ni<br>
    
    - movimentos da litosfera<br>
        se as placas litosfericas se movem tem de existir forças geradoras desse movimento, no interior da Terra<br>
        Pensa-se que são as correntes de convecção no manto as responsaveis por estes movimentos horizontaius das placas<br>

        - modelos para o sistema de conveção no manto<br>
            em ambos os modelos, as dorsais ocânicas estao localizadas nos ramos ascendentes e as zonas de subducçao nos ramos descendentes das correntes de convecçao<br>
            - processo    
                - a fonte de energia principal para as correntes de conveçao é o calor gerado pela radiotividade nas rochas do manto<br>
                - a distribuição do calor interno não é uniforme - em certas regioes do manto, o material é aquecido e expande-se, diminuindo a sua densidade. Este material menos denso ascende em direçao á base da litosfera<br>
                - em contacto com as rochas da litosfera, mais fria, arrefece, tornando-se mais denso e afundando novamente<br>
                este movimento circular constitui as correntes de conveção<br>

            <img src='img/petro/conveçao-modelos.png' width='300'><br>
            <img src='img/petro/conveçao-modelos2.png' width='300'><br>
            <img src='img/petro/conveçao-modelos3.png' width='300'><br>
        
        - orogénese<br>

            os movimentos das placas geram nos seus limites tensões responsaveis por enrugamentos das rochas - nestas zonas ocorrem movimentos verticais que provocam o espessamento da crusta<br>
            este espessamento traduz-se tanto num aumento de altitude da crusta como no afundamento da litosfera na astenosfera<br>
            Estes movimentos verticais e levantamentos crustais na colisao de placas originam cadeias montanhosas (orogénese).<br>
            eg.
            - os Alpes
            - os Andes
            - os Himalaias<br>
                colisão entre a placa Euro-Asiática e a placa Indiana<br>

        <img src='img/petro/ambiente-tectonico.png' width='300'><br>

    TODO - ver 2003 Earth's Dynamic Systems, christiansen PEARSON, ch18 seismicity and earth's interior, possivelmente util
    
    
- métodos de estudo do interior da geosfera
    - métodos diretos<br>
        permitem o contacto com materiais do interior da Terra<br>
        limitadas pois fornecem pouca informaçao relativamente a zonas mais profundas<br>
        - vulcanologia
        - sondagens
        - estudo de materiais de profundidade que afloram á superficie

            - xenólitos
                - eg. xenobomba - um xenólito revestido por lava, emitido como uma bomba numa erupçao<br>
                    <img src='img/petro/xenobomba.png' width='150'><br>
    - métodos indiretos<br>
        são métodos no ambito da Geofisica<br>
        permitem a construçao de modelos da estrutura interna da Terra<br>
        - geotermia<br>

            TODO - que métodos se usam para estas mediçoes?-----------------------------<br>

            a geotermia estuda o calor no interior da Terra<br>
            A principal origem é o decaimento radioativo de isótopos ainda abundantes no seu interior<br>

            - gradiente geobárico<br>

                <img src='img/petro/gradiente-geobarico.png' width='350'><br>

            - fluxo térmico<br>

                é o fluxo de calor do interior para a superficie do planeta<br>
                é contínuo mas não uniforme - varia desde os altos valores verificados nos riftes até aos mais baixos, no interior das grandes placas continentais<br>
                unidade são energia/area, eg. mW/m^2<br>
                os limites de placas litosfericas, em especial os divergentes, sao locais de elevado fluxo térmico<br>

                <img src='img/petro/fluxo-termico1.png' width='350'><br>
                <img src='img/petro/fluxo-termico.png' width='350'><br>

            - gradiente geotérmico<br>

                o gradiente geotérmico é a variação de temperatura com a profundidade (declive do grafico profundidade vs T)<br>
                para a crusta superior, o valor médio é 25ºC/Km<br>
                o valor do gradiente diminui á medida que consideramos zonas mais profundas da Terra<br>
                a banda vermelha corresponde aos intervalos de valores de incerteza<br>
                
                <img src='img/petro/gradiente-geotermico1.png' width='300'><br>
                <img src='img/petro/gradiente-geotermico.png' width='300'><br>
                <img src='img/petro/gradiente-geotermico2.png' width='300'><br>
            
            - grau geotérmico<br>

                é a distancia/intervalo de profundidade associada á variação de 1ºC<br>
                para as zonas mais superficiais e frias da Terra, ronda os 33 metros<br>
                valor tende a aumentar á medida que a profundidade aumenta<br>

        - geomagnetismo
        - sismologia
    

- vulcanologia
    - magmas<br>
        <img src='img/petro/magmas-quantidade.png' width='300'><br>


https://www.researchgate.net/publication/256542901_Earth's_surface_heat_flux


# 2. Rochas Igneas - Texturas

- textura primária (eg. olivina) vs secundária (eg. olivina transforma-se em serpentina)
- textura porfiroide vs porfiritica

- identificação
    - rochas faneríticas - analise nodal<br>
        contar ~2000 cristais em lâmica delgada em rochas faneríticas
    - rochas vulcanicas - análise quimica<br>
        - determinação da composiçao quimica
        - cálculo da norma<br>
            converter os resultados da análise quimica (as % de elementos) em % de minerais, de forma a podermos usar os mesmos diagramas que usamos em rochas faneriticas

- [feldspatoides](https://en.wikipedia.org/wiki/Feldspathoid)
    - tectossilicatos do grupo feldspatoides
    - semelhantes aos feldspatos mas com menos silica
    - eg. [nefelina](https://en.wikipedia.org/wiki/Nepheline)

# 3. Rochas Igneas - Classificaçao

- geologia da regiao Norte
    - maioritario é o granito
    - granito do Sameiro, granito de Braga

- rochas igneas
    - vulcanicas<br>
        formado "imediatamente abaixo" da superficie<br>
        geralmente, baixo desenvolvimento de cristais<br>
        Pode ser holocristalina, hemicristalina ou vitrea

    - plutónicas<br>
        formado em profundidade, 5-10km<br>
        geralmente, grande desenvolvimento de cristais<br>
        eg. granito - holocristalina

- granito
    - minerais "essenciais" (composiçao)
        - quartzo - um tectossilicato, subgrupo da silica
        - feldspatos (plagioclase + potássico), os tectossilicatos do subsgrupo dos feldspatos
    - usos: para brita, ornamental
    - formado a 5-10km, dependendo do teor em Fe

    <img src='img/petro/feldspatos.png' width='300'><br>

- dolerito
    - mesma composição que o granito e gabro mas grão mais fino

- peridotito
    - - minerais "essenciais"
        - olivina - $`(Mg,Fe,Ca)_2SiO_4`$, nesossilicato
        - piroxenas - inossilicato de cadeia simples
    - no vulcanismo, o magma formado será pobre em silica mas rico em Fe e Mg
    - as piroxenas têm menor ponto de fusao e fundem primeiro. As olivinas associadas a esta rocha vao ser da série forsterite-faialite. A forsterite (substituição por Fe) tem um ponto de fusão superior (1890 ºC) à faialite (1205 ºC, substituição por Mg). Por isso a faialite irá ser formada primeiro e o magma/liquido originalmente terá pouco Fe. Há medida que ascende, o magma vai arrefecendo e a faialite passará a forsterite e o magma ficará com mais Fe<br>
    <-------------------- nao me perguntem em que é que isto é relevante mas alas

    <img src='img/petro/olivinas.png' width='300'><br>
    <img src='img/petro/piroxenas.png' width='300'><br>


- basalto
    - minerais "essenciais"
        - feldspatos (plagioclase)
        - olivina + piroxena

- vidro
    - não tem minerais
    - rocha vulcânicas, eg. obsidiana, formada á superficie, por arrefecimento rápido
    - o magma que lhe dá origem é viscoso

    <-------------------- nao mto convincente!