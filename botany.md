# Aulas


# "Resumos" 

Mostly copy-pasted from wiki - other sources in references. In the trees, i ignore extinct groups

- Botany<br>
    generally refers to the study of **plants**, but other organisms are often included in the field such as photosynthetic bacteria, fungi, algae, and slime molds.

- Plants<br>
    - Domain: Eukaryota, Kingdom: Plantae<br>
        => all are eukaryotic
    - historically, the plant kingdom encompassed all living things that were not animals, and included algae and fungi. However, all current definitions exclude the fungi and some algae, as well as the prokaryotes (the archaea and bacteria)<br>
        => plant in the strict sense/sensu stricto means **green plants/Viridiplantae**, and includes the green algae, and land plants that emerged within them, including stoneworts<br>
        => plants in the broad sense/sensu latu are the **Archaeplastida/Plastida/Primoplantae**, and includes the green plants plus the red algae (Rhodophyta) and the glaucophyte algae (Glaucophyta) that store [Floridean starch](https://en.wikipedia.org/wiki/Floridean_starch) outside the plastids, in the cytoplasm.
    - most are multicellular and photosynthetic
    - most are made of typical plant cells - cells which contain cell walls, chloroplasts, and other cell structures that are absent in animal cells.<br>
        in most, there is absence of phycobilins (light-capturing bile pigments), the presence of chlorophyll a and chlorophyll b, cellulose in the cell wall and the use of starch, stored in the plastids, as a storage polysaccharide.<br>
        But there are some plants that are heterotrophic (parasitic or saprophytic/lysotrophic - decayed matter)<br>
        eg. [Monotropa uniflora](https://en.wikipedia.org/wiki/Monotropa_uniflora)
    - divisions:
        - Chlorophyta (green algae)
        - Streptophyta
            - Charophyta (also green algae but have plasmodesmata)<br>
                plasmodesmata (singular: plasmodesma) are microscopic channels which traverse the cell walls of plant cells and some algal cells, enabling transport and communication between them
            - Embryophyta (aka **land plants**)<br>
                have cuticle, antheridia and archegonia and a multicellular embryo
                - Bryophyta<br>
                    liverworts, mosses, hornworts
                - Tracheophyta (aka **vascular plants**)<br>
                    have vascular tissue (xylem and phloem), specific organs (stems, roots and leaves) and a dominant sporophyte
                    - Lycophyta<br>
                        eg. clubmosses, spikemosses, quillworts, scale trees
                    - Euphyllophyta<br>
                        have euphylls
                        - Polypodiophyta (aka **ferns**)
                        - Spermatophyta (aka **seed plants**)<br>
                            have seeds
                            - Gymnospermae (aka **gymnosperms** in a strict sense)<br>
                                - Cycadophyta<br>
                                    cycads - have large compound leaves and often a thick trunk, superficially resemble palm trees
                                - Ginkgophyta<br>
                                    Ginkgo
                                - Gnetophyta<br>
                                    Gnetum, Ephedra, Welwitschia
                                - Coniferophyta/Pinophyta<br>
                                    conifers - pines, cypresses, and relatives
                            - Angiospermae (**angiosperms**)<br>
                                have flowers and fruits
    - other things to note:
        - the seedless vascular plants are the lycophytes and the ferns
        - the angiosperms are the most diverse and most recent lineage, containing nearly 90% of all plant species

![lichen-relationship](img/botany/lichen-relationship.png)

- Animals<br>
    - Domain: Eukaryota, Kingdom: Animalia
    - all are eukaryotic, multicellular and heterotrophic
    - with few exceptions, animals consume organic material, breathe oxygen, are able to move, can reproduce sexually, and go through an ontogenetic stage in which their body consists of a hollow sphere of cells, the blastula, during embryonic development
    - tree:
        - Porifera (aka Sponges) - multicellular organisms that have bodies full of pores and channels allowing water to circulate through them, consisting of jelly-like mesohyl sandwiched between two thin layers of cells
        - Eumetazoa (aka Diploblasts)
            - Ctenophora -  marine invertebrates, commonly known as comb jellies, that inhabit sea waters
            - ParaHoxozoa
                - Placozoa - a basal form of marine free-living (non-parasitic) multicellular organism; they are the simplest in structure of all animals
                - Cnidaria - aquatic animals with cnidocytes, specialized cells used mainly for capturing prey. Their bodies consist of mesoglea, a non-living jelly-like substance, sandwiched between two layers of epithelium that are mostly one cell thick
                - Bilateria (animals with bilateral symmetry as an embryo)
                    - Xenacoelomorpha
                    - Protostomia - more varied digestive tract development
                        - Ecdysozoa<br>
                            include the Arthropoda (insects, chelicerata, crustaceans, and myriapods), Tardigrada (water bears), Nematoda (nematodes/round worms), and several smaller phyla.
                        - Spiralia<br>
                            include the Rotifera (rotifers/wheel animalcules), Platyhelminthes (flat worms), Molusca (mostly the cephalopod molluscs such as squid, cuttlefish, and octopuses and the gastropods, the snails and slugs), Annelida (segmented worms)
                    - Deuterostomia - animals typically characterized by their anus forming before their mouth during embryonic development
                        - Ambulacraria
                            - Echinodermata - radial symmetry; include starfish, brittle stars, sea urchins, sand dollars, and sea cucumbers
                            - Hemichordate - acorn worms, solitary worm-shaped organisms
                        - Chordata - possess 5 synapomorphies, or primary characteristics, at some point during their larval or adulthood stages that distinguish them from all other taxa.<br>
                            These 5 synapomorphies include a notochord, dorsal hollow nerve cord, endostyle or thyroid, pharyngeal slits, and a post-anal tail.
                            - Tunicata or Urochordata (sea squirts, salps)
                            - Cephalochordata (lancelets)
                            - Vertebrata (fish, amphibians, reptiles, birds, and mammals)


- Fungi<br>
    - Domain: Eukaryota, Kingdom: Fungi
    - before the introduction of molecular methods for phylogenetic analysis, taxonomists considered fungi to be members of the plant kingdom because of similarities in lifestyle: both fungi and plants are mainly immobile, and have similarities in general morphology and growth habitat.
    - like animals, they are heterotrophs<br>
        they acquire their food by absorbing dissolved molecules, typically by secreting digestive enzymes into their environment
    - the fungal cell wall is made of glucans and chitin<br>
        while glucans are also found in plants and chitin in the exoskeleton of arthropods, fungi are the only organisms that combine these two structural molecules in their cell wall. Unlike those of plants and oomycetes, fungal cell walls do not contain cellulose.
    - includes microorganisms such as yeasts and molds, as well as the more familiar mushrooms
    - other heterotrophic organisms once thought to be fungi, including the slime molds and water molds, are now considered protists

- Protists<br>
    - Domain: Eukaryota
    - an unrelated assemblage of eukaryotes that don't fit in as plants, fungi, or animals
    - protists do not form a natural group, or clade. They are a paraphyletic assemblage of similar-appearing but diverse taxa (biological groups)<br>
        Therefore, some protists may be more closely related to animals, plants, or fungi than they are to other protists; however, like the groups algae, invertebrates, and protozoans, the biological category protist is used for convenience.
    - some engulfed photosynthetic organisms and gained chloroplasts. These include the brown algae and diatoms from one engulfing event, and the red algae and green aglae from another
    - Protists were traditionally subdivided into several groups based on similarities to the "higher" kingdoms such as:
        - Protozoa<br>
            unicellular "animal-like" (heterotrophic, and sometimes parasitic) organisms are further sub-divided based on characteristics such as motility, such as the (flagellated) Flagellata, the (ciliated) Ciliophora, the (phagocytic) amoeba, and the (spore-forming) Sporozoa.
        - Protophyta<br>
            "plant-like" (autotrophic) organisms are composed mostly of unicellular algae. The dinoflagellates, diatoms and Euglena-like flagellates are photosynthetic protists.
        - Mold<br>
            "Mold" generally refer to fungi; but slime molds and water molds are "fungus-like" (saprophytic) protists, although some are pathogens. Two separate types of slime molds exist, the cellular and acellular forms.
    - These traditional subdivisions, largely based on superficial commonalities, have been replaced by classifications based on phylogenetics<br>
        in cladistic systems (classifications based on common ancestry), there are no equivalents to the taxa Protista and its contents are mostly distributed among various supergroups, including:
        - SAR supergroup (of stramenopiles or heterokonts, alveolates, and Rhizaria)
        - Archaeplastida (or Plantae sensu lato)
        - Excavata (which is mostly unicellular flagellates)
        - Opisthokonta (which commonly includes unicellular flagellates, but also animals and fungi)

- Algae
    - an informal term for a large and diverse group of photosynthetic eukaryotic organisms
    - polyphyletic grouping that includes species from multiple distinct clades<br>
        Included organisms range from unicellular microalgae, such as Chlorella, Prototheca and the diatoms, to multicellular forms, such as the giant kelp, a large brown alga which may grow up to 50 metres in length
    - most are aquatic and autotrophic
    - lack many of the distinct cell and tissue types that are found in land plants (eg. stomata, xylem and phloem)
    - the red and green algae share their ancestors with land plants.
    - groups included:
        - Archaeplastida
            - **Viridiplantae/green algae** <-------------
                - Mesostigmatophyceae
                - Chlorokybophyceae
                - **Chlorophyta** <-------------
                - **Charophyta** <-------------
            - **Rhodophyta** (red algae) <-------------
            - Glaucophyta
        - Chlorarachniophytes
        - Euglenids
        - Heterokonts
            - Bacillariophyceae (Diatoms)
            - Axodines
            - Bolidomonas
            - Eustigmatophyceae
            - **Phaeophyceae** (brown algae) <-------------
            - Chrysophyceae (golden algae)
            - Raphidophyceae
            - Synurophyceae
            - **Xanthophyceae** (yellow-green algae) <-------------
        - Cryptophyta
        - Dinoflagellata
        - Haptophyta

- related topics of study
    - plant biology - studies plants specifically
    - mycology (from the Greek μύκης mykes, mushroom) - studies fungi specifically
    - plant physiology (how they function. eg. how they respond to the environment, coordinate responses using hormones, gather energy and nutrients, and change throughout their life cycles)
    - plant ecology (plant populations and their roles in communities and ecosystems)

- uses of plants to Humans
    - food
    - clothing/fibers
    - medicine
    - environmental (cleaning air, erosion control)

- common descent
    - when one species is the ancestor of two or more species later in time
    - common descent is an effect of speciation, in which multiple species derive from a single ancestral population. The more recent the ancestral population two species have in common, the more closely are they related.
    - the most recent common ancestor of all currently living organisms is the last universal ancestor (LUCA), which lived about 3.9 billion years ago

- phylogeny
    - the evolutionary history of a group of species
    - a phylogenetic/evolutionary tree is a branching diagram or a tree showing the evolutionary relationships among various species. It represents the phylogeny
    - systematics is the study of biological diversity and its classification, which includes the process of reconstructing phylogenies
    - taxonomy is scientific study of naming, defining (circumscribing) and classifying groups of biological organisms based on shared characteristics.<br>
        Organisms are grouped into taxa (singular: taxon) and these groups are given a taxonomic rank; groups of a given rank can be aggregated to form a more inclusive group of higher rank, thus creating a taxonomic hierarchy.<br>
        Biologists prefer a system of classification that indicates evolutionary relationships among organisms (evolutionary taxonomy), using the cladistic method. The principal ranks in modern use are domain, kingdom, phylum (division is sometimes used in botany in place of phylum), class, order, family, genus, and species.<br>
        The Swedish botanist Carl Linnaeus is regarded as the founder of the current system of taxonomy, as he developed a ranked system known as Linnaean taxonomy for categorizing organisms and binominal nomenclature for naming organisms.
    - cladistics is a method of classification in which organisms are categorized in groups (or clades) based on the most recent common ancestor. A clade is a group of organisms that descended from a single ancestor, and so it is a monophyletic group ("mono" meaning one, and "phyletic" meaning evolutionary relationship)<br>
        It differs from historical methods of classification by relying on shared derived characters (a character shared by closely related organisms because they inherited it from their closest ancestor and which is not present in more distant ancestors) rather than overall similarity.<br>
        Synapomorphies (shared, derived character states) are viewed as evidence of grouping, while symplesiomorphies (shared ancestral character states) are not. The outcome of a cladistic analysis is a cladogram – a tree-shaped diagram (dendrogram) that is interpreted to represent the best hypothesis of phylogenetic relationships.<br>
        Although traditionally such cladograms were generated largely on the basis of morphological characters and originally calculated by hand, genetic sequencing data and computational phylogenetics are now commonly used in phylogenetic analyses, and the parsimony criterion has been abandoned by many phylogeneticists in favor of more "sophisticated" but less parsimonious evolutionary models of character state transformation.<br>

        ![clades](img/botany/clades.png)<br>

    - phylogenetic relationships provide information on shared ancestry, they don't necessarily address how organisms are similar or different.<br>

        ![taxonomic-ranks](img/botany/taxonomic-ranks.png)<br>

- phylogenetic trees
    - the concept originated with Charles Darwin, who sketched the first phylogenetic tree in 1837

        ![darwin-phylo-tree](img/botany/darwin-phylo-tree.png)<br>

    - it's a hypothesis of the evolutionary past since one cannot go back to confirm the proposed relationships
    - they are constructed by scoring similarities and differences between the taxa of interest, using morphologic/developmental data (relative to form and function, coming from fossils or from studying the structure of body parts or molecules used by an organism) or genetic (from DNA analysis - in particular single nucleotide polymorphisms, SNPs/snips). Taxa with the most similarities are placed into the same clade.
    - a "tree of life" can be constructed to illustrate when different organisms evolved and to show the relationships among them
    - many phylogenetic trees have a single lineage at the base representing a common ancestor. Scientists call such trees rooted, which means there is a single ancestral lineage (typically drawn from the bottom or left) to which all organisms represented in the diagram relate. Unrooted trees don’t predict a common ancestor but do show relationships among species.
    - in a rooted tree, the branching indicates evolutionary relationships.<br>
        The point where a split occurs, called a branch point or node, represents where a single lineage evolved into a distinct new one. A lineage that evolved early from the root and remains unbranched is called a basal taxon. When two lineages stem from the same branch point, they are called sister taxa. Rotation at branch points does not change the information. A branch with more than two lineages is called a polytomy and serves to illustrate where scientists have not definitively determined all of the relationships.<br>

        ![rooted-tree](img/botany/rooted-tree.png)<br>
        ![simple-trees](img/botany/simple-trees.png)<br>

    - the full name of an organism technically has multiple terms associated.<br>
        Eg. shore pine: Eukarya, Plantae, Pinophyta, Pinopsida, Pinales, Pinaceae, Pinus, Pinus contorta, and var. contorta.<br>
        Notice that each name is capitalized except for the specific epithet (contorta), and the genus and species names are italicized<br>

        ![pinus-contorta-example](img/botany/pinus-contorta-example.png)<br>

    - in plant taxonomy, phyla end in -phyta, classes end in -opsida, orders end in -ales, and families end in -aceae

    - features that overlap both morphologically (in form) and genetically are referred to as homologous structures and stem from developmental similarities. In other words, homologous traits are shared due to common ancestry<br>
        eg. bat and bird wings, the leaf in plants<br>
        When similar characteristics occur because of environmental constraints and not due to a close evolutionary relationship, it is called an analogy or homoplasy.<br>
        eg. insects use wings to fly like bats and birds, but the wing structure and embryonic origin is completely different. These are called analogous structures and are often caused by convergent evolution of structures used for similar functions.<br>

        ![homologous-wing](img/botany/homologous-wing.png)<br>
        ![homologous-leaf](img/botany/homologous-leaf.png)<br>
        ![analogous-root-stem](img/botany/analogous-root-stem.png)<br>
        ![phylogeny-vertebrates](img/botany/phylogeny-vertebrates.png)<br>

    -  the amniotic egg is a shared ancestral character (because all of the organisms in the taxon or clade have that trait) for the Amniota clade, while having hair is a shared derived character for some organisms in this group (because this trait derived at some point but does not include all of the ancestors in the tree).

    - in DNA analysis, matching can be tricky.<br>
        For some situations, two very closely related organisms can appear unrelated if a mutation occurred that caused a shift in the genetic code. In other cases, sometimes two segments of DNA code in distantly related organisms randomly share a high percentage of bases in the same locations, causing these organisms to appear closely related when they are not.<br>

    - taxonomy is a subjective discipline: many organisms have more than one connection to each other, so each taxonomist will decide the order of connections.<br> Some heuristics help with the decision:
        - Maximum parsimony<br>
            is a principle that assumes the fewest evolutionary events occurred in a given phylogeny. This is also called Ockham's razer - that we should also select the simplest hypothesis that explains the data. 
        - Maximum likelihood<br>
            another principle used when constructing phylogenies, suggests that the most likely sequence of evolutionary events most likely occured.<br>

    - phylogeny is obviously of practical interest, not just academic<br>
        ![phylogeny-use-case](img/botany/phylogeny-use-case.png)<br>
    
    - the classic tree model ("tree of life") has limitations

    stopped here,,,,,,,,,,,,,,
    https://bio.libretexts.org/Bookshelves/Botany/Botany_(Ha_Morrow_and_Algiers)/Unit_1%3A_Biodiversity_(Organismal_Groups)/02%3A_Systematics/2.04%3A_Perspectives_on_the_Phylogenetic_Tree

## Raven (mostly a repetition of what's above)

- a terrestrial environment offers abundant CO2 and solar radiation for photosynthesis. But for at least 500 million years, the lack of water and the higher UV radiation on land confined green algal ancestors of plants to an aquatic environment.<br>

- **green algae** and the **land plants or embryophytes** shared a common ancestor a little over 1 bya and are collectively referred to as the **green plants/Viridiplantae**.<br>
    - land plants/embryophytes = plantas terrestres/embriófitas, 
    - some saltwater algae evolved to thrive in a freshwater environment. Just a single species of freshwater green algae gave rise to the entire terrestrial plant lineage<br>
    - the green algae split into two major groups: the **chlorophytes**, which never made it to land, and the **charophytes**, which are a sister clade to all the land plants<br>
    Together charophytes and land plants are referred to as **streptophytes**. Land plants have multicellular haploid and diploid stages, unlike the charophytes. Diploid embryos are also land plant innovations<br>
    Although the **plant kingdom** includes the green algae, it does not include the **fungi**, which are more closely related to animals<br>

    ![green-plant-phylogeny](img/botany/green-plant-phylogeny.png)<br>
    ![land-plant-innovations](img/botany/land-plant-innovations.png)<br>


- the land plant life cycle is **haplodiplontic**, having multicellular haploid and diploid stages.<br>
    Humans and other animals have a **diplontic life cycle**, meaning that only the diploid stage is multicellular. In animals, meiosis produces single-celled haploid gametes (egg and sperm cells).These gametes fuse at fertilization to produce a diploid embryo, which divides by mitosis to become the next generation. The products of meiosis never divide by mitosis, so there is no multicellular haploid generation in animals.<br>
    Land plants undergo mitosis after both fertilization (the diploid stage) and meiosis (the haploid stage). The result is a multicellular diploid individual (called the sporophyte) and a multicellular haploid individual (called the gametophyte).<br>

    ![haplodiplont](img/botany/haplodiplont.png)

    Many brown, red, and green algae are also haplodiplontic. Humans produce gametes via meiosis, but land plants actually produce gametes by mitosis in a multicellular, haploid individual. Meiosis produces a haploid spore that divides by mitosis to produce a haploid gametophyte. The multicellular diploid generation, or **sporophyte**, alternates with the multicellular haploid generation, or **gametophyte**. Sporophyte means "spore plant", and gametophyte means "gamete plant". These terms indicate the types of reproductive cells the respective generations produce.<br>
    The diploid sporophyte produces haploid spores (not gametes) by meiosis. Meiosis takes place in structures called **sporangia**, where diploid **spore mother cells (sporocytes)** undergo meiosis, each producing four haploid **spores**. Spores are the first cells of the gametophyte generation. Spores divide by mitosis, producing a multicellular, haploid gametophyte.<br>
    The haploid gametophyte produces gametes by mitosis. Consequently, the gametes (egg cells and sperm cells) are also haploid. When two gametes fuse, the zygote they form is diploid and is the first cell of the next sporophyte generation. The zygote grows into a diploid sporophyte by mitosis and produces sporangia, in which meiosis ultimately occurs.<br>


- evolutionary innovations for reproduction, structural support, and prevention of water loss are key in the story of plant adaptation to land.<br>
    - reproduction and UV protection:<br>
        The evolutionary shift on land to life cycles dominated by a diploid generation protects against potentially damaging mutations arising from higher UV exposure that could be lethal to an organism with a predominantly haploid generation.<br>
        Terrestrial plants are exposed to higher intensities of UV irradiation than aquatic algae, increasing the chance of mutation. These land plants minimize the effects of mutation by carrying two copies of every gene. That is, their bodies are diploid, meaning that deleterious recessive mutations are masked. All land plants have both haploid and diploid generations, but there is an evolutionary shift toward a dominant diploid generation.<br>
        The haploid generation consumes a much larger portion of the life cycle in **mosses and ferns** than it does in the **seed plants - the gymnosperms and angiosperms**.<br>
        In mosses and ferns, the gametophyte is photosynthetic and free-living. When you see a moss, you are actually looking at the gametophyte (haploid) generation. The sporophyte is a small structure attached to the gametophyte.<br>
        Although ferns produce photosynthetic gametophytes, they are barely visible to the naked eye. When you see a fern plant, you are looking at the sporophyte generation.<br>
        The gametophyte is even smaller in the seed plants. It is typically microscopic. So, as with the ferns, the sporophyte is the dominant generation in seed plants.<br>

    - prevention of water loss:<br>
        As an adaptation to living on land, most plants are protected from drying out by a waxy surface material called the **cuticle** that is secreted onto parts that are exposed to the air. The cuticle is relatively impermeable, preventing water loss.<br>
        This solution, however, limits the gas exchange essential for respiration and photosynthesis. Gas diffusion into and out of a plant occurs through tiny mouth-shaped openings called **stomata** (singular, stoma) on plant leaves and stems, which allow water to diffuse out at the same time. Stomata can be closed at times to limit water loss.<br>

    - transport of water:<br>
        Moving water within plants is a challenge that increases with plant size. **Bryophytes** cannot grow tall because they lack a system to efficiently transport water.<br>**Tracheophytes** are more advanced, producing specialized vascular tissue for transport over long distances. The tissue that carries water is called **xylem**, whereas food is carried by **phloem**<br>



- Bryophytes
    - consist of three distinct clades: **liverworts** (phylum Hepaticophyta), **mosses** (phylum Bryophyta), and **hornworts** (phylum Anthocerotophyta).<br>
    - liverworts = hepáticas, mosses = musgos, hornworts = antóceros
    
    ![bryophytes](img/botany/bryophytes.png)<br>

    - are the closest living descendants of the first land plants
    - also called nontracheophytes because they lack the transport cells called tracheids
    - Fungi and early land plants cohabited, and the fungi formed close associations with the plants, enhancing water uptake. The beneficial symbiotic relationship between fungi and plants, called **mycorrhizal associations**, are also found in many existing bryophytes.<br>


**references:**<br>
[Biology, raven](https://drive.google.com/file/d/1CFOXjbK4TBLS9qU0Z9-MSrnWOYvT5f0Z/view?usp=sharing) - CH29 Seedless Plants and CH30 Seed Plants<br>
[libretexts - botany bookshelf](https://bio.libretexts.org/Bookshelves/Botany), especially [this one](https://bio.libretexts.org/Bookshelves/Botany/Botany_(Ha_Morrow_and_Algiers))<br>