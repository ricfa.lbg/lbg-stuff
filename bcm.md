Muito disto **não é necessário**! Principalmente esta parte inicial, é uma revisão do secundário

# conteúdo de secundário
- [teoria celular](https://en.wikipedia.org/wiki/Cell_theory)
    - [Robert Hooke](https://en.wikipedia.org/wiki/Robert_Hooke) escreveu [um livro](https://en.wikipedia.org/wiki/Micrographia) em 1665 em que descreve observações com um microscópio composto rudimentar. Numa das observações ele descreve uma rolha de garrafa e descreve pequenos poros a que chamou [células](https://books.google.com/books?id=0DYXk_9XX38C&q=Micrographia+honeycomb&pg=PA113), que vem do latim Cella (um pequeno quarto, como os monges viviam) ou Cellulae (a célula de 6 lados de um favo de mel). Ele não pensou que as células estivessem vivas, pois a resolução não permitia indentificar nenhuma subsestrutura. Também observou [bolor](https://en.wikipedia.org/wiki/Mold) (um fungo) em [couro](https://en.wikipedia.org/wiki/Leather). Como não conseguiu observar "sementes" que indicassem que o bolor cresceria a partir delas e aumentasse de quantidade, sugeriu em vez disso a [teoria da geração espontânea](https://en.wikipedia.org/wiki/Spontaneous_generation), que diz que seres vivos apareceriam a partir de materia nao viva, sem necessidade de ovos, sementes ou progenitores. **Eg.** [pulgas](https://en.wikipedia.org/wiki/Flea) poderiam surgir do pó e [larvas](https://en.wikipedia.org/wiki/Maggot) de carne morta.
    - [Anton van Leeuwenhoek](https://en.wikipedia.org/wiki/Anton_van_Leeuwenhoek) pouco depois usou um microscópio com melhores lentes e consegui ver objectos a mover-se e considerou essa caracteristica como indicadora de vida. Chamou-lhes [animálculos](https://en.wikipedia.org/wiki/Animalcule), e entre eles estão [protozoários](https://en.wikipedia.org/wiki/Protozoa) (termo umbrella para eucariotas unicelulares) e [bactérias](https://en.wikipedia.org/wiki/Bacteria). Também observou [hemácias](https://en.wikipedia.org/wiki/Red_blood_cell) e [espermatozóides](https://en.wikipedia.org/wiki/Spermatozoon) em animais e humanos e raciocinou que o processo de fertilização requeriria que um espermatozóide entrasse numa [oócito](https://en.wikipedia.org/wiki/Spermatozoon), formando uma visão convincente contra a geração espontânea.
    - segundo a teoria celular:
        1. todos os organismos (coisas vivas) são compostos por uma ou mais células
        2. a célula é a unidade básica estrutural/organizacional da vida
        3. células surgem de células pré-existentes. A esse processo chama-se genericamente de divisão celular.

- definição de vida

- níveis de organização biológica
    - célula

        as moléculas (biomoléculas) organizam-se de forma a formar estruturas macromoleculares, que por sua vez formam a célula.<br>
        As células são separadas do meio externo pela [membrana celular](https://en.wikipedia.org/wiki/Cell_membrane), que é uma [bicamada de lípidos](https://en.wikipedia.org/wiki/Lipid_bilayer). Através da membrana, a célula regula as trocas entre o meio interno e externo e establece interações com outras células.<br>
        Ao meio interno á celula podemos chamar de meio intracelular ou [protoplasma](https://en.wikipedia.org/wiki/Protoplasm), embora não seja um termo muito usado.<br>
        O meio interior não é homogéneo - podem distinguir-se várias partes que realizam funções especificas, chamadas [organelos](https://en.wikipedia.org/wiki/Organelle).<br>
        Os organelos podem ser rodeados por uma membrana semelhante á membrana celular, por exemplo, o [retículo endoplasmático](https://en.wikipedia.org/wiki/Endoplasmic_reticulum), o [complexo de Golgi](https://en.wikipedia.org/wiki/Golgi_apparatus) e [vacúolos](https://en.wikipedia.org/wiki/Vacuole). Alguns são rodeados por duas membranas, como no caso do núcleo, das [mitocondrias](https://en.wikipedia.org/wiki/Mitochondrion) e [cloroplastos](https://en.wikipedia.org/wiki/Chloroplast), e outros não são rodeados por nenhuma membrana.<br>
        As células podem ou não ter [núcleo](https://en.wikipedia.org/wiki/Cell_nucleus). As que têm núcleo (um ou mais, pois há células que têm mais que um), chamam-se [eucarióticas](https://en.wikipedia.org/wiki/Eukaryote), enquanto que as sem núcleo são as [procarióticas](https://en.wikipedia.org/wiki/Prokaryote).<br>
        Os seres constituidos por células eucarióticas são eucariontes (mesmo em células sem núcelo, como as hemácias) e as células procarióticas constituem os procariontes.<br>
        O [citoplasma](https://en.wikipedia.org/wiki/Cytoplasm) é o material existente no interior da célula, excepto o núcleo. O material contido no núcleo, rodeado pela [membrana nuclear](https://en.wikipedia.org/wiki/Nuclear_envelope) é o [nucleoplasma](https://en.wikipedia.org/wiki/Nucleoplasm). O citoplasma coincide com o protoplasma nas células procarióticas. O citoplasma é constituido pelos organelos e pelo [citosol/matrix citoplasmática](https://en.wikipedia.org/wiki/Cytoplasm), que é sobretudo água (70%), com pH 7-7.4, mas que podemos pensar como sendo uma mistura gelatinosa pois apesar da viscosidade ser semelhante á água pura, a difusão de pequenas moléculas neste líquido é 4x mais lenta, sobretudo devido ás colisões com o elevado numero de macromoléculas. No citosol estão dissolvidos iões em concentrações que diferem do meio exterior e que são mantidas pela célula.

        ![cytosol-vs-plasma-concentrations](img/bcm/cytosol-vs-plasma-concentrations.png)



    - multicelularidade

        alguns seres vivos são [unicelulares](https://en.wikipedia.org/wiki/Unicellular_organism) (apenas uma célula), outros são [multi/pluricelulares](https://en.wikipedia.org/wiki/Multicellular_organism) (constituídos multiplas células).<br>

    - biosfera

        subsistema da Terra constituido por todas as formas de vida e pelos ambientes que habitam

    **para pensar**
    - a definição de uni e multicelular levanta alguns problemas. Se se refere apenas ao número de células, como delimitamos o sistema (definimos onde começa e acaba o organismo)? Por outro lado, se incluirmos na definição o requesito de similaridade genética (todas as células com o mesmo DNA), também há alguma ambiguidade.<br>
    **eg.** numa colónia microbiana, em que a maior parte das células são clones de uma célula inicial (e diferem geneticamente muito pouco umas das outras, apenas devido a mutações aleatórias), trata-se de um ser multicelular ou um grupo de seres unicelulares? E nos casos em que os elementos têm de viver em colónia para sobreviver ("coloniais obrigatórios")?<br>
    **eg. 2** nos organismos multicelulares, nem todas as células têm o mesmo DNA (resultante da fusão do núcleo das células sexuais na fertilização). As hemácias não têm DNA nuclear, os linfócitos têm um DNA alterado, os gâmetas têm metade e também alterado devido ao [cruzamento cromossómico](https://en.wikipedia.org/wiki/Chromosomal_crossover) na meiose, e existem sempre células com DNA diferente (mutantes, cancerigenas) e o [microbioma](https://en.wikipedia.org/wiki/Human_microbiome)


# Programa
## 1. Os ácidos nucleicos

- [dogma central da biologia molecular](https://en.wikipedia.org/wiki/Central_dogma_of_molecular_biology)
    - termo cunhado por [Francis Crick](https://en.wikipedia.org/wiki/Francis_Crick) em 1957
    - refere-se ao fluxo de informação em sistemas biológicos. 
    
        Afirma que uma vez que a informação tenha passado a proteina, não pode ser transferida para outras proteinas ou de volta para ácidos nucleicos

        ![central-dogma](img/bcm/central-dogma.png)
    
    - esta hipótese continua válida atualmente

        note-se que com esta hipótese o Crick não se refere á transferencia de informação entre DNA e RNA (transcrição no sentido DNA → RNA, transcrição reversa/inversa mo sentido RNA → DNA) ou entre si (DNA → DNA na replicação do DNA, ou RNA → RNA na reprlicação do RNA), esses mecanismos podem ou não ocorrer mas não afetam a hipótese.<br>
        Importante é no entanto que a informação contida no RNA pode ser usada para criar proteinas (tradução) mas as proteinas não são capazes de produzir RNAs ou DNAs.

        ![central-dogma-2](img/bcm/central-dogma-2.png)

        Uma coisa que causa confusão é que o dogma é por vezes descrito de forma imprecisa, como o foi por [James Watson](https://en.wikipedia.org/wiki/James_Watson), que o descreveu como sendo o processo de dois passos DNA → RNA e RNA → proteina, nas edições iniciais do seu popular livro de texto. Isto popularizou esta descrição simplificada (o Watson, o Crick e o [Maurice Wilkins](https://en.wikipedia.org/wiki/Maurice_Wilkins) receberam um prémio Nobel em 1962 pela descoberta da estrutura do DNA).

        Ainda sobre os fluxos de informação, nem todo o RNA codifica para um proteina. Apenas o mRNA será traduzido em aminoácidos mas o RNA também pode ser funcional.<br>
        Alguns virus têm um genoma constituido por RNA em vez de DNA, e realizam duplicação do genoma sem nenhum intermediário DNA, por replicação do RNA.<br>
        A transcriptaçao reversa é usada por retrovirus para replicação do seu genoma (usam uma enzima transcriptase reversa para converter o seu RNA em DNA que é posteriormente integrado no genoma da célula invadida) e por eucariotas na replicação dos terminais dos cromossomas (telómeros).

        ![central-dogma-3](img/bcm/central-dogma-3.png)



## 2. Níveis de organização do DNA

- Os niveis de organização do DNA referem-se á sua estrutura primária, secundária e terciária

- As propriedades do DNA dependem da sua estrutura quimica

- O DNA é compactado em estruturas chamadas nucleossomas

    TODO: falar das histonas e dos cromossomas (particularmente relevante para divisao celular)

## 3. Replicação do DNA

todo

## 4. Transcrição

todo

## 5. Regulação da transcrição (operões bacterianos)

todo

## 6. Tradução

todo

## Lab - Extracção de DNA

todo

## Lab - Electroforese

todo

## Lab - PCR

todo


O=opcional, nao deve ser necessario decorar

# T7

- nome de organismos modelo (O)

- distinguir virus vs celulas

- seres subvirais (saber o nome)

    - alguns exemplos de virus (O)

    - [viroides](https://bio.libretexts.org/Bookshelves/Microbiology/Book%3A_Microbiology_(Boundless)/9%3A_Viruses/9.6%3A_Subviral_Entities/9.6B%3A_Viroids)

    - [replicaçao em virus](https://courses.lumenlearning.com/microbiology/chapter/the-viral-life-cycle/)

    - [prioes](https://bio.libretexts.org/Bookshelves/Microbiology/Book%3A_Microbiology_(Boundless)/9%3A_Viruses/9.6%3A_Subviral_Entities/9.6C%3A_Prions) e ciclo replicativo nos slides (O)

- distinguir eucariontes vs procariontes
    - e eubacterias vs arqueobacterias

# T8