# Microbiologia!

```diff 
-se algo estiver mal (embora seja copiar-colar)... @me
```

# T 16 fev

- profs
    - Cristina Aguiar, cristina.aguiar@bio.uminho.pt
    - António Rego, antonio.rego@bio.uminho.pt
    - Margarida Casal (Bioquimica), mcasal@bio.uminho.pt

- programa T
    - História e âmbito da Microbiologia<br>
        principais marcos históricos e cientistas
    - Organização em procariotas<br>
        parede celular, endósporos
    - Nutrição e crescimento microbiano<br>
        principais grupos nutricionais/metabólicos, tipos de meios de cultura indicados
    - Crescimento em sistema fechado<br>
        cinética do crescimento exponencial, taxa específica de crescimento, métodos de avaliação quantitativa de populações
    - Efeito de fatores ambientais no crescimento<br>
        agentes antimicrobianos
    - Fungos<br>
        ciclos de vida
    - Vírus, viróides e priões<br>
        morfologia e classificaçao de virus, ciclo viral
    - Micróbios<br>
        taxonomia e filogenia, técnicas bioquímicas e moleculares (principais testes usados para os classificar/identificar)
    - taxonomia dos procariotas<br>
        principais filos


- programa PL
    - Técnicas<br>
        esterilização, isolamento e cultura de microrganismos
    - Identificação de estruturas celulares bacterianas por técnicas de coloração
    - Quantificação do crescimento microbiano<br>
        determinação da taxa específica de crescimento e tempo de duplicação
    - Análise do efeito de agentes antimicrobianos e antibióticos no crescimento de populações


- tretas habituais
    - conteúdo no BB - deu-nos uns códigos MICRO21-22 e BQm21/22 ...
    - questionários nas aulas através do [menti](https://www.menti.com)
    - avaliaçao (só aplicável a BG)
        - avaliação contínua (assiduidade, pontualidade, participação, desempenho, tarefas/mini-fichas…) => 0% !!!
        - mini-testes => 70%<br>
            os "mini-testes" sao testes normais: 9 Abril (sáb), 21 Maio (sáb)
        
        - PL (apresentações 20% + teste 10%) => 30%
    - para a PL: nao temos de escrever relatórios mas necessário o caderno de lab
    - aprovaçao<br>
        frequência de pelo menos 2/3 das PL, mínima de 8 valores em cada um dos testes, classificação mínima de 10

- material
    - biblio
        - Ferreira, W.F.C., Sousa, J.C., Lima, N. (2010). Microbiologia. Lidel, Edições Técnicas Lda. Lisboa
        - Madigan, M.T., Martinko, J.M., Parker, J. (2013). Brock Biology of Microorganisms (12th ed.). Prentice-Hall International, Inc. New Jersey
        - Nobre, A., Ribeiro, A., Pais, C., Schuller, D., Rodrigues, G., Cardoso, H., Casal, M.(2004). Microbiologia e Genética Molecular Microbiana – Manual de Laboratório. Departamento de Biologia, Universidade do Minho
        - Prescott, L.M., Harley, J.P., Klein, D.A. (2008). Microbiology (7th ed.). WCB/McGrawHill Companies, Inc., New York
    - [slides T](https://elearning.uminho.pt/webapps/blackboard/content/listContent.jsp?course_id=_52281_1&content_id=_1212612_1)
    - [slides TP](https://elearning.uminho.pt/webapps/blackboard/content/listContent.jsp?course_id=_52281_1&content_id=_1213376_1)

# T 23 fev

- definiçao de microorganismo
    - geralmente considera-se que sao **seres vivos**, mas a microbiologia também estuda nao-vivos, como vírus
    - tamanho na ordem do micrometro, &mu;m

        unidades de mediçao de distancia<br>
        - dm = 0.1 m = $`10^{-1}`$ m (decimetro)
        - cm = $`10^{-2}`$ m (centi-)
        - mm = $`10^{-3}`$ m (mili-)
        - &mu;m = $`10^{-6}`$ m (micro-)
        - nm $`10^{-9}`$ m (nano-)
        - Å = $`10^{-10}`$ m (angstrom - sempre uma unidade de distancia)
        
        possivel o seu estudo devido a técnicas de microscopia<br>
        - limite de resoluçao do olho humano = 100 &mu;m
        - limite de resoluçao do microscópio óptico = até 0.1 &mu;m = 100 nm
        - limite de resoluçao do microscópio electrónico = até 0.1 nm = 1 Å = $`10^{-10}`$ m

- crescimento microbiano
    - tempo de duplicaçao<br>
        eg.<br>
        [Saccharomyces cerevisiae](https://en.wikipedia.org/wiki/Saccharomyces_cerevisiae) - "levedura de padeiro", uma espécie de levedura (fungo unicelular) usada na produçao de vinho, pao e cerveja, tem tempo duplicaçao de 2h<br>
        célula humana - 24h

        ```diff
        -falta dizer pq 
        ```
    
    - meios de cultura<br>
        - as condiçoes de cultura para crescimento máximo deve ser tao próximo do ideal quanto possivel; condiçoes encontradas na natureza sao frequentemente dificeis de mimetizar no laboratório<br>
        
        - [agar](https://en.wikipedia.org/wiki/Agar)<br>
        O ágar-ágar, também conhecido simplesmente como ágar ou agarose, é mistura heterogênea de dois polissacarídeos, agarose e agaropectina, que parece gelatina<br> É extraído da parede celular de várias espécies de algas marinhas pertencentes filo das Rhodophyta (vermelhas), sendo libertado quando fervem (algas agarófitas)<br>
        É muito empregue em microbiologia para culturas sólidas de bactérias. É especialmente útil por manter-se sólido em temperaturas comumente empregues para cultura de bactérias (37º C) e ser indigestivel para muitos microorganismos (de modo que o crescimento microbiano nao afeta o gel).<br>
    
- importancia e utilizaçao de microorganismos
    
    - microbioma humano<br>
        ```diff
        -falta dizer alguma coisa em concreto sobre isto. Quais orgaos + relevante? Cerebro?Proporçao de células para humanas? Exemplos... 
        ```
    - produçao alimentar<br>
        pão, vinho, iogurte
    
    - produçao industrial de substancias<br>
        com recurso às tecnologias do DNA recombinante, recorre-se aos micróbios para a produção de muitas substâncias que marcadamente trazem benefícios para a sociedade, nomeadamente na produção de antibióticos, vacinas, plásticos biodegradáveis

    - gestao ambiental<br>
        a PETase é uma enzima que degrada plásticos que foi primeiramente identificada em microorganismos
    
    - açoes prejudiciais
        - para a saúde humana (patogénicos)
        - para a produção de alimentos


- termos eng
    - tempo de duplicaçao = generation/duplication/doubling time
    - levedura = yeast
    - fervura = boiling


# PL 2 mar

- segurança no laboratório
    - manuseamento de reagentes
        - ler e interpretar corretamente o rótulo dos reagentes e as suas instruções de segurança antes de utilizar os químicos.
        - sempre que a manipulação assim o exija, deve-se trabalhar protegido quer usando luvas, máscaras e óculos (EPIs), quer utilizando uma “hotte”.
        - nunca misturar reagentes ou soluções que não previstos pelo protocolo.
        - não contaminar as soluções stocks de reagentes; fechar bem e não trocar tampas. Não deixar frascos abertos.
        - não tocar, cheirar ou provar os reagentes ou soluções.
        - nunca pipetar um líquido com a boca, usar sempre uma micropipeta ou pompete.
        - nunca verter água em cima de ácido.
        - evitar derramar produtos químicos – se ocorrer, pedir orientação ao professor.
        - reagentes como o HCl concentrado (37%), emitem vapores tóxicos e corrosivos, pelo que só devem ser manipulados dentro da "hotte".
        - nunca pousar a rolha dum frasco que se abriu diretamente sobre a banca de trabalho.
        - num frasco de reagentes sólido, só se deve introduzir um instrumento limpo e seco, tal como uma espátula ou colher.
        - num reagente líquido só se deve introduzir uma pipeta nas mesmas condições (limpa e seca).
        - depois duma amostra dum reagente ter sido removida dum frasco, nenhuma porção dele deve ser colocada de novo no frasco; esse resto de reagente pode ter-se contaminado durante a manipulação e iria introduzir impurezas que ficariam no frasco
    
    - utilizaçao de equipamentos<br>
        - placas de aquecimento, bicos de Bunsen ou outros equipamentos nunca devem funcionar sem vigilância
        - potenciometro<br>
            deve lavar-se sempre a sonda do potenciómetro entre as medições e colocar a proteção no final das leituras

            ![potenciometro-utilizaçao](img/microbio/potenciometro-utilizaçao.png)
        
        - micropipetas<br>
            o líquido aspirado pelas micropipetas não deve entrar no corpo principal da micropipeta, sob o risco de a adulterar e descalibrar. Respeitar o volume das micropipetas

            ![micropipeta-utilizaçao](img/microbio/micropipeta-utilizaçao.png)
        
        - microscópio<br>
            no final da sua utilização dos microscópios, as objetivas e a platina devem ser limpas com álcool. Os parafusos macro e micrométricos nunca devem ser forçados
        
        - placas de aquecimento<br>
            à temperatura ambiente ou a altas temperaturas têm sempre o mesmo aspeto. Mesmo desligadas podem provocar queimaduras.

            ![placa-aquecimento](img/microbio/placa-aquecimento.png)
        
        - centrífuga<br>
            repartir a carga de forma simétrica

            ![centrifuga](img/microbio/centrifuga.png)

    - nível de segurança biológico (biohazard)
        - biosafety levels, BSL
        - 4 níveis

            ![bsl-levels](img/microbio/bsl-levels.png)

        - nível de (bio)segurança 1, BSL-1
            - este nível de segurança biológica não requer mais que a pele do próprio operador como barreira de contenção, porque os microrganismos utilizados não representam risco especial para o homem nem para o ambiente
            - eg. levedura de padeiro, E. coli
            - nível aplicado aos trabalhos que se desenvolverão no laboratório durante a execução das aulas práticas
            - cuidados a ter
                - bata<br>
                    O uso de uma bata limpa e devidamente ajustada ao corpo é indispensável sempre que se opere em laboratório. A bata protege o operador e o seu vestuário de eventuais acidentes e deverá ser usada apenas no laboratório, sendo vestida à entrada e despida à saída. Desta forma, além de proteger o experimentador, a bata minimiza o transporte de microrganismos exteriores para o laboratório.<br>
                    TLDR: bata fica no lab (sqn)

                - vestuário e cabelos<br>
                    Os cabelos compridos deverão sempre ser atados e não é permitido o uso de peças de vestuário soltas, como batas desabotoadas ou lenços de pescoço e cachecóis. Além da movimentação excessiva de ar que provoca, a sua eventual inflamação nos bicos de gás poderá ter consequências desastrosas.

                - maos e uso de luvas<br>
                    À entrada e à saída do laboratório é preciso lavar bem as mãos com abundante água e sabão.<br> Dependente do tipo de microrganismo a manipular, o uso de luvas impermeáveis, máscaras ou outras proteções, poderá ser necessário.

                - alimentos<br>
                    É expressamente proibido comer, beber ou fumar no laboratório

                - bancade de trabalho<br>
                    O local de trabalho deverá estar sempre devidamente limpo, para o que se impõe proceder à desinfeção da bancada antes de iniciados os trabalhos, para não permitir a contaminação das próprias experiências com microrganismos de sessões anteriores e, depois das manipulações terminadas.<br>
                    Há uma técnica especifica para limpar a bancada - spray com alcool e depois movimentos retilineos ("em cobrinha") de forma a não passar repetidamente pelo mesmo local e levar os microbios para os cantos. Não é movimentos circulares aleatórios!

                - livros e cadernos<br>
                    Só levar para a bancada o material estritamente indispensável, como o protocolo experimental, um bloco de notas ou um caderno e um lápis.<br>
                    É imprescindível usar um bloco ou caderno (o "caderno de laboratório") para registo de todas as ocorrências e dos resultados da experimentação. Deve incluir os objetivo(s) da aula/trabalho, registo dos resultados e sua interpretação, e respostas as questões/discussao dos resultados

                - identificaçao do material<br>
                    - nome ou número do grupo, turma e data do ensaio
                    - conteúdo e concentrações utilizadas
                    - numa placa de petri, podiamos rotular o material na tampa mas normalmente rotula-se na base porque caso deixemos cair é possivel identificar e anotar os resultados da experiencia (embora tenha ficado contaminado nao se verá o efeito no próprio dia em principio...)
                
                - limpeza e residuos<br>
                    - pipetas de vidro
                        deverão ser colocadas nos contentores de plástico, contendo uma solução com detergente, devidamente identificados para esse fim

                    - pontas das pipetas automáticas
                        devem ser colocadas em tinas existentes, para o efeito, na bancada

                    - placas e tubos
                        deverão ser desinfetados ou colocados em local próprio para posterior autoclavagem

                    - acidentes
                        Qualquer acidente deverá ser de imediato comunicado ao responsável pela sessão de trabalho.<br>
                        Contactos de emergencia - número do telefone de emergência (tel. 112, ligando primeiro o Zero) ou a extensão do guarda de segurança (tel. 605100, só a partir das 17 horas)
                        Identificar:
                        - os chuveiros (para uma primeira e rápida descontaminação)
                        - os extintores<br>
                    

            ![bsl-1](img/microbio/bsl-1.png)<br>

        - BSL-2<br>
            eg. [Candida albicans](https://en.wikipedia.org/wiki/Candida_albicans) utilizada em investigaçao para estudar a doença. Usa-se a camara laminar, que inclui uma fonte UV que esteriliza a área antes de iniciar o trabalho (nao se trabalha á chama)

            ![bsl-2](img/microbio/bsl-2.png)
        
        - BSL-3

            ![bsl-3](img/microbio/bsl-3.png)
        
        - BSL-4<br>
            eg. [covid-19](https://en.wikipedia.org/wiki/Severe_acute_respiratory_syndrome_coronavirus_2)

            ![bsl-4](img/microbio/bsl-4.png)


- isolamento de microorganismos
    - devido às suas reduzidas dimensões, os microrganismos não são geralmente "detetáveis" a olho nu, pelo menos individualmente. Apesar de uma população suficientemente numerosa poder ser visível, não o podem ser os indivíduos que a compõem. A deteção de microrganismos em vários habitats pode ser verificada através do seu crescimento em condições favoráveis, formando populações numerosas facilmente observáveis.<br>
    - para o seu estudo, as populações mistas que se encontram no ambiente podem ser separadas em espécies individuais.
    - uma cultura que contenha uma única espécie de células é designada por cultura pura (em oposiçao a uma cultura mista)
    - a possibilidade de cultivar microrganismos em laboratório é essencial para o seu isolamento e caracterização, quer do ponto de vista morfológico, quer para estudar as suas características fisiológicas, bioquímicas e genéticas.

- cultura de microorganismos
    - necessitamos de material e meios de cultura estéreis, ou seja, desprovidos de microrganismos viáveis
    - desinfeçao vs esterilizaçao<br>
        na desinfeçao usa-se um [desinfetante](https://en.wikipedia.org/wiki/Disinfectant) para inativar ou destruir microorganismos de uma superficie inerte. Espera-se que seja o suficiente para remover a maior parte dos [patogénicos](https://en.wikipedia.org/wiki/Pathogen) - os que causam doenças<br>
        o estado de [assépcia](https://en.wikipedia.org/wiki/Asepsis)/ambiente asséptico é estar livre de microorganismos patogénicos. Um [antiséptico](https://en.wikipedia.org/wiki/Antiseptic) é um antimicrobiano que é aplicado a tecido vivo (em particular, á pele humana). Pode ser um antibacteriano, um antifúngico ou um antiviral<br>
        a [esterilizaçao](https://en.wikipedia.org/wiki/Sterilization_(microbiology)) tem como objectivo destruir todas os microorganismos e agentes biológicos (virus, prioes), resultando em esterilidade. Pode ser fisica ou quimica, com um biocida. Normalmente esterilizam-se as linhas em operaçoes cirurgicas.<br>
        os [antibióticos](https://en.wikipedia.org/wiki/Antibiotic) matam microorganismos dentro do corpo<br>

        eg. betadine ([iodopovidona](https://en.wikipedia.org/wiki/Povidone-iodine)) é frequentemente usado com antisséptico (mas também é desinfetante). O etanol pode ser usado com antisséptico ou como desinfetante. A lixívia ([hipoclorito de sódio](https://en.wikipedia.org/wiki/Sodium_hypochlorite)) é um desinfetante

    - eg. crescer E. coli
        pq? ela pode ser usada industrialmente para produzir insulina (ou outra substancia de interesse), ou em investigaçao<br>
        como? isola-se uma colónia e coloca-se num receipiente com meio de cultura esterilizado<br>
        como ter um meio de cultura esterilizado? primeiro, prepara-se o meio de cultura, geralmente num erlenmeyer. Depois o erlenmeyer vai ao autoclave. Obviamente que nao vai aberto para o autoclave - cria-se uma "rolha de segurança" colocando uma gaze (algodao), na boca e a rodear, papel de alumínio. Depois prende-se o conjunto com uma fita de autoclave. A cor inicial da fita é beje, e passa a castanho quando esterilizado. Normalmente todo o material que vai ao autoclave é reconhecivel por ter essa fita - se se contaminar, retirar a fita para nao enganar!<br>
        como garanto que após aberto o erlenmeyer nao fica contaminado? Temos de seguir os procedimentos assépticos/trabalhar em condiçoes de assepsia.

- Procedimentos assépticos
    - genéricos
        - ter todo o material necessário próximo antes de começar o procedimento
        - banca de trabalho desinfetada
        - recipientes devem estar abertos o mínimo tempo possível
        - trabalho deve ser realizado junto à chama! (bico de bunsen ou lamparina)

            ![condiçoes-assepticas](img/microbio/condiçoes-assepticas.png)

    - os recipientes
        - uma vez abertos os recipientes (tubos, balões, frascos/erlenmeyer), o seu bocal deve ser flamejado de imediato
        - procedimento:
            - a mao nao-dominante segura o erlenmeyer (prof chamou-lhe o "modo perguiçoso")
            - retira-se a tampa/rolha com o dedo mindinho da mao dominante (nao se pousa, continua-se a segurar!)
            - flameja-se o gargalo (faz-se passar pela chama)
            - realiza-se o trabalho.....
            - e depois tapa-se
        - é habitual realizar este procedimento numa cultura a crescer no erlenmeyer porque de tempos a tempos o meio de cultura fica gasto e é necessário "repicar" o meio. Podemos fazer a repicagem com uma ansa ou com um palito estéril
        - para pipetar, pode nao ser necessário retirar a rolha

    - [ansas](https://en.wikipedia.org/wiki/Inoculation_loop)
        - usada para inoculaçoes, para repicar meios
        - tem de ser esterilizada pela passagem na chama, antes e depois de serem utilizadas (como sempre...)
        - devem ser aquecidas até ficarem ao rubro (para assegurar que todos os esporos sejam destruídos). Devem ser agarradas quase pelo topo num ângulo praticamente vertical.

        ![ansa-inoculaçao](img/microbio/ansa-inoculaçao.png)

    - espalhadores/alças de esfregaço
        - utensilio na forma de L usado para espalhar o inóculo sobre a placa
        - podem ser esterilizados em estufa uma vez empacotados, ou por flamejação com álcool
            - mergulhar a extremidade do espalhador em álcool contido num recipiente tapado (com a outra mao, levantar ligeiramente a tampa ou a folha de aluminio)
            - passar rapidamente através da chama (o álcool vai arder esterilizando o vidro)
            - afastar o espalhador ligeiramente da chama até o álcool parar de arder
            - realizar o trabalho...

        ![espalhador](img/microbio/espalhador.png)
    

- material de laboratório

    - o material habitual de quimica
        - erlenmeyer, gobelés (nao se usa mto o balao volumétrico aqui)
        - pipetas e pompete
        - provetas
        - espátula
        - tubos e suporte
        - micropipetas e pontas
            - 1000micro =1ml sao as mais comuns
            - 5micro sao o mínimo
            - sistema de cores para as pontas
            - pipetar por ordem de volumes
        
        ![recipientes-transferencias](img/microbio/recipientes-transferencias.png)
    
    - alguns dos aparelhos habituais de quimica
        - centrifugadora + tubos

            ![centrifugadora](img/microbio/centrifugadora.png)

        - vortex
        - balanças

    - hotte vs camara de fluxo laminar

    - bico de bunsen (ligado ao gás) vs lamparinas

        ![bunsen-lamparina](img/microbio/bunsen-lamparina.png)

    - zaragatoas - "cotonetes"

        ![zaragatoa](img/microbio/zaragatoa.png)

    - incubadora com placa de agitaçao vs versao estufa<br>
        pomos lá os erlenmeyers e fica a incubar... Em ambas definimos a temperatura, mas na estufa nao há agitaçao

        ![incubadora](img/microbio/incubadora.png)

    - autoclave vs estufas<br>
        - o autoclave
            - é distintivo dos laboratórios de biologia, é uma "panela de pressao gigante" onde se esteriliza o material com calor húmido - em geral a 120ºC, 1atm, 20min - e o material sai com a fita de autoclave castanha
            - usado para vidro, plástico => baloes, pipetas

            ![autoclave](img/microbio/autoclave.png)

        - as estufas
            - podem ir a temperaturas mais elevadas
            - restritas a material de vidro
    
    - placas de Petri
        - podem ser de vidro (após usar, necessário esterilizar) ou de plástico (maior parte das nossas, sao descartáveis)
        - vem estereis no saco
        - sao seladas usando parafilme (uma película elástica)

- esterilizaçao de material
    - fisica
        - pelo calor seco em estufa
            - é um processo que pode ser utilizado com materiais resistentes a altas temperaturas (eg. de vidro)
            - utilizam-se estufas que atinjam temperaturas da ordem dos 160-180ºC
            - as células vegetativas são destruídas a 100ºC mas os esporos podem ser muito resistentes ao tratamento térmico

        - pelo calor seco por incineração
            - consiste na queima direta do material pelo fogo
            - utilizado na esterilização de ansas e eliminaçao de lixo tóxico
        
        ![calor-seco](img/microbio/calor-seco.png)

        - pelo calor húmido
            - pode ser conseguida através da fervura, autoclavagem ou pasteurização
            - o autoclave é o mais utilizado para esterilização de materiais e meios de cultura
            - a duração do tempo de esterilização depende da facilidade de penetraçao do calor. Para a mesma temperatura, os tempos de tratamento térmico são tanto maiores quanto maior for o volume do material a esterilizar. De uma forma geral, os materiais ficam esterilizados em 20-40 minutos
        
        ![calor-humido](img/microbio/calor-humido.png)
        
        - por filtração
            - utiliza-se no caso de produtos que são danificados pelo calor (eg. antibióticos e vitaminas)
            - os microrganismos são removidos por passagem da solução por uma membrana filtrante com poro inferior às dimensões bacterianas usuais
            - este tipo de filtração também é utilizado na esterilização de ar. As câmaras de fluxo laminar possuem um sistema de filtração de ar por membrana. O ar filtrado é pressionado para o interior da câmara, em feixes paralelos, obrigando o ar do interior a sair pelos poros da caixa inferior (câmara de fluxo vertical) ou pela abertura anterior (câmara de fluxo horizontal), criando-se assim um ambiente estéril

        ![esterilizaçao-filtraçao](img/microbio/esterilizaçao-filtraçao.png)

        - por ultrassons<br>
            radiação acústica de alta frequência
        
        - por radiações eletromagnéticas
            - utilizadas na esterilização de materiais sensíveis ao calor
            - Os raios X e os raios gama são radiações com poder ionizante e que alteram muitos dos compostos celulares. Estes raios têm um poder de penetração elevado sendo ideais para aplicação em objetos volumosos. Existem, no entanto, muitos problemas técnicos que dificultam a sua aplicação em microbiologia
            - As radiações UV têm menor energia que os raios X ou os raios gama mas são absorvidas por moléculas como o DNA, conduzindo a alterações na sua estrutura molecular que poderão ser letais para os microrganismos. Este tipo de esterilização é limitado a superfícies uma vez que se trata de radiações muito pouco penetrantes.

        ![esterilizaçao-radiaçao](img/microbio/esterilizaçao-radiaçao.png)
        
        - por raios catódicos<br>
            os raios catódicos, que são feixes de eletrões de intensidade e velocidade elevadas circulando num tubo de vácuo, são utilizados na esterilização de equipamento cirúrgico previamente embalado.
    
    - quimica
        - a maioria dos agentes quimicos elimina as células vegetativas mas não tem qualquer efeito sobre os esporos dos microrganismos
        - alguns (sabões, sais e fenóis) atuam por alteração da tensão superficial na interface célula/meio.
        - alguns (álcoois), desnaturam as proteínas e solubilizam os lípidos celulares
        - O cloro é um dos agentes mais vulgarmente utilizado em esterilização.<br>
        O cloro gasoso comprimido, sob a forma líquida, é utilizado na desinfeção de água de abastecimento público. A destruição das células é devida à combinação do cloro com as proteínas e à oxidação de compostos celulares provocada pelo oxigénio libertado na transformação do ácido hipercloroso em ácido clorídrico


- meios de cultura<br>
    A sementeira de amostras recolhidas em ambientes naturais contém em regra, populações mistas de microrganismos, pelo que devem utilizar-se meios de cultura complexos que permitam o crescimento do maior número possível de isolados.<br> Contudo, mesmo nestas circunstâncias não é possível detetar-se mais do que uma pequena percentagem dos indivíduos presentes na população inicial. Uma vez isolado um determinado microrganismo, torna-se necessária a sua caracterização.<br> 
    Em estudos de nutrição, inibição, toxicidade ou outros, são utilizados meios de composição definida.<br>
    Os meios de cultura sólidos são usados para contagem de colónias de microrganismos e para a multiplicação, isolamento e conservação dos microrganismos isolados.<br>
    O isolamento de determinados microrganismos é conseguido com maior rapidez e eficácia se for realizado em meios de cultura seletivos/diferenciais.<br>
    Os meios de cultura líquidos possibilitam o desenvolvimento de espécies contidas em populações mistas e permitem a realização de estudos de crescimento e nutrição.<br>

    - tipos<br>
        Alguns meios podem ser considerados, em simultâneo, englobados em mais de uma das categorias indicadas. Um agar salino de manitol, por exemplo, é um meio complexo, diferencial e seletivo

        - estado fisico
            - sólidos<br>
                usados para contagem de colónias e para a multiplicação, isolamento e conservação de estirpes
            - líquidos<br>
                usados para enriquecimento de determinadas estirpes contidas numa população mista, para estudos de crescimento e nutrição e para preparação de grandes volumes de uma cultura.
        - composiçao<br>
            A sementeira de uma amostra natural, contendo populações mistas de microrganismos heterotróficos, não permite detetar mais do que uma pequena percentagem dos indivíduos presentes na população inicial (os que se adaptam melhor a esse meio de cultura)<br>
            - meios (quimicamente) definidos<br>
                quantidades determinadas de reagentes químicos puros são misturados para a preparação do meio de cultura. Contém, em geral, sais inorgânicos que incluem fontes de carbono e de azoto. Outros componentes indispensáveis ao crescimento são necessários em tão pequenas quantidades que as pequenas contaminações dos reagentes químicos usados na preparação do meio são suficientes.
            - meios complexos<br>
                contém todos os ingredientes necessários ao crescimento do microrganismo; são compostos por misturas de proteínas e outros extratos de origem biológica, em que as quantidades precisas de cada aminoácido ou glúcido, por exemplo, não são conhecidas.<br>
                Alguns dos componentes dos meios complexos são resultantes de digestões preliminares de produtos biológicos. Estas digestões permitem uma melhor acessibilidade do microrganismo aos materiais plásticos de que necessita. Peptonas e triptonas, hidrolizados enzimáticos de proteína animal e de levedura, respetivamente, são dois frequentes constituintes dos meios complexos.
        - outras formas de classificaçao
            - enriquecidos<br>
                quando contém também alguns importantes fatores de crescimento como vitaminas, aminoácidos ou componentes do sangue. São meios necessários para fazer crescer microrganismos mais exigentes
            - seletivos<br>
                permite a seleção de um microrganismo particular de entre uma população em que os restantes microrganismos presentes não conseguem crescer. Os aditivos para tornar estes meios seletivos são os antibióticos ou outros compostos tóxicos
            - diferenciais<br>
                permite a separação de microrganismos de diferentes características, por alteração da coloração das colónias ou da região envolvente no meio de cultura
            - mínimos<br>
                são meios quimicamente definidos que contêm apenas os sais inorgânicos indispensáveis ao crescimento microbiano. Uma determinada fonte de carbono pode ser adicionada, dependendo do tipo de microrganismo a cultivar.


    - preparaçao de meios de cultura
        - cuidados gerais:<br>
            - a água deve ser colocada previamente no recipiente e só depois se devem adicionar os componentes para dissolução.<br>
                Uma adição da água posterior à colocação dos componentes pode originar a formação de agregados que se colam ao fundo do frasco dificultando a sua dissolução
            - se o meio contém muitos componentes, estes devem ser dissolvidos individualmente antes de efetuar a mistura. O agar só deve ser adicionado à mistura após os outros componentes terem sido dissolvidos
            - os frascos com meio de cultura para autoclavar não devem exceder a metade da sua capacidade
            - as rolhas/ tampas devem estar pouco apertadas durante a autoclavagem

        - verter meios em tubos de ensaio - técnica slant:<br>
            verter o meio sólido no tubo de ensaio e deixar solidificar inclinado<br>
            depois é mais fácil riscar uma cultura com uma ansa de inoculaçao
        
        - verter meios em placas de Petri (procedimento):<br>
            - retirar o frasco de meio com agar derretido do autoclave e deixar arrefecer até aos 50 ºC, de preferência num banho-maria
            - segurar o frasco com a mão esquerda junto à chama do bico de Bunsen e retirar a rolha com a mão direita
            - passar o gargalo pela chama
            - retirar ligeiramente a tampa da caixa de Petri com a mão direita, verter o meio estéril na caixa e recolocar a tampa (normalmente colocam-se 15-20 ml de meio por caixa de Petri); a base da caixa deve ficar coberta, o agar não deve tocar na tampa da caixa e a superfície deve ficar sem bolhas
            - passar o gargalo pela chama e recolocar a rolha
            - agitar suavemente a caixa para assegurar que o meio fica uniformemente distribuído
            - deixar o meio solidificar na placa
            - guardar a placa em posição invertida

    - exemplos de meios
        - meio YEPD
            - YEPD (Yeast Extract-Peptone-Dextrose) é o meio rico mais utilizado para o crescimento de fungos e em particular de leveduras, sempre que não são necessárias condições especiais para o seu cultivo.
            - fornece um excesso de carbono e azoto, assim como de aminoácidos, precursores de nucleótidos, vitaminas e metabolitos essenciais para um ótimo crescimento celular

            - ![yepd-composiçao](img/microbio/yepd-composiçao.png)

            - ![yepd-preparaçao](img/microbio/yepd-preparaçao.png)
        
        - meio LB
            - LB (Luria-Bertani broth) é utilizado para crescimento e manutenção de bactérias

            - ![lb-composiçao](img/microbio/lb-composiçao.png)

            - ![lb-preparaçao](img/microbio/lb-preparaçao.png)

    - recipientes para o cultivo e manutenção de microrganismos
        - poderiam-se manter em placas de Petri com meio de cultura, mas isso ocupa muito espaço
        - em vez disso, pode-se guardar em tubos de ensaio (usando a técnica slant) ou erlenmeyers ou bioreatores (industrialmente)
        - em investigaçao, quando é para usar em prazo indefinido ou por questoes de espaço, congelam-se as amostras

- procedimento experimental: "Prospeção do mundo microbiano"
    - Deve começar por escolher um local para efetuar a pesquisa de microrganismos e levar para esse local placas de Petri com meio de cultura estéril que permita o seu crescimento
    - No local escolhido, usando uma placa de Petri com o meio que preparou e, com auxílio de um cotonete húmido, recolha desse local uma amostra e faça a inoculação na placa de meio agarizado;
    - Deixe incubar as placas à temperatura ambiente até à aula seguinte, onde irá fazer a sua observação;
    - As restantes placas que tem na bancada poderão ser inoculadas com materiais à sua escolha. Use a imaginação! Proceda como foi anteriormente referido.
    - Observar à lupa as placas de Petri na aula seguinte.<br>
    Registar com fotografia.<br>
    Descrever os microrganismos que observa. A listagem de características que se segue servirá de orientação para as observações a efetuar e para a classificação quanto à morfologia das colónias das diferentes espécies de observadas.

    ![morfologia-colonias](img/microbio/morfologia-colonias.png)

# T 2 mar

história da microbiologia
TODO

- referencias:
    - https://bio.libretexts.org/Bookshelves/Microbiology/Book%3A_Microbiology_(Boundless)/6%3A_Culturing_Microorganisms
    - https://www.frontiersin.org/articles/10.3389/fmicb.2020.580709/full