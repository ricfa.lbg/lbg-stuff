# Coesite

## **Resumo**
- classe: Silicatos

- ocorrencia: é um mineral muito raro que se forma a partir de quartzo quando sujeito a altas pressoes e temperaturas.

    Não ocorre naturalmente na crusta<br>
    Foi produzido artificialmente em 1953, e foi apelidado segundo o quimico que a descobriu (Loring Coes).<br>
    Foi encontrado na natureza em 1960 numa cratera meteorítica em Winslow (cratera Barringer), nos Estado Unidos, gerado pelo impacto do meteorito.<br>
    E mais tarde também em rochas metamorficas de pressão ultra-alta.<br>
    Á superficie a coesite não é estável e decai para quartzo mas no interior da Terra, a profundidades de 60-100Km (ou seja, depois da crusta, no manto superior) ela é mais estável que o quartzo.

- composição: silica

    tem formula quimica $`SiO_2`$ (dióxido de silicio/silica)

- grupo: tectosilicatos

    tem a mesma composição quimica que o quartzo (silica) mas diferente estrutura cristalina (ou seja, é um polimorfo da silica) porque é formado a altas pressoes e temperaturas

- Cor: incolor ou rosa/amarelo muito pálido

- Risca: branca

- Dureza (escala de Mohs): 7.5

- Densidade: pesado

- Hábito: ocorre em cristais microscópicos prismáticos e tabulares, incluidas em rochas de quartzo

- Clivagem: sem clivagem

## **Mais treta**

- classe: Silicatos

    - os silicatos são minerais compostos predominantemente por grupos silicato.

        Um anião silicato (é um membro da familia de aniões) que consistem em silicio (Si) e oxigenio (O)

    - são geralmente compostos iónicos
    - compõe 90% da crusta

- formula (unidade repetitiva): $`SiO2`$
    
    uma rede de silica/dióxido de silicio ($`SiO2`$), ou seja, uma proporção 1:2

- grupo: tectosilicatos

    - composição como a do quartzo e dos feldspatos

    - uma rede 3d de tetraedros, com o catião $`Si^{4-}`$ no centro e 4 anioes $`O^{2-}`$ nos cantos do tetraedro.<br>
    E partilhando cada vértice de cada tetraedro (ou seja, parilhando o O) temos outro tetraedo

    
    ![tectosilicatos](img/miner/tectosilicatos.png)

    [imagem para comparação com os outros silicatos](https://cdn.britannica.com/42/2642-050-AC18536C/silicon-tetrahedron-silicate-minerals-atom-corner-oxygen.jpg)

## **Ainda não falamos disto**

- sistema cristalino: monoclínico

- classe do cristal: prismático (2/m)

- Fratura: concoidal


## "O que é que nós demos na última aula???"
- 28 outubro

    - Que tipo de ligações químicas podemos encontrar nos minerais

    todas. Para já estamos a falar minerais que são compostos iónicos. No quartzo por exemplo entre o Si e o O são covalentes, mais fracas, e as ligações van der Waals tb podem ser relevantes, como na grafite. Em metais puros são ligações metálicas.

    - O que são poliedros de coordenação e como se define o número de coordenação

    o poliedro é um catião rodead por aniões. O numero de aniões que o rodeia é o numero de coordenação

    - Que tipos de coordenação existem nas estruturas cristalinas e o que determina os diferentes tipos?
    
    o valor do quociente entre o raio iónico do catião e dos anições á volta

    O que é a valência electrostática e qual a sua importância no estudo da estrutura cristalina dos minerais?

    ainda nao falamos mas deve a questao de átomos com = valencia poderem substituir entre si numa estrutura cristalina