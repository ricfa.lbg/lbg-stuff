Isto é literalmente copy-paste dos resumos do stor e pouco ou nada mais

# CH1 - Introduçao

- mineralogia<br>
    é o estudo dos minerais

- uso dos minerais
    - produtos domésticos
    - agricultura<br>
    eg. fertilizantes minerais

    - processos ambientais<br>
    eg. controlo da composição química das águas

- aplicaçoes da mineralogia
    - melhor uso dos minerais
    - compreender fenómenos quimicos e geológicos

## 1.1 - definiçao de mineral e de rocha

- definiçao de mineral (geralmente aceite)<br>
    Um mineral é um sólido homogéneo que ocorre naturalmente e que possui uma composição química definida (não necessariamente fixa) e um arranjo atómico altamente ordenado

    =><br> 
    - um mineral tem ocorrencia natural<br>
        nao é produzido pelo Homem

    - um mineral é um sólido homogéneo<br>
        Homogénio significa que nao pode ser fisicamente subdividido.<br>
        E é sólido, logo exclui gases e líquidos => assim, H2O em forma de gelo é um mineral mas a água líquida não o é. O mercúrio como metal nativo não deveria ser considerado como mineral, embora este metal líquido ocorra naturalmente e seja considerado habitualmente como mineral.

    - um mineral tem composição química definida mas não necessariamente fixa<br> 
        indica que podemos atribuir uma fórmula química para um mineral, tal como SiO2 para o quartzo. Porém, é possível referir muitos exemplos de minerais em que um ou mais elementos se podem substituir mutuamente; p.e. (Mg,Fe)2SiO4 - olivina

    - um mineral pode ter impurezas<br>
        todos os sólidos naturais possuem numerosas impurezas em concentrações que podem ir a vários ppm, originando uma grande variabilidade entre os mesmos minerais de proveniências diversas, quando estudados com grande detalhe.

    - um mineral é *geralmente* cristalino<br>
        O conceito de "arranjo atómico altamente ordenado" refere-se à ordenação da estrutura interna dos cristais da maioria dos minerais, em que os átomos (ou iões) estão ligados entre si de modo a produzir uma malha tridimensional que se repete no espaço - ou seja, os minerais são exemplos de materiais cristalinos. Porém, existem exemplos de minerais que apresentam uma cristalinidade muito baixa ou são mesmo amorfos; p.e. algumas formas de sílica criptocristalina conhecida como calcedónia.

    - um mineral é inorganico, mas pode ser biogénico<br>
        os sólidos orgânicos tais como o carvão ou linhite nao sao minerais.<br>
        Mas os minerais podem ser formados por processos que envolvem organismos vivos, tais como algas e bactérias.


- mineralóide<br>
    As substâncias naturais que não são integradas na definição limitada de mineral, mas que são normalmente objecto de estudo pela mineralogia, são conhecidas por mineralóides.<br>
    eg. vidro vulcânico, mercúrio

- definiçao de mineral (mais abrangente)<br>
    - proposta pela Comissão para os Novos Minerais da International Mineralogical Association, em 1995
    - "um elemento químico ou um composto, geralmente cristalino, gerado por um processo geológico"
    - inclui as espécies excluídas pela anterior definição, que alguns autores referem por mineralóides

- definiçao de rocha<br>
    - rocha é um agregado sólido natural composto por grãos de minerais, vidro, macerais e/ou outros sólidos naturais
    - macerais são restos orgânicos cuja acumulação origina a formação de carvões

## 1.2 composiçao média da crusta terrestre

O grupo dos feldspatos é o grupo de minerais mais abundante, atingindo cerca de 50% do total.

composiçao das crusta (quanto ao tipo de rocha):
- ígneas : 65%
- metamórficas: 28%
- sedimentares: 8%

tipos de rochas igneas na crusta:
- basalto e gabro: 66%
- granito e rochas claras: 34%

abundancia de minerais na crusta:
- plagioclase: 39%
- ortoclase: 12%
- quartzo: 12%
- piroxena: 11%
- mica: 5%
- anfíbola: 5%
- argila: 4.4%
- olivina: 3%
- outros (principalmente nao-silicatos): 8.4%

Apesar da variedade de minerais, a composiçao elementar é simples<br>
- sao conhecidos mais de 100 elementos químicos, sendo 80 destes estáveis
- mas a crusta terrestre é constituída quase na totalidade apenas por 8 elementos, com todos os restantes elementos a somarem menos de 1%

composição elemental da crusta (% peso):
- Oxigénio, O: 46.4%
- Silicio, Si: 28.15%
- Aluminio, Al: 8.23%
- Ferro, Fe: 5.63%
- Calcio, Ca: 4.15%
- Sódio, Na: 2.36%
- Magnesio, Mg: 2.33%
- Potássio, K: 2.09%
- Titanio, Ti: 0.53%
- Hidrogenio, H: 0.14%

## 1.3 ligaçoes quimicas e estruturas de compostos iónicos

ligaçoes quimicas nos minerais:
- intramoleculares (envolvem electroes de valencia)
    - iónicas<br>
    um cristal iónico é constituído pela alternância de catiões e aniões ligados entre si por forças electrostáticas. Cada ião é rodeado o mais possível pelos electrões do outro ião vizinho, aplicando-se o princípio do empacotamento de átomos esféricos.

    - covalentes<br>
    uma ligação covalente é formada pela partilha de electrões de valencia pelos átomos vizinhos, produzindo uma sobreposição de orbitais atómicas adjacentes.<br>
    a sobreposição de orbitais define a direcção da ligação pelo que a coordenação de um átomo ligado por este tipo de ligação é bem definida e restrita.

    - metálicas<br>
    numa estrutura metálica, os electrões de valência retiram-se dos seus átomos permanecendo num "mar" de electrões que mantêm ligados vários iões positivos. Este facto contribui também para a elevada condutividade eléctrica dos metais. O ordenamento dos iões resulta da geometria do empacotamento das várias "esferas".

- intermoleculares
    - ligaçoes de H
    - ligaçoes van der Waals

as ligações que não envolvem electrões de valência estão presentes nos sólidos cristalinos mas, normalmente, em associação com outras ligações químicas dado a fragilidade característica das primeiras.

- empacotamento de esferas<br>
    - a estrutura interna de um material cristalino pode ser descrita como um empacotamento ordenado de esferas, em que as esferas representam átomos ou iões.
    - As estruturas mais simples são constituídas por empacotamentos de esferas de dimensão semelhante; as estruturas são mais complexas quando estas esferas possuem dimensões variadas.
    - interstício (ou espaço intersticial)<br>
        é um espaço entre os arranjos de esferas
    - poliedro de coordenação<br>
        arranjo geométrico de esferas colocadas mais proximamente umas das outras encerrando um interstício ou esfera
    - número de coordenação (NC)<br>
        é o número de vizinhos mais próximos
    - rácio de raios dos ioes/iónicos (RR)<br>
        o raio de um interstício ou átomo esférico é referido habitualmente por RX (raio do catião) e o raio das esferas (que definem o poliedro de coordenação) é referido como RZ (raio de um anião).<br>
        Valores de RX/RZ são denunciadores do número de esferas vizinhas mais próximas e da forma geométrica do poliedro de coordenação

        razao de raios:
        - <0.155 => NC 2, coordenaçao Linear

        - 0.155 => NC 3, Triangular<br>
            nos vértices de um triangulo equilátero<br>
            implicito aqui que RR>=0.155 e RR<0.255

        - 0.255 => NC 4, Tetraédrica<br>
            nos vértices de um tetraedro

        - 0.414 => NC 6, Octaédrica<br>
            nos vértices de um octaedro
        
        - 0.732 => NC 8, Cúbica<br>
            nos vértices de um cubo
        
        - 1 ou maior => NC 12, empacotamento fechado<br>
            nos vértices de um cuboocatedro

    - valência electrostática (v.e.)<br>
        a carga do catião dividida pelo seu número de coordenação.<br>
        eg. caso do grupo (SiO4)4-. O ião Si4+ está rodeado por 4 oxigénios; a v.e. é +1, exatamente metade da carga de um ião O2-. Assim a carga do ião O pode ser neutralizada com duas ligações para dois iões Si. Isto é, o ião O faz a ponte entre dois tetraedros ligando dois grupos (SiO4)4-, dando origem a um grupo (Si2O7)6-

    - polimerizaçao<br>
        combinaçao de monómeros (eg. tetraedros de silica) para formar a totalidade do mineral
    
    - ligaçao mesodésmica<br>
        ligaçao entre monómeros

![coordenacao-1](img/miner-t/coordenacao-1.png)<br>
![coordenacao-2](img/miner-t/coordenacao-2.png)

# CH2 - Cristalografia

- definiçao de cristal/substancia cristalina<br>
    qualquer sólido que possua uma estrutura interna regular, independentemente de possuir formas regulares

    quanto á forma externa:
    - euédrico e subédrico<br>
    por terem crescido em condiçoes favoraveis, o sólido apresenta superficies planas assumindo formas geométricas regulares<br>
    euédrico - possui faces perfeitas<br>
    subédrico - possui faces imperfeitas

    - anédrico<br>
    nao apresenta qualquer tipo de face

    quanto á estrutura interna:
    - macrocristalino => cristais visiveis ao olho nu
    - microcristalino => observáveis ao microscópio óptico
    - criptocristalino => só reconhecivel com a ajuda de técnicas complementares (eg. difraçao de raios X)
    - amorfo => sem ordenamento estrutural


- cristalografia<br>
    é o estudo dos sólidos cristalinos e das regras que controlam o seu crescimento, forma exterior e estrutura interna

