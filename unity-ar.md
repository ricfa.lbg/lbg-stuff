# Realidade Aumentada

- realidade aumentada
    - augmented reality, AG
    - usar conteúdos digitais para aumentar uma experiencia real
        - aumentaçao visual<br>
            gráficos gerados por computador sobrepostos ao campo visual (óculos) ou ecra (tele/tablet)
    - critérios para ser considerado AR:
        - combina o real e o virtual<br>
            percebidos como objetos reais que partilham o espaço
        - interativo em tempo-real<br>
            experienciada em tempo real (nao gravada)
        - registada em 3D<br>
            "3D graphics registration"<br>
            os gráficos sao registados a localizaçoes reais 3D (nao é só uma sobreposiçao/overlay como um HUD - heads up display - num jogo...)<br>
            para conseguir isto, o dispositivo tem que:
            - mapear o espaço
            - rastrear/seguir a pose (posiçao + orientaçao) de objetos no espaço - "pose tracking"
        
        - tecnologias envolvidas:
            - geolocalizaçao<br>
                o GPS fornece um rastreamente de baixa precisao na Terra (erro de metros)

            - rastreamento de imagens<br>
                "image tracking"<br>
                imagens que vemos na camara do dispositivo (eg. marcadores código QR, game cards, códigos de barras)

            - rastreamento de movimentos<br>
                "simultaneous localization and mapping" (SLAM)
            
            - compreensao da cena/ambiente
                reconhecer os conjuntos de pontos de localizaçao como planos e outras formas 3D => necessário para posicionar objetos virtuais e interagir com objetos reais
            
            - rastreamente de objetos e faces<br>
                eg. fotos de selfie aumentadas no snapchat...




- tecnologia em teles e tablets
    - Android => [ARCore](https://developers.google.com/ar/develop/fundamentals)<br>
        [dispositivos compativeis](https://developers.google.com/ar/devices)
        
    - iOS => [ARKit](https://developer.apple.com/augmented-reality/arkit/)<br>
        [compatibilidade](https://ioshacker.com/iphone/arkit-compatibility-list-iphone-ipad-ipod-touch)

- vestíveis ("wearables")
    - óculos inteligentes ("smart glasses")

- Unity 3D: introduçao
    - motor de jogos
    - [requesitos mínimos](https://docs.unity3d.com/Manual/system-requirements.html)
    - componentes
        - Unity Hub
        - Unity Editor
        
    - [instalaçao](https://docs.unity3d.com/Manual/GettingStartedInstallingUnity.html)<br>
        preferivel instalar o Hub primeiro, que permite gerir as versoes, e instalar uma versao do Editor a seguir<br>
        https://unity3d.com/get-unity/download<br>

        ![install-editor](img/unity-ar/install-editor.png)
    
    - versoes<br>
        - o sistema de numeraçao atual corresponde com os anos do calendário<br>
        eg.2021.x.x<br>
        - versoes mais estáveis - LTS<br>
            "Long Term Support"

        - releases tecnicos<br>
            versoes menores que LTS, considerados estáveis<br>
            eg. 21.2.14f1

        - apha e beta<br>
            risco de cenas nao funcionarem direito

        ![editor-versions](img/unity-ar/editor-versions.png)
    
    - módulos de suporte de build<br>
        escolher os adequados (Android, iOS, outros)<br>

        ![editor-builds](img/unity-ar/editor-builds.png)
    
    - projetos<br>
        cada um está associado a uma versao do Unity.<br>
        atualizar um projeto para 1 minor update (2021.2.3 para 2021.2.16) nao deve dar problemas, mas para um point release é possivel (2021.2.x para 2021.3.x)<br>
        o Unity tem ferramentas para fazer o upgrade para uma versao mais recente (reimporta os assets) mas downgrading para versao anterior nao é suportado => ter um backup antes de converter<br>
        evitar espaços no nome do projeto
    
    - Git<br>
        https://github.com/github/gitignore/blob/main/Unity.gitignore<br>
        ![unity-gitignore](img/unity-ar/unity-gitignore.png)<br>


- [Unity graficos](https://docs.unity3d.com/Manual/Graphics.html)
    - [pipelines de renderizaçao](https://docs.unity3d.com/Manual/render-pipelines.html)
        - [built-in](https://docs.unity3d.com/Manual/built-in-render-pipeline.html)<br>
            mais antiga, usada por defeito, controlo de renderizaçao limitado

        - [Universal Render Pipeline (URP)](https://docs.unity3d.com/Manual/universal-render-pipeline.html)<br>
            é uma Scriptable Render Pipeline (SRP) - uma feature que permite controlar a renderizaçao via scripts

        - [High Definition Render Pipeline (HDRP)](https://docs.unity3d.com/Manual/high-definition-render-pipeline.html)<br>
            outra SRP mas para aparelhos de alto desempenho

        - [costumizada](https://docs.unity3d.com/Manual/srp-custom.html)<br>
            criar a tua SRP
        
        ![editor-urp](img/unity-ar/editor-urp.png)<br>
        ![editor-project](img/unity-ar/editor-project.png)<br>

- Unity basicos
    - [interface](https://docs.unity3d.com/Manual/UsingTheEditor.html)<br>
        ![editor-default-scene](img/unity-ar/editor-default-scene.png)<br>
        ![editor-interface](img/unity-ar/editor-interface.png)<br>
        - (A) the Toolbar provides access to the most essential working features. On the left it contains the basic tools for manipulating the Scene view and the GameObjects within it. In the centre are the play, pause and step controls. The buttons to the right give you access to Unity Collaborate, Unity Cloud Services and your Unity Account, followed by a layer visibility menu, and finally the Editor layout menu (which provides some alternate layouts for the Editor windows, and allows you to save your own custom layouts).
        - (B) the Hierarchy window is a hierarchical text representation of every GameObject in the Scene. Each item in the Scene has an entry in the hierarchy, so the two windows are inherently linked. The hierarchy reveals the structure of how GameObjects attach to each other.
        - (C) the Game view simulates what your final rendered game will look like through your Scene Cameras. When you click the Play button, the simulation begins.
        - (D) The Scene view allows you to visually navigate and edit your Scene. The Scene view can show a 3D or 2D perspective, depending on the type of Project you are working on.
        - (E) The Inspector Window allows you to view and edit all the properties of the currently selected GameObject. Because different types of GameObjects have different sets of properties, the layout and contents of the Inspector window change each time you select a different GameObject.
        - (F) The Project window displays your library of Assets that are available to use in your Project. When you import Assets into your Project, they appear here.
        - (G) The status bar provides notifications about various Unity processes, and quick access to related tools and settings.

    - [plataforma alvo](https://docs.unity3d.com/Manual/PlatformSpecific.html)<br>
        nas settings do Build (File > Build Settings)<br>
        
        ![editor-build](img/unity-ar/editor-build.png)<br>
        
        ![editor-build-android](img/unity-ar/editor-build-android.png)<br>
        
        ![build-jdk-error](img/unity-ar/build-jdk-error.png)<br>
        é porque ao instalar o editor nao selecionei esse suporte<br>
        https://docs.unity3d.com/Manual/android-sdksetup.html<br>
        
        ![unity-android-support](img/unity-ar/unity-android-support.png)<br>
        ![unity-android-support2](img/unity-ar/unity-android-support2.png)<br>
        ![empty-build](img/unity-ar/empty-build.png)<br>

    - [2d vs 3d](https://docs.unity3d.com/Manual/2Dor3D.html)<br>
        o projeto pode ser 2d ou 3d mas esta opçao [pode ser mudada a qualquer altura](https://docs.unity3d.com/Manual/2DAnd3DModeSettings.html)<br>

        abrir settings do Editor (Edit > Project Settings e abrir categoria Editor) e escolher o Default Behavior Mode (2D ou 3D)<br>

        ![editor-behaviour-mode](img/unity-ar/editor-behaviour-mode.png)<br>

        - Projeto em modo 2D:
            - any images you import are assumed to be 2D images (Sprites) and set to Sprite mode.
            - the Sprite Packer is enabled.
            - the Scene View is set to 2D.
            - the default game objects do not have real time, directional light.
            - the camera's default position is at 0,0,–10. (It is 0,1,–10 in 3D Mode.)
            - the camera is set to be Orthographic. (In 3D Mode it is Perspective.)
            - in the Lighting Window:
                - Skybox is disabled for new scenes
                - Ambient Source is set to Color. (With the color set as a dark grey: RGB: 54, 58, 66.)
                - Realtime Global Illumination is set to off.
                - Baked Global Illumination is set to off.
                - Auto-Building set to off.
        - Projeto em modo 3D:
            - any images you import are NOT assumed to be 2D images (Sprites).
            - the Sprite Packer is disabled.
            - the Scene View is set to 3D.
            - the default game objects have real time, directional light.
            - the camera's default position is at 0,1,–10. (It is 0,0,–10. in 2D Mode.)
            - the camera is set to be Perspective. (In 2D Mode it is Orthographic.)
            - in the Lighting Window:
                - Skybox is the built-in default Skybox Material.
                - Ambient Source is set to Skybox.
                - Realtime Global Illumination is set to on.
                - Baked Global Illumination is set to on.
                - Auto-Building is set to on.
    - criar cenas<br>
        File > New Scene<br>
        o Basic (built-in) contém uma camara e uma luz por defeito mas nao contem o SRP...<br>
        ![editor-new-scene](img/unity-ar/editor-new-scene.png)<br>
        
    - criar objetos<br>
        Gameobject > ...<br>
        ![editor-gameobject](img/unity-ar/editor-gameobject.png)<br>
    
    - [posicionar objectos](https://docs.unity3d.com/Manual/PositioningGameObjects.html)<br>
        consiste em manipular o componente [Transform](https://docs.unity3d.com/Manual/class-Transform.html)
    
    - assets do projeto<br>
        posso criar subpastas e organizar conforme perferir...<br>
        é melhor criá-las a partir do editor
    
    - escolher o [input handler](https://docs.unity3d.com/Manual/Input.html)<br>
        o built-in usa o [Input Manager](https://docs.unity3d.com/Manual/class-InputManager.html), mas existe um novo sistema ([Input System](https://docs.unity3d.com/Packages/com.unity.inputsystem@1.3/manual/index.html))

        - instalaçao do Input System<br>

        ![editor-input-system](img/unity-ar/editor-input-system.png)<br>
        ![editor-input-system2](img/unity-ar/editor-input-system2.png)<br>

        ou manualmente, Player Settings (Project Settings > Player e tab Configuration > Active Input Handling)<br>

        ![editor-input-system3](img/unity-ar/editor-input-system3.png)<br>


        
- [Unity XR](https://docs.unity3d.com/Manual/XR.html)
    - plug-in framework XR SDK<br>

    ![unity-xr-stack](img/unity-ar/unity-xr-stack.png)<br>

    - suporta as plataformas:
        - ARKit
        - ARCore
        - Microsoft HoloLens
        - Windows Mixed Reality
        - Magic Leap
        - Oculus
        - OpenXR
        - PlayStation VR

    - instalar XR plugins no dispositivo alvo<br>
        posso instalar através do Package Manager (Window > Package Manager) ou... o atalho<br>
        Project Settings > XR Plugin Management<br>
        eg. [ARCore XR Plugin](https://docs.unity3d.com/Manual/com.unity.xr.arcore.html)

        ![editor-xr](img/unity-ar/editor-xr.png)<br>
        ![editor-xr2](img/unity-ar/editor-xr2.png)<br>
        ![editor-xr3](img/unity-ar/editor-xr3.png)<br>
    
    - pacote [AR Foundation](https://docs.unity3d.com/Manual/com.unity.xr.arfoundation.html) para facilitar o desenvolvimento<br>
        é um conjunto de ferramentas de mais alto nível que o sistema de plugins
        - caracteristicas<br>
            AR Foundation is a set of MonoBehaviours and APIs for dealing with devices that support the following concepts:<br>

            - Device tracking: track the device's position and orientation in physical space.<br>
            - Plane detection: detect horizontal and vertical surfaces.
            - Point clouds, also known as feature points.<br>
            - Anchor: an arbitrary position and orientation that the device tracks.<br>
            - Light estimation: estimates for average color temperature and brightness in physical space.<br>
            - Environment probe: a means for generating a cube map to represent a particular area of the physical environment.<br>
            - Face tracking: detect and track human faces.<br>
            - 2D image tracking: detect and track 2D images.
            - 3D object tracking: detect 3D objects.
            - Meshing: generate triangle meshes that correspond to the physical space.
            - Body tracking: 2D and 3D representations of humans recognized in physical space.
            - Colaborative participants: track the position and orientation of other devices in a shared AR experience.
            - Human segmentation: determines a stencil texture and depth map of humans detected in the camera image.
            - Raycast: queries physical surroundings for detected planes and feature points.
            - Pass-through video: optimized rendering of mobile camera image onto touch screen as the background for AR content.
            - Session management: manipulation of the platform-level configuration automatically when AR Features are enable or disabled.
            - Occlusion: allows for occlusion of virtual content by detected environmental depth (environment occlusion) or by detected human depth (human occlusion).
        
        - instalaçao<br>
            no Package Manager, ver a Unity Registry e procurar pelo pacote<br>
            ![editor-ar-found](img/unity-ar/editor-ar-found.png)<br>
    
    - adicionar suporte para Universal Render Pipeline (URP)<br>
        selecionar ao criar a cena
    
    - [setting up para Android](https://developers.google.com/ar/develop/unity-arf/getting-started-ar-foundation)
        - ter instalados os módulos de suporte para build em Android (feito)
        - definir a plataforma alvo - Android (feito)
        - ter instalado os plugins XR - ARCore (feito)
        - ativar o debbuging por USB no tele<br>
            nalguns, as opçoes de programador aparecem logo.<br>
            Nos outros, ir a Settings > About > clicar 7x no Build Number e Developer Options deve aparecer
        - configurar uma AR Session e adicionar componentes AR Foundation<br>
            - A scene needs an AR session to enable AR processes, such as motion tracking, environmental understanding, and lighting estimation. You will need the following game objects to support an AR session:
                - AR Session: Controls the lifecycle of an AR experience.
                - AR Session Origin: Transforms AR coordinates into Unity world coordinates.

                Before adding the new game objects, delete the default Main Camera. It will be replaced by a new AR Camera in the AR Session Origin. Then, add a new AR Session and a new AR Session Origin game object.

            ![ar-components](img/unity-ar/ar-components.png)
        
        - configurar as Android Player settings<br>
            ![android-player-settings](img/unity-ar/android-player-settings.png)<br>
        
        - instalar as ARCore Extensions<br>
            opcional<br>
            fazer download da tarball [arcore-unity-extensions-*.tgz](https://github.com/google-ar/arcore-unity-extensions/releases/)<br>
            Package Manager > Add package from tarball
    
    - cena teste para Android<br>
        Add a point cloud manager to the Session Origin object (Add Component in the Inspector window> AR Point Cloud Manager)<br>
        Create a Point Cloud Prefab to visualize the detected depth points (create a Particle System with a AR Point Cloud and a AR Point Cloud Visualizer)<br>
        It would be GameObject > Effects > Particle System but if you create a Visualizer, it should come with a particle system already<br>
        Drag the PointParticle object from the Hierarchy window to the Prefabs folder in the Project window (create the folder first if necessary). This makes the GameObject into a prefab.<br>
        Delete the PointParticle object from the Hierarchy and apply it  to the AR Point Cloud Manager<br>
        Ensure your device is connected to your computer via USB cable. Press the Build And Run button to build the project and install it on your device.<br>
        If all goes well, the project will build, install on your device, and launch. You should see a camera video feed on your device's screen. Move the phone slowly in different directions. As it scans the environment, feature points will be detected and rendered on the screen.<br>

- [codelabs - Create an AR game using Unity's AR Foundation](https://codelabs.developers.google.com/arcore-unity-ar-foundation#0)

